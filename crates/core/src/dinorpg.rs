#[cfg(feature = "sqlx")]
use crate::pg_num::PgU32;
use crate::twinoid::{TwinoidSessionKey, TwinoidUserDisplayName};
use crate::types::AnyError;
use async_trait::async_trait;
use auto_impl::auto_impl;
#[cfg(feature = "serde")]
use etwin_serde_tools::serialize_ordered_map;
#[cfg(feature = "serde")]
use etwin_serde_tools::{Deserialize, Serialize};
use std::collections::HashMap;
use thiserror::Error;

declare_new_enum!(
  pub enum DinorpgServer {
    #[str("www.dinorpg.com")]
    DinorpgCom,
    #[str("en.dinorpg.com")]
    EnDinorpgCom,
    #[str("es.dinorpg.com")]
    EsDinorpgCom,
  }
  pub type ParseError = DinorpgServerParseError;
  const SQL_NAME = "dinorpg_server";
);

declare_decimal_id! {
  pub struct DinorpgUserId(u32);
  pub type ParseError = DinorpgUserIdParseError;
  const BOUNDS = 0..1_000_000_000;
  const SQL_NAME = "dinorpg_user_id";
}

declare_new_string! {
  /// A Dinorpg session key.
  ///
  /// It correspond to the value of the `sid` cookie.
  ///
  /// - `wKc12oxNKinxZmDMxOY38jmBD1wSEtOL`
  pub struct DinorpgSessionKey(String);
  pub type ParseError = DinorpgSessionKeyParseError;
  const PATTERN = r"^[0-9a-zA-Z]{32}$";
  const SQL_NAME = "dinorpg_session_key";
}

declare_decimal_id! {
  pub struct DinorpgDinozId(u32);
  pub type ParseError = DinorpgDinozIdParseError;
  const BOUNDS = 0..1_000_000_000;
  const SQL_NAME = "dinorpg_dinoz_id";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "DinorpgUser"))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DinorpgUserIdRef {
  pub server: DinorpgServer,
  pub id: DinorpgUserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "DinorpgUser"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortDinorpgUser {
  pub server: DinorpgServer,
  pub id: DinorpgUserId,
  pub display_name: TwinoidUserDisplayName,
}

impl ShortDinorpgUser {
  pub const fn as_ref(&self) -> DinorpgUserIdRef {
    DinorpgUserIdRef {
      server: self.server,
      id: self.id,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDinozSkillsResponse {
  pub profile: DinorpgProfile,
  pub dinoz_id: DinorpgDinozId,
  pub skills: Vec<DinorpgSkill>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDemonShopResponse {
  pub profile: DinorpgProfile,
  pub dinoz: Vec<SacrificedDinoz>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct SacrificedDinoz {
  pub name: DinorpgDinozName,
  pub level: u8,
  pub skin_check: DinorpgDinozSkinCheck,
  pub elements: DinorpgDinozElements,
  pub skills: Vec<DinorpgSkill>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDinozElements {
  pub fire: u8,
  pub wood: u8,
  pub water: u8,
  pub thunder: u8,
  pub air: u8,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDinozMissionsResponse {
  pub profile: DinorpgProfile,
  pub dinoz: Option<Vec<DinorpgDinozMission>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDinozMission {
  pub id: DinorpgDinozId,
  #[cfg_attr(feature = "serde", serde(serialize_with = "serialize_ordered_map"))]
  pub missions: HashMap<DinorpgNpc, Vec<DinorpgMission>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgIngredientResponse {
  pub profile: DinorpgProfile,
  #[cfg_attr(feature = "serde", serde(serialize_with = "serialize_ordered_map"))]
  pub ingredients: HashMap<DinorpgIngredient, DinorpgIngredientCount>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgProfile {
  pub user: ShortDinorpgUser,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DinorpgSession {
  pub key: DinorpgSessionKey,
  pub user: ShortDinorpgUser,
}

#[async_trait]
#[auto_impl(&, Arc)]
pub trait DinorpgClient: Send + Sync {
  async fn get_own_ingredients(&self, session: &DinorpgSession) -> Result<DinorpgIngredientResponse, AnyError>;

  async fn get_own_missions(&self, session: &DinorpgSession) -> Result<DinorpgDinozMissionsResponse, AnyError>;

  async fn get_own_demon_shop(&self, session: &DinorpgSession) -> Result<DinorpgDemonShopResponse, AnyError>;

  async fn create_session(
    &self,
    server: DinorpgServer,
    tid_key: &TwinoidSessionKey,
  ) -> Result<DinorpgSession, DinorpgClientCreateSessionError>;
}

#[derive(Debug, Error)]
pub enum DinorpgClientCreateSessionError {
  #[error("invalid Twinoid session key")]
  InvalidTwinoidSessionKey,
  #[error(transparent)]
  Other(#[from] AnyError),
}

declare_new_int! {
  pub struct DinorpgIngredientCount(u32);
  pub type RangeError = DinorpgIngredientQuantityRangeError;
  const BOUNDS = 0..1_000_000_000;
  type SqlType = PgU32;
  const SQL_NAME = "dinorpg_ingredient_quantity";
}

declare_new_string! {
  pub struct DinorpgDinozName(String);
  pub type ParseError = DinorpgDinozNameParseError;
  const PATTERN = r"^.{1,50}$";
  const SQL_NAME = "dinorpg_dinoz_name";
}

declare_new_string! {
  /// Skin code for the appearance of a Dinoz
  pub struct DinorpgDinozSkin(String);
  pub type ParseError = DinorpgDinozSkinParseError;
  const PATTERN = r"^.{1,50}$";
  const SQL_NAME = "dinorpg_dinoz_skin";
}

declare_new_string! {
  /// Checksum for a DinorpgDinozSkin
  pub struct DinorpgDinozSkinCheck(String);
  pub type ParseError = DinorpgDinozSkinCheckParseError;
  const PATTERN = r"^.{1,50}$";
  const SQL_NAME = "dinorpg_dinoz_skin_check";
}

declare_new_enum!(
  pub enum DinorpgIngredient {
    #[str("MerouLujidane")]
    MerouLujidane,
    #[str("PoissonVengeur")]
    PoissonVengeur,
    #[str("AnGuiliGuilille")]
    AnGuiliGuilille,
    #[str("Globulos")]
    Globulos,
    #[str("SuperPoisson")]
    SuperPoisson,
    #[str("TouffeDeFourrure")]
    TouffeDeFourrure,
    #[str("RocheRadioActive")]
    RocheRadioActive,
    #[str("GriffesAcerees")]
    GriffesAcerees,
    #[str("CorneEnChocolat")]
    CorneEnChocolat,
    #[str("OeilVisqueux")]
    OeilVisqueux,
    #[str("LangueMonstrueuse")]
    LangueMonstrueuse,
    #[str("EnergieFoudre")]
    EnergieFoudre,
    #[str("EnergieAir")]
    EnergieAir,
    #[str("EnergieEau")]
    EnergieEau,
    #[str("EnergieFeu")]
    EnergieFeu,
    #[str("EnergieBois")]
    EnergieBois,
    #[str("SilexTaille")]
    SilexTaille,
    #[str("FragmentDeTexteAncien")]
    FragmentDeTexteAncien,
    #[str("VieilAnneauPrecieux")]
    VieilAnneauPrecieux,
    #[str("CaliceCisele")]
    CaliceCisele,
    #[str("CollierKarat")]
    CollierKarat,
    #[str("BrocheEnParfaitEtat")]
    BrocheEnParfaitEtat,
    #[str("SuperbeCouronneRoyale")]
    SuperbeCouronneRoyale,
    #[str("FeuillesDePelinae")]
    FeuillesDePelinae,
    #[str("BoletPhaliskBlanc")]
    BoletPhaliskBlanc,
    #[str("OrchideeFantasque")]
    OrchideeFantasque,
    #[str("RacinesDeFigonicia")]
    RacinesDeFigonicia,
    #[str("SadiquaeMordicus")]
    SadiquaeMordicus,
    #[str("Flaureole")]
    Flaureole,
    #[str("SporeEtheral")]
    SporeEtheral,
    #[str("PousseSombre")]
    PousseSombre,
    #[str("GrainedeDevoreuse")]
    GrainedeDevoreuse,
    #[str("DentDeDorogon")]
    DentDeDorogon,
    #[str("BrasMecanique")]
    BrasMecanique,
  }
  pub type ParseError = DinorpgIngredientNameParseError;
  const SQL_NAME = "dinorpg_ingredient_name";
);

declare_new_enum!(
  pub enum DinorpgNpc {
    #[str("Chen")]
    Chen,
    #[str("DianKorgsey")]
    DianKorgsey,
    #[str("GardienDeLaForet")]
    GardienDeLaForet,
    #[str("Gulom")]
    Gulom,
    #[str("MadameX")]
    MadameX,
    #[str("MaitreElementaire")]
    MaitreElementaire,
    #[str("MaitreZenit")]
    MaitreZenit,
    #[str("NicolasMulot")]
    NicolasMulot,
    #[str("PapyJoe")]
    PapyJoe,
    #[str("ProfesseurHenderson")]
    ProfesseurHenderson,
    #[str("RhubarbapapaLeSage")]
    RhubarbapapaLeSage,
    #[str("RodeurEtrange")]
    RodeurEtrange,
    #[str("ShamanMou")]
    ShamanMou,
    #[str("SergentPepper")]
    SergentPepper,
    #[str("Skully")]
    Skully,
    #[str("Cassandre")]
    Cassandre,
    #[str("BureauDesRequetes")]
    BureauDesRequetes,
    #[str("BaoBob")]
    BaoBob,
    #[str("AlZaimeur")]
    AlZaimeur,
    #[str("HommeLouche")]
    HommeLouche,
    #[str("Broc")]
    Broc,
    #[str("CapitaineDeLaGarde")]
    CapitaineDeLaGarde,
    #[str("AnnaTomie")]
    AnnaTomie,
  }
  pub type ParseError = DinorpgCharacterNameParseError;
  const SQL_NAME = "dinorpg_character_name";
);

declare_new_enum!(
  pub enum DinorpgMission {
    #[str("CommeUnKorgonDansLeau")]
    CommeUnKorgonDansLeau,
    #[str("KorgonsDuNordKorgonsDuSud")]
    KorgonsDuNordKorgonsDuSud,
    #[str("SteakDeBois")]
    SteakDeBois,
    #[str("RivalitesKorgons")]
    RivalitesKorgons,
    #[str("MenageAutourDuCamp")]
    MenageAutourDuCamp,
    #[str("MaitreOuEstTu")]
    MaitreOuEstTu,
    #[str("LieuHante")]
    LieuHante,
    #[str("TourSombre")]
    TourSombre,
    #[str("Lumiereeee")]
    Lumiereeee,
    #[str("LesCoursesDuMaitre")]
    LesCoursesDuMaitre,
    #[str("Lecon1LaFoudre")]
    Lecon1LaFoudre,
    #[str("Lecon2LeFeu")]
    Lecon2LeFeu,
    #[str("Lecon3LeBois")]
    Lecon3LeBois,
    #[str("Lecon4Lair")]
    Lecon4Lair,
    #[str("Lecon5Leau")]
    Lecon5Leau,
    #[str("InfusionAFusion")]
    InfusionAFusion,
    #[str("LeCha")]
    LeCha,
    #[str("LAffrontement")]
    LAffrontement,
    #[str("LaRouteDuGrandPeril")]
    LaRouteDuGrandPeril,
    #[str("Allergies")]
    Allergies,
    #[str("Remede")]
    Remede,
    #[str("Cartographie")]
    Cartographie,
    #[str("KitDeLAventurier")]
    KitDeLAventurier,
    #[str("DuPoissonFrais")]
    DuPoissonFrais,
    #[str("LeChienPerdu")]
    LeChienPerdu,
    #[str("LesGoupignonsDesCollines")]
    LesGoupignonsDesCollines,
    #[str("LaChasseAuxLoups")]
    LaChasseAuxLoups,
    #[str("LeRosierEnDanger")]
    LeRosierEnDanger,
    #[str("LeLivreDeRecettes")]
    LeLivreDeRecettes,
    #[str("LesTimbres")]
    LesTimbres,
    #[str("LaLettreConfidentielle")]
    LaLettreConfidentielle,
    #[str("UnEtrangeMonstre")]
    UnEtrangeMonstre,
    #[str("LesGeants")]
    LesGeants,
    #[str("LexploitDuMois")]
    LexploitDuMois,
    #[str("ColmaterLaBreche")]
    ColmaterLaBreche,
    #[str("ProtegerLesIlesAtlanteinees")]
    ProtegerLesIlesAtlanteinees,
    #[str("ReparerLesInstallationsDuLaboratoire")]
    ReparerLesInstallationsDuLaboratoire,
    #[str("Pastafarisme")]
    Pastafarisme,
    #[str("DeuxMillePiedsSousLeau")]
    DeuxMillePiedsSousLeau,
    #[str("NouvelleExperience")]
    NouvelleExperience,
    #[str("MenaceDuMondeSombre")]
    MenaceDuMondeSombre,
    #[str("UnePetiteMousse")]
    UnePetiteMousse,
    #[str("UnePetiteGoutteDether")]
    UnePetiteGoutteDether,
    #[str("EclatDeMagnetite")]
    EclatDeMagnetite,
    #[str("QuestDevenuDinoland")]
    QuestDevenuDinoland,
    #[str("Meditation")]
    Meditation,
    #[str("CombatDeLesprit")]
    CombatDeLesprit,
    #[str("LeRizAmnesique")]
    LeRizAmnesique,
    #[str("LesDinozSombres")]
    LesDinozSombres,
    #[str("LaFiestaDal")]
    LaFiestaDal,
    #[str("DesMergeuzSinonRien")]
    DesMergeuzSinonRien,
    #[str("Alcool")]
    Alcool,
    #[str("PermissionDeMinuit")]
    PermissionDeMinuit,
    #[str("ExcesDeMagnetisme")]
    ExcesDeMagnetisme,
    #[str("OuvrirLaRoute")]
    OuvrirLaRoute,
    #[str("LaCaravaneOccidentale")]
    LaCaravaneOccidentale,
    #[str("ZesteDeScorpwink")]
    ZesteDeScorpwink,
    #[str("SecuriserLaCroisee")]
    SecuriserLaCroisee,
    #[str("LeVoleurGourmand")]
    LeVoleurGourmand,
    #[str("Vermifuge")]
    Vermifuge,
    #[str("OnRechercheSahalamiLeTrancheur")]
    OnRechercheSahalamiLeTrancheur,
    #[str("UnCollierBienMysterieux")]
    UnCollierBienMysterieux,
    #[str("OnRechercheTripouLeMou")]
    OnRechercheTripouLeMou,
    #[str("OnRechercheBoukaneLintuable")]
    OnRechercheBoukaneLintuable,
    #[str("OnRechercheCervelahLempoisonneur")]
    OnRechercheCervelahLempoisonneur,
    #[str("LeCadeauDanniversaire")]
    LeCadeauDanniversaire,
    #[str("LeTrocDesIlesAtlanteinees")]
    LeTrocDesIlesAtlanteinees,
    #[str("LaChasseALaKazkadine")]
    LaChasseALaKazkadine,
    #[str("LesAnguillozAuVinaigre")]
    LesAnguillozAuVinaigre,
    #[str("LaGrandeChasse")]
    LaGrandeChasse,
    #[str("LeRallyeDesSardines")]
    LeRallyeDesSardines,
    #[str("LeRallyeDesPoissons")]
    LeRallyeDesPoissons,
    #[str("LeRallyeDesRequins")]
    LeRallyeDesRequins,
    #[str("LeRallyeDesBaleines")]
    LeRallyeDesBaleines,
    #[str("LeTourDeDinoland")]
    LeTourDeDinoland,
    #[str("Sos")]
    Sos,
    #[str("LaTombeDeLarchidorogon")]
    LaTombeDeLarchidorogon,
    #[str("RechercheDingredient")]
    RechercheDingredient,
    #[str("VisiteAuxEnfants")]
    VisiteAuxEnfants,
    #[str("SurLesTracesDeMorg")]
    SurLesTracesDeMorg,
    #[str("LeParcoursDuCombattant")]
    LeParcoursDuCombattant,
    #[str("TerrainMine")]
    TerrainMine,
    #[str("LaDeMouktization")]
    LaDeMouktization,
    #[str("Linitiation")]
    Linitiation,
    #[str("LeGantDeCapture")]
    LeGantDeCapture,
    #[str("LaMiseALepreuve")]
    LaMiseALepreuve,
    #[str("Lepouvantarchelion")]
    Lepouvantarchelion,
    #[str("AuFeu")]
    AuFeu,
    #[str("LeBarbecueDejante")]
    LeBarbecueDejante,
    #[str("LaDefenseDesForges")]
    LaDefenseDesForges,
    #[str("LeColisDisparu")]
    LeColisDisparu,
    #[str("RituelRidicule")]
    RituelRidicule,
    #[str("Hieroglyphes")]
    Hieroglyphes,
    #[str("PigeonVoyageur")]
    PigeonVoyageur,
    #[str("Querelles")]
    Querelles,
    #[str("CommercePeuEquitable")]
    CommercePeuEquitable,
    #[str("InformationsPreliminaires")]
    InformationsPreliminaires,
    #[str("SurLesTracesDeMouldeur")]
    SurLesTracesDeMouldeur,
    #[str("MademoiselleBao")]
    MademoiselleBao,
    #[str("LesTouristesGenants")]
    LesTouristesGenants,
    #[str("DesCrevettesAuNapalm")]
    DesCrevettesAuNapalm,
    #[str("LesRetrouvaillages")]
    LesRetrouvaillages,
    #[str("LaMaletteNoire")]
    LaMaletteNoire,
    #[str("LeMarchandTraitre")]
    LeMarchandTraitre,
    #[str("LaCoursePoursuite")]
    LaCoursePoursuite,
    #[str("LesIlesCestKool")]
    LesIlesCestKool,
    #[str("LeComplotAshpouk")]
    LeComplotAshpouk,
    #[str("LeGardienDeLaForet")]
    LeGardienDeLaForet,
    #[str("LaMainVerte")]
    LaMainVerte,
    #[str("PermisDeCouper")]
    PermisDeCouper,
    #[str("LeRoiDeLaJungle")]
    LeRoiDeLaJungle,
    #[str("FaitesVosVoeux")]
    FaitesVosVoeux,
    #[str("UnCricPourLaForet")]
    UnCricPourLaForet,
    #[str("MonnaieDeSinge")]
    MonnaieDeSinge,
    #[str("ExplosionDeBoulon")]
    ExplosionDeBoulon,
    #[str("InventionAChaud")]
    InventionAChaud,
    #[str("PiegeAKorgonsGrognons")]
    PiegeAKorgonsGrognons,
    #[str("FuiteMoteur")]
    FuiteMoteur,
    #[str("AnalyseSanguinaire")]
    AnalyseSanguinaire,
    #[str("AppelALaPelle")]
    AppelALaPelle,
    #[str("PremierGeste")]
    PremierGeste,
    #[str("SecondGeste")]
    SecondGeste,
    #[str("TroisiemeGeste")]
    TroisiemeGeste,
    #[str("QuatriemeGeste")]
    QuatriemeGeste,
    #[str("CinquiemeGeste")]
    CinquiemeGeste,
    #[str("SixiemeGeste")]
    SixiemeGeste,
    #[str("PremiereOrdonnance")]
    PremiereOrdonnance,
    #[str("BoletABeauPied")]
    BoletABeauPied,
    #[str("PhytotherapieFarfelue")]
    PhytotherapieFarfelue,
    #[str("VoisinsRecalcitrants")]
    VoisinsRecalcitrants,
    #[str("InvasionDeMonstres")]
    InvasionDeMonstres,
    #[str("PremierSecours")]
    PremierSecours,
    #[str("LaCuriositeEstUnVilainDefaut")]
    LaCuriositeEstUnVilainDefaut,
    #[str("UnServiceLouche")]
    UnServiceLouche,
    #[str("LesMallesSeFontLaMalle")]
    LesMallesSeFontLaMalle,
    #[str("FuiteDeMana")]
    FuiteDeMana,
    #[str("LichenManactif")]
    LichenManactif,
    #[str("HistoireDuMondeSombre")]
    HistoireDuMondeSombre,
    #[str("HistoireDuMondeSombreDeux")]
    HistoireDuMondeSombreDeux,
    #[str("VisiteDuMondeSombre")]
    VisiteDuMondeSombre,
  }
  pub type ParseError = DinorpgMissionParseError;
  const SQL_NAME = "dinorpg_mission";
);

declare_new_enum!(
  pub enum DinorpgDinozRace {
    #[str("Castivore")]
    Castivore,
    #[str("Feross")]
    Feross,
    #[str("Gorilloz")]
    Gorilloz,
    #[str("Kabuki")]
    Kabuki,
    #[str("Mahamuti")]
    Mahamuti,
    #[str("Moueffe")]
    Moueffe,
    #[str("Nuagoz")]
    Nuagoz,
    #[str("Pigmou")]
    Pigmou,
    #[str("Planaille")]
    Planaille,
    #[str("Pteroz")]
    Pteroz,
    #[str("Quetzu")]
    Quetzu,
    #[str("Rocky")]
    Rocky,
    #[str("Santaz")]
    Santaz,
    #[str("Sirain")]
    Sirain,
    #[str("Hippoclamp")]
    Hippoclamp,
    #[str("Smog")]
    Smog,
    #[str("Soufflet")]
    Soufflet,
    #[str("Toufufu")]
    Toufufu,
    #[str("Triceragnon")]
    Triceragnon,
    #[str("Wanwan")]
    Wanwan,
    #[str("Winks")]
    Winks,
  }
  pub type ParseError = DinorpgDinozRaceParseError;
  const SQL_NAME = "dinorpg_dinoz_race";
);

impl DinorpgDinozRace {
  pub fn from_skin_code(skin: &str) -> Option<Self> {
    if let Some(c) = skin.chars().next() {
      match c {
        '0' => Some(Self::Moueffe),
        '1' => Some(Self::Pigmou),
        '2' => Some(Self::Winks),
        '3' => Some(Self::Planaille),
        '4' => Some(Self::Castivore),
        '5' => Some(Self::Rocky),
        '6' => Some(Self::Pteroz),
        '7' => Some(Self::Nuagoz),
        '8' => Some(Self::Sirain),
        '9' => Some(Self::Hippoclamp),
        'A' => Some(Self::Gorilloz),
        'B' => Some(Self::Wanwan),
        'C' => Some(Self::Santaz),
        'D' => Some(Self::Feross),
        'E' => Some(Self::Kabuki),
        'F' => Some(Self::Mahamuti),
        'H' => Some(Self::Toufufu),
        'I' => Some(Self::Quetzu),
        'J' => Some(Self::Smog),
        'K' => Some(Self::Triceragnon),
        _ => None,
      }
    } else {
      None
    }
  }
}

declare_new_enum!(
  pub enum DinorpgSkill {
    #[str("GriffesEnflammees")]
    GriffesEnflammees,
    #[str("Colere")]
    Colere,
    #[str("Force")]
    Force,
    #[str("Charge")]
    Charge,
    #[str("SangChaud")]
    SangChaud,
    #[str("Furie")]
    Furie,
    #[str("ArtsMartiaux")]
    ArtsMartiaux,
    #[str("CoeurArdent")]
    CoeurArdent,
    #[str("Kamikaze")]
    Kamikaze,
    #[str("Sprint")]
    Sprint,
    #[str("ProteinesDinozienne")]
    ProteinesDinozienne,
    #[str("Extenuation")]
    Extenuation,
    #[str("CatapaceDeMagma")]
    CatapaceDeMagma,
    #[str("Carapace")]
    Carapace,
    #[str("Sauvagerie")]
    Sauvagerie,
    #[str("Endurance")]
    Endurance,
    #[str("Vignes")]
    Vignes,
    #[str("RenfortsKorgon")]
    RenfortsKorgon,
    #[str("Sympathique")]
    Sympathique,
    #[str("Croissance")]
    Croissance,
    #[str("EtatPrimal")]
    EtatPrimal,
    #[str("InstinctSauvage")]
    InstinctSauvage,
    #[str("LargeMachoire")]
    LargeMachoire,
    #[str("Acrobate")]
    Acrobate,
    #[str("Choc")]
    Choc,
    #[str("OxygenationMusculaire")]
    OxygenationMusculaire,
    #[str("Vert")]
    Vert,
    #[str("SourceDeVie")]
    SourceDeVie,
    #[str("AcideLactique")]
    AcideLactique,
    #[str("LancerDeRoche")]
    LancerDeRoche,
    #[str("PeauDeFer")]
    PeauDeFer,
    #[str("RiviereDeVie")]
    RiviereDeVie,
    #[str("Champollion")]
    Champollion,
    #[str("ReceptacleAqueux")]
    ReceptacleAqueux,
    #[str("PeauDacier")]
    PeauDacier,
    #[str("MurDeBoue")]
    MurDeBoue,
    #[str("Mutation")]
    Mutation,
    #[str("PocheVentrale")]
    PocheVentrale,
    #[str("Intelligence")]
    Intelligence,
    #[str("Focus")]
    Focus,
    #[str("Celerite")]
    Celerite,
    #[str("Concentration")]
    Concentration,
    #[str("AttaqueEclair")]
    AttaqueEclair,
    #[str("CoupDouble")]
    CoupDouble,
    #[str("PremiersSoins")]
    PremiersSoins,
    #[str("Foudre")]
    Foudre,
    #[str("VoieDeKaos")]
    VoieDeKaos,
    #[str("PlanDeCarriere")]
    PlanDeCarriere,
    #[str("Adrenaline")]
    Adrenaline,
    #[str("VoieDeGaia")]
    VoieDeGaia,
    #[str("DanseFoudroyante")]
    DanseFoudroyante,
    #[str("Embuche")]
    Embuche,
    #[str("Surcharge")]
    Surcharge,
    #[str("SoutienMoral")]
    SoutienMoral,
    #[str("Jaune")]
    Jaune,
    #[str("CrampeChronique")]
    CrampeChronique,
    #[str("BarriereElectrifiee")]
    BarriereElectrifiee,
    #[str("Agilite")]
    Agilite,
    #[str("Esquive")]
    Esquive,
    #[str("Elasticite")]
    Elasticite,
    #[str("MaitriseCorporelle")]
    MaitriseCorporelle,
    #[str("CompetenceDouble")]
    CompetenceDouble,
    #[str("FrenesieCollective")]
    FrenesieCollective,
    #[str("LimiteBrisee")]
    LimiteBrisee,
    #[str("Waikikido")]
    Waikikido,
    #[str("Vigilance")]
    Vigilance,
    #[str("PaumeChalumeau")]
    PaumeChalumeau,
    #[str("ChefDeGuerre")]
    ChefDeGuerre,
    #[str("Belier")]
    Belier,
    #[str("ChasseurDeGoupignon")]
    ChasseurDeGoupignon,
    #[str("SouffleArdent")]
    SouffleArdent,
    #[str("ChasseurDeGeant")]
    ChasseurDeGeant,
    #[str("CouleeDeLave")]
    CouleeDeLave,
    #[str("BouleDeFeu")]
    BouleDeFeu,
    #[str("Combustion")]
    Combustion,
    #[str("ChasseurDeDragon")]
    ChasseurDeDragon,
    #[str("Torche")]
    Torche,
    #[str("AuraIncandescente")]
    AuraIncandescente,
    #[str("Vengeance")]
    Vengeance,
    #[str("Sieste")]
    Sieste,
    #[str("Meteores")]
    Meteores,
    #[str("SelfControl")]
    SelfControl,
    #[str("Brave")]
    Brave,
    #[str("Planificateur")]
    Planificateur,
    #[str("HeritageFaroe")]
    HeritageFaroe,
    #[str("PrintempsPrecoce")]
    PrintempsPrecoce,
    #[str("EspritGorilloz")]
    EspritGorilloz,
    #[str("GardeForestier")]
    GardeForestier,
    #[str("Fouille")]
    Fouille,
    #[str("Detective")]
    Detective,
    #[str("ExpertEnFouille")]
    ExpertEnFouille,
    #[str("Cocon")]
    Cocon,
    #[str("Geant")]
    Geant,
    #[str("Archeologue")]
    Archeologue,
    #[str("Colosse")]
    Colosse,
    #[str("Tenacite")]
    Tenacite,
    #[str("Charisme")]
    Charisme,
    #[str("ResistanceALaMagie")]
    ResistanceALaMagie,
    #[str("Ingenieur")]
    Ingenieur,
    #[str("CanonAEau")]
    CanonAEau,
    #[str("Gel")]
    Gel,
    #[str("DoucheEcossaise")]
    DoucheEcossaise,
    #[str("ZeroAbsolu")]
    ZeroAbsolu,
    #[str("Petrification")]
    Petrification,
    #[str("Acupuncture")]
    Acupuncture,
    #[str("Sapeur")]
    Sapeur,
    #[str("RayonKaarSher")]
    RayonKaarSher,
    #[str("Magasinier")]
    Magasinier,
    #[str("Perception")]
    Perception,
    #[str("CoupsSournois")]
    CoupsSournois,
    #[str("ApprentiPecheur")]
    ApprentiPecheur,
    #[str("CoupFatal")]
    CoupFatal,
    #[str("EntrainementSousMarin")]
    EntrainementSousMarin,
    #[str("PecheurConfirme")]
    PecheurConfirme,
    #[str("Marecage")]
    Marecage,
    #[str("EntrainementSousMarinAvance")]
    EntrainementSousMarinAvance,
    #[str("MaitrePecheur")]
    MaitrePecheur,
    #[str("MaitreNageur")]
    MaitreNageur,
    #[str("KarateSousMarin")]
    KarateSousMarin,
    #[str("Sumo")]
    Sumo,
    #[str("SansPitie")]
    SansPitie,
    #[str("CloneAqueux")]
    CloneAqueux,
    #[str("GriffesEmpoisonnes")]
    GriffesEmpoisonnes,
    #[str("Cuisinier")]
    Cuisinier,
    #[str("SangAcide")]
    SangAcide,
    #[str("Regenerescence")]
    Regenerescence,
    #[str("PureeSalvatrice")]
    PureeSalvatrice,
    #[str("AuraHermetique")]
    AuraHermetique,
    #[str("CrepusculeFlambloyant")]
    CrepusculeFlambloyant,
    #[str("AubeFeuillue")]
    AubeFeuillue,
    #[str("Benediction")]
    Benediction,
    #[str("ArchangeCorrosif")]
    ArchangeCorrosif,
    #[str("ArchangeGenesif")]
    ArchangeGenesif,
    #[str("Pretre")]
    Pretre,
    #[str("Paratonnerre")]
    Paratonnerre,
    #[str("Medecine")]
    Medecine,
    #[str("FissionElementaire")]
    FissionElementaire,
    #[str("CrocsDiamant")]
    CrocsDiamant,
    #[str("Brancardier")]
    Brancardier,
    #[str("Marchand")]
    Marchand,
    #[str("Reincarnation")]
    Reincarnation,
    #[str("Saut")]
    Saut,
    #[str("DisqueVacuum")]
    DisqueVacuum,
    #[str("AttaquePlongeante")]
    AttaquePlongeante,
    #[str("Furtivite")]
    Furtivite,
    #[str("TrouNoir")]
    TrouNoir,
    #[str("MaitreLevitateur")]
    MaitreLevitateur,
    #[str("Strategie")]
    Strategie,
    #[str("Analyse")]
    Analyse,
    #[str("Cueillette")]
    Cueillette,
    #[str("Specialiste")]
    Specialiste,
    #[str("TalonDAchille")]
    TalonDAchille,
    #[str("OeilDeLynx")]
    OeilDeLynx,
    #[str("NuageToxique")]
    NuageToxique,
    #[str("HalineFetive")]
    HalineFetive,
    #[str("Mistral")]
    Mistral,
    #[str("TaiChi")]
    TaiChi,
    #[str("Tornade")]
    Tornade,
    #[str("Eveil")]
    Eveil,
    #[str("PaumeEjectable")]
    PaumeEjectable,
    #[str("VentVif")]
    VentVif,
    #[str("FormeVaporeuse")]
    FormeVaporeuse,
    #[str("Professeur")]
    Professeur,
    #[str("MeditationSolitaire")]
    MeditationSolitaire,
    #[str("MeditationTranscendantale")]
    MeditationTranscendantale,
    #[str("SouffleDeVie")]
    SouffleDeVie,
    #[str("FormeEtherale")]
    FormeEtherale,
    #[str("Brasero")]
    Brasero,
    #[str("Detonation")]
    Detonation,
    #[str("CoeurDuPhenix")]
    CoeurDuPhenix,
    #[str("LanceurDeGland")]
    LanceurDeGland,
    #[str("Gratteur")]
    Gratteur,
    #[str("GrosseBeigne")]
    GrosseBeigne,
    #[str("Vitalite")]
    Vitalite,
    #[str("MoignonsLiquides")]
    MoignonsLiquides,
    #[str("Deluge")]
    Deluge,
    #[str("Reflex")]
    Reflex,
    #[str("EclairSinueux")]
    EclairSinueux,
    #[str("Survie")]
    Survie,
    #[str("Aiguillon")]
    Aiguillon,
    #[str("AuraPuante")]
    AuraPuante,
    #[str("Hypnose")]
    Hypnose,
    #[str("Vendetta")]
    Vendetta,
    #[str("Secousse")]
    Secousse,
    #[str("Bulle")]
    Bulle,
    #[str("Electrolyse")]
    Electrolyse,
    #[str("MaitreElementaire")]
    MaitreElementaire,
    #[str("Infatiguable")]
    Infatiguable,
    #[str("ArmureDeBasalte")]
    ArmureDeBasalte,
    #[str("Coque")]
    Coque,
    #[str("ChargeCornue")]
    ChargeCornue,
    #[str("Rock")]
    Rock,
    #[str("Pietinement")]
    Pietinement,
    #[str("Cuirasse")]
    Cuirasse,
    #[str("Insaisissable")]
    Insaisissable,
    #[str("DeplacementInstantane")]
    DeplacementInstantane,
    #[str("Napomagicien")]
    Napomagicien,
    #[str("EcaillesLuminescentes")]
    EcaillesLuminescentes,
    #[str("PeauDeSerpent")]
    PeauDeSerpent,
    #[str("PropulsionDivine")]
    PropulsionDivine,
    #[str("GriffesInfernales")]
    GriffesInfernales,
    #[str("Rouge")]
    Rouge,
    #[str("CriDeGuerre")]
    CriDeGuerre,
    #[str("FievreBrulante")]
    FievreBrulante,
    #[str("BenedictionDArtemis")]
    BenedictionDArtemis,
    #[str("Joker")]
    Joker,
    #[str("ArmureDeFeu")]
    ArmureDeFeu,
    #[str("PaysDeCendre")]
    PaysDeCendre,
    #[str("ReceptacleRocheux")]
    ReceptacleRocheux,
    #[str("PlumesDePhoenix")]
    PlumesDePhoenix,
    #[str("AcclamationFraternelle")]
    AcclamationFraternelle,
    #[str("PoingDeFeu")]
    PoingDeFeu,
    #[str("VideEnergetique")]
    VideEnergetique,
    #[str("BouclierDinoz")]
    BouclierDinoz,
    #[str("Courbatures")]
    Courbatures,
    #[str("ForceControl")]
    ForceControl,
    #[str("CourantDeVie")]
    CourantDeVie,
    #[str("Berserk")]
    Berserk,
    #[str("Sharignan")]
    Sharignan,
    #[str("Amazonie")]
    Amazonie,
    #[str("EauDivine")]
    EauDivine,
    #[str("RadiationsGamma")]
    RadiationsGamma,
    #[str("Bleu")]
    Bleu,
    #[str("MueAcqueuse")]
    MueAcqueuse,
    #[str("CarapaceBlindee")]
    CarapaceBlindee,
    #[str("DieteChromatique")]
    DieteChromatique,
    #[str("EffluveAphrodisiaque")]
    EffluveAphrodisiaque,
    #[str("Nemo")]
    Nemo,
    #[str("Cleptomane")]
    Cleptomane,
    #[str("Abysse")]
    Abysse,
    #[str("BanniDesDieux")]
    BanniDesDieux,
    #[str("TourbillonMagique")]
    TourbillonMagique,
    #[str("Hyperventilation")]
    Hyperventilation,
    #[str("TherapieDeGroupe")]
    TherapieDeGroupe,
    #[str("ReceptacleTesla")]
    ReceptacleTesla,
    #[str("VitaliteMarine")]
    VitaliteMarine,
    #[str("StimulationCardiaque")]
    StimulationCardiaque,
    #[str("MorsureDuSoleil")]
    MorsureDuSoleil,
    #[str("BatterieSupplementaire")]
    BatterieSupplementaire,
    #[str("Einstein")]
    Einstein,
    #[str("ReceptacleAerien")]
    ReceptacleAerien,
    #[str("FeuDeStElme")]
    FeuDeStElme,
    #[str("Oracle")]
    Oracle,
    #[str("ForceDeZeus")]
    ForceDeZeus,
    #[str("RemanenceHertzienne")]
    RemanenceHertzienne,
    #[str("Blanc")]
    Blanc,
    #[str("Anaerobie")]
    Anaerobie,
    #[str("DoubleFace")]
    DoubleFace,
    #[str("Flagellation")]
    Flagellation,
    #[str("SouffleDange")]
    SouffleDange,
    #[str("Ouragan")]
    Ouragan,
    #[str("Ouranos")]
    Ouranos,
    #[str("Twinoid500mg")]
    Twinoid500mg,
    #[str("LonDuhaut")]
    LonDuhaut,
    #[str("SurplisDhades")]
    SurplisDhades,
    #[str("ReceptacleThermique")]
    ReceptacleThermique,
    #[str("QiGong")]
    QiGong,
    #[str("Sylphides")]
    Sylphides,
    #[str("Messie")]
    Messie,
    #[str("Mutinerie")]
    Mutinerie,
    #[str("MainsCollantes")]
    MainsCollantes,
    #[str("GrosCostaud")]
    GrosCostaud,
    #[str("OrigineCaushemeshenne")]
    OrigineCaushemeshenne,
    #[str("Ecrasement")]
    Ecrasement,
    #[str("ForceDeLumiere")]
    ForceDeLumiere,
    #[str("ChargePigmou")]
    ChargePigmou,
    #[str("ForceDesTenebres")]
    ForceDesTenebres,
    #[str("Bigmagnon")]
    Bigmagnon,
    #[str("DurACuire")]
    DurACuire,
    #[str("LoupGarou")]
    LoupGarou,
    #[str("Salamandre")]
    Salamandre,
    #[str("BenedictionDesFees")]
    BenedictionDesFees,
    #[str("Bouddha")]
    Bouddha,
    #[str("TotemAncestralAeroporte")]
    TotemAncestralAeroporte,
    #[str("Leviathan")]
    Leviathan,
    #[str("Vulcain")]
    Vulcain,
    #[str("Fujin")]
    Fujin,
    #[str("ArmureDIfrit")]
    ArmureDIfrit,
    #[str("Raijin")]
    Raijin,
    #[str("Djinn")]
    Djinn,
    #[str("Quetzacoatl")]
    Quetzacoatl,
    #[str("Golem")]
    Golem,
    #[str("Hades")]
    Hades,
    #[str("Ondine")]
    Ondine,
    #[str("Hercolubus")]
    Hercolubus,
    #[str("ReineDeLaRuche")]
    ReineDeLaRuche,
    #[str("RoiDesSinges")]
    RoiDesSinges,
    #[str("BigMama")]
    BigMama,
    #[str("Yggdrasil")]
    Yggdrasil,
    #[str("BaleineBlanche")]
    BaleineBlanche,
    #[str("Invocateur")]
    Invocateur,
    #[str("Envol")]
    Envol,
    #[str("PoingDeBraise")]
    PoingDeBraise,
    #[str("Leader")]
    Leader,
  }
  pub type ParseError = DinorpgSkillParseError;
  const SQL_NAME = "dinorpg_skill";
);
