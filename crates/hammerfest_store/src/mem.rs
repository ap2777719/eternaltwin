use async_trait::async_trait;
use etwin_core::clock::Clock;
use etwin_core::core::Instant;
use etwin_core::email::EmailAddress;
use etwin_core::hammerfest::{
  GetHammerfestUserOptions, HammerfestForumThemePageResponse, HammerfestForumThreadPageResponse, HammerfestGodchild,
  HammerfestGodchildrenResponse, HammerfestInventoryResponse, HammerfestItemId, HammerfestProfileResponse,
  HammerfestSessionUser, HammerfestShop, HammerfestShopResponse, HammerfestStore, HammerfestUserId,
  ShortHammerfestUser, StoredHammerfestProfile, StoredHammerfestUser,
};
use etwin_core::temporal::{LatestTemporal, SnapshotLog};
use etwin_core::types::AnyError;
use std::collections::{BTreeMap, HashMap};
use std::sync::RwLock;

fn clone_latest_temporal<T: Clone + Eq>(log: &SnapshotLog<T>) -> Option<LatestTemporal<T>> {
  log.latest().map(|l| LatestTemporal { latest: l.cloned() })
}

struct StoreState {
  users: HashMap<HammerfestUserId, MemUser>,
}

struct MemUser {
  short: ShortHammerfestUser,
  archived_at: Instant,
  email: SnapshotLog<Option<EmailAddress>>,
  profile: SnapshotLog<StoredHammerfestProfile>,
  inventory: SnapshotLog<BTreeMap<HammerfestItemId, u32>>,
  tokens: SnapshotLog<u32>,
  shop: SnapshotLog<HammerfestShop>,
  godchildren: SnapshotLog<Vec<HammerfestGodchild>>,
}

impl MemUser {
  fn new(time: Instant, user: ShortHammerfestUser) -> Self {
    MemUser {
      short: user,
      archived_at: time,
      email: SnapshotLog::new(),
      profile: SnapshotLog::new(),
      inventory: SnapshotLog::new(),
      tokens: SnapshotLog::new(),
      shop: SnapshotLog::new(),
      godchildren: SnapshotLog::new(),
    }
  }

  fn to_archived(&self) -> StoredHammerfestUser {
    StoredHammerfestUser {
      server: self.short.server,
      id: self.short.id,
      username: self.short.username.clone(),
      archived_at: self.archived_at,
      profile: clone_latest_temporal(&self.profile),
      inventory: clone_latest_temporal(&self.inventory),
    }
  }
}

impl StoreState {
  fn new() -> Self {
    Self { users: HashMap::new() }
  }

  fn get_user(&self, id: &HammerfestUserId) -> Option<&MemUser> {
    self.users.get(id)
  }

  fn touch_user(&mut self, time: Instant, user: ShortHammerfestUser) -> &mut MemUser {
    self.users.entry(user.id).or_insert_with(|| MemUser::new(time, user))
  }

  fn touch_session(&mut self, time: Instant, sess: HammerfestSessionUser) -> &mut MemUser {
    let user = self.touch_user(time, sess.user);
    user.tokens.snapshot(time, sess.tokens);
    user
  }
}

pub struct MemHammerfestStore<TyClock: Clock> {
  clock: TyClock,
  state: RwLock<StoreState>,
}

impl<TyClock> MemHammerfestStore<TyClock>
where
  TyClock: Clock,
{
  pub fn new(clock: TyClock) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock> HammerfestStore for MemHammerfestStore<TyClock>
where
  TyClock: Clock,
{
  async fn get_short_user(&self, options: &GetHammerfestUserOptions) -> Result<Option<ShortHammerfestUser>, AnyError> {
    let state = self.state.read().unwrap();
    Ok(state.get_user(&options.id).map(|u| u.short.clone()))
  }

  async fn get_user(&self, options: &GetHammerfestUserOptions) -> Result<Option<StoredHammerfestUser>, AnyError> {
    let state = self.state.read().unwrap();
    Ok(state.get_user(&options.id).map(|u| u.to_archived()))
  }

  async fn touch_short_user(&self, short: &ShortHammerfestUser) -> Result<StoredHammerfestUser, AnyError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.now();
    let user = state.touch_user(now, short.clone());
    Ok(user.to_archived())
  }

  async fn touch_shop(&self, response: &HammerfestShopResponse) -> Result<(), AnyError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.now();
    let user = state.touch_session(now, response.session.clone());
    user.shop.snapshot(now, response.shop.clone());
    Ok(())
  }

  async fn touch_profile(&self, response: &HammerfestProfileResponse) -> Result<(), AnyError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.now();

    let (user, profile) = match (&response.session, &response.profile) {
      (Some(sess), prof) => (state.touch_session(now, sess.clone()), prof.as_ref()),
      (None, Some(prof)) => (state.touch_user(now, prof.user.clone()), Some(prof)),
      (None, None) => return Ok(()),
    };

    if let Some(profile) = profile {
      user.profile.snapshot(
        now,
        StoredHammerfestProfile {
          best_score: profile.best_score,
          best_level: profile.best_level.into(),
          game_completed: profile.has_carrot,
          items: profile.items.clone(),
          quests: profile.quests.clone(),
        },
      );

      if let Some(email) = &profile.email {
        user.email.snapshot(now, email.clone());
      }
    }

    Ok(())
  }

  async fn touch_inventory(&self, response: &HammerfestInventoryResponse) -> Result<(), AnyError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.now();
    let user = state.touch_session(now, response.session.clone());
    user.inventory.snapshot(now, response.inventory.clone());
    Ok(())
  }

  async fn touch_godchildren(&self, response: &HammerfestGodchildrenResponse) -> Result<(), AnyError> {
    let mut state = self.state.write().unwrap();
    let now = self.clock.now();
    let user = state.touch_session(now, response.session.clone());
    user.godchildren.snapshot(now, response.godchildren.clone());
    Ok(())
  }

  async fn touch_theme_page(&self, _response: &HammerfestForumThemePageResponse) -> Result<(), AnyError> {
    eprintln!("Stub: Missing `MemHammerfestSore::touch_theme_page` implementation");
    Ok(())
  }

  async fn touch_thread_page(&self, _response: &HammerfestForumThreadPageResponse) -> Result<(), AnyError> {
    eprintln!("Stub: Missing `MemHammerfestSore::touch_thread_page` implementation");
    Ok(())
  }
}

#[cfg(feature = "neon")]
impl<TyClock> neon::prelude::Finalize for MemHammerfestStore<TyClock> where TyClock: Clock {}

#[cfg(test)]
mod test {
  use crate::mem::MemHammerfestStore;
  use crate::test::TestApi;
  use etwin_core::clock::VirtualClock;
  use etwin_core::core::Instant;
  use etwin_core::hammerfest::HammerfestStore;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn HammerfestStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let hammerfest_store: Arc<dyn HammerfestStore> = Arc::new(MemHammerfestStore::new(Arc::clone(&clock)));

    TestApi {
      clock,
      hammerfest_store,
    }
  }

  test_hammerfest_store!(|| make_test_api());
}
