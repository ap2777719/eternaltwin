use async_trait::async_trait;
use etwin_core::twinoid::api::User;
use etwin_core::twinoid::{
  TwinoidApiAuth, TwinoidClient, TwinoidClientCreateSessionError, TwinoidCredentials, TwinoidSession,
};
use etwin_core::types::AnyError;

pub struct MemTwinoidClient {}

impl MemTwinoidClient {
  pub fn new() -> Self {
    Self {}
  }
}

impl Default for MemTwinoidClient {
  fn default() -> Self {
    Self::new()
  }
}

#[async_trait]
impl TwinoidClient for MemTwinoidClient {
  async fn get_me_short(&self, _auth: TwinoidApiAuth) -> Result<User, AnyError> {
    todo!()
  }

  async fn create_session(
    &self,
    _options: &TwinoidCredentials,
  ) -> Result<TwinoidSession, TwinoidClientCreateSessionError> {
    todo!()
  }
}
