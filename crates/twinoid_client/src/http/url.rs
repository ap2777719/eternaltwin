use reqwest::Url;

pub struct TwinoidUrls {
  root: Url,
}

impl TwinoidUrls {
  pub fn new() -> Self {
    Self {
      root: Url::parse("https://twinoid.com").expect("failed to parse twinoid root URL"),
    }
  }

  fn make_url(&self, segments: &[&str]) -> Url {
    let mut url = self.root.clone();
    url.path_segments_mut().expect("invalid root url").extend(segments);
    url
  }

  pub fn host(&self) -> &'static str {
    "twinoid.com"
  }

  pub fn me(&self) -> Url {
    self.make_url(&["graph", "me"])
  }

  pub fn login(&self) -> Url {
    self.make_url(&["user", "login"])
  }

  pub fn bar_view(&self) -> Url {
    self.make_url(&["bar", "view"])
  }

  /// Cookie policy page
  ///
  /// A short page, perfect to grab a cookie.
  pub fn cookies(&self) -> Url {
    self.make_url(&["tid", "cookies"])
  }
}

impl Default for TwinoidUrls {
  fn default() -> Self {
    Self::new()
  }
}
