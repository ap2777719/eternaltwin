use crate::http::errors::ScraperError;
use etwin_scraper_tools::{selector, ElementRefExt};
use itertools::Itertools;
use once_cell::sync::Lazy;
use regex::Regex;
use scraper::Html;
use scraper::Selector;
use serde::Deserialize;
#[cfg(test)]
use serde::Serialize;
use std::collections::BTreeMap;
use url::Url;

/// Regular expression for the Twinoid config statement
static TID_CONFIG_RE: Lazy<Regex> = Lazy::new(|| Regex::new(r#"^__tid = (\{[^\n]*});$"#).unwrap());

/// Regular expression for Twinoid bar redirection
static TID_BAR_REDIRECT_RE: Lazy<Regex> = Lazy::new(|| Regex::new(r#"^\s*_tid.redirect\(('[^\n]*')\);\s*$"#).unwrap());

#[cfg_attr(test, derive(Serialize, Deserialize), serde(tag = "type"))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) enum LoginResponse {
  /// Flash form data for the login redirector
  Flash(LoginResponseFlash),
  /// Wrong credentials error page
  WrongCredentials,
}

#[cfg_attr(test, derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct LoginResponseFlash {
  /// Target URI, relative to the origin URI
  pub target: String,
  /// POST body
  pub form: BTreeMap<String, String>,
}

pub(crate) fn scrape_login_response(doc: &Html) -> Result<LoginResponse, ScraperError> {
  let root = doc.root_element();

  let flash_form = root.select(selector!("form#flogin")).next();
  let logger_form = root.select(selector!("form#logger")).next();

  match (flash_form, logger_form) {
    (None, None) => Err(ScraperError::MissingLoginResponseForms),
    (Some(_), Some(_)) => Err(ScraperError::AmbiguousLoginResponseForms),
    (Some(flash_form), None) => {
      let action = flash_form
        .value()
        .attr("action")
        .ok_or(ScraperError::MissingLoginFormAction)?;
      let mut inputs = BTreeMap::new();
      for input in flash_form.select(selector!(":scope > input")) {
        let input = input.value();
        match input.attr("type") {
          None | Some("text") | Some("password") | Some("hidden") => {
            let name = input.attr("name").ok_or(ScraperError::MissingLoginFormInputName)?;
            let value = input.attr("value").unwrap_or("");
            inputs.insert(name.to_string(), value.to_string());
          }
          Some("submit") => {
            // Skip
          }
          Some(typ) => return Err(ScraperError::UnexpectedLoginFormInputType(typ.to_string())),
        }
      }

      Ok(LoginResponse::Flash(LoginResponseFlash {
        target: action.to_string(),
        form: inputs,
      }))
    }
    (None, Some(_)) => {
      // TODO: Check there is a "wrong credentials" error
      Ok(LoginResponse::WrongCredentials)
    }
  }
}

#[cfg_attr(test, derive(Serialize))]
#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct TidConfig {
  pub lang: String,
  pub chk: String,
  pub ver: String,
  pub infos: String,
}

pub fn scrape_tid_config(doc: &Html) -> Result<TidConfig, ScraperError> {
  let root = doc.root_element();

  let config = root
    .select(selector!("script[type=\"text/javascript\"]"))
    .filter_map(|e| match e.get_one_text() {
      Ok(t) => TID_CONFIG_RE.captures(t),
      Err(_) => None,
    })
    .map(|c| c.get(1).expect("regex has capture group 1").as_str())
    .exactly_one()
    .map_err(|_| ScraperError::NonUniqueTidConfig)?;

  let config: TidConfig = json5::from_str(config).map_err(|_| ScraperError::InvalidTidConfig)?;

  Ok(config)
}

#[cfg_attr(test, derive(Deserialize, Serialize), serde(tag = "type"))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum TidBarView {
  Redirect(TidBarViewRedirect),
}

#[cfg_attr(test, derive(Deserialize, Serialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TidBarViewRedirect {
  pub url: Url,
}

pub fn scrape_tid_bar_view(script: &str) -> Result<TidBarView, ScraperError> {
  let redirect_arg = TID_BAR_REDIRECT_RE
    .captures(script)
    .ok_or(ScraperError::NonRedirectTidBarView)?
    .get(1)
    .expect("regex has capture group 1")
    .as_str();

  let redirect: String = json5::from_str(redirect_arg).map_err(|_| ScraperError::InvalidRedirectArg)?;
  let redirect: Url = redirect.parse().map_err(|_| ScraperError::InvalidRedirectArg)?;

  Ok(TidBarView::Redirect(TidBarViewRedirect { url: redirect }))
}

#[cfg(test)]
mod test {
  use crate::http::scraper::{
    scrape_login_response, scrape_tid_bar_view, scrape_tid_config, LoginResponse, TidBarView, TidConfig,
  };
  use scraper::Html;
  use std::path::{Path, PathBuf};
  use test_generator::test_resources;

  #[test_resources("./test-resources/scraping/twinoid/login-response/*/")]
  fn test_scrape_login_response(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let html_path = path.join("main.html");
    let actual_path = path.join("rs.actual.json");

    let raw_html = ::std::fs::read_to_string(html_path).expect("Failed to read html file");

    let html = Html::parse_document(&raw_html);

    let actual = scrape_login_response(&html).unwrap();
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected = serde_json::from_str::<LoginResponse>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }

  #[test_resources("./test-resources/scraping/twinoid/tid-config/*/")]
  fn test_scrape_tid_config(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let html_path = path.join("main.html");
    let actual_path = path.join("rs.actual.json");

    let raw_html = ::std::fs::read_to_string(html_path).expect("Failed to read html file");

    let html = Html::parse_document(&raw_html);

    let actual = scrape_tid_config(&html).unwrap();
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected = serde_json::from_str::<TidConfig>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }

  #[test_resources("./test-resources/scraping/twinoid/bar-view/*/")]
  fn test_scrape_tid_bar_view(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let js_path = path.join("main.js");
    let actual_path = path.join("rs.actual.json");

    let raw_js = ::std::fs::read_to_string(js_path).expect("Failed to read js file");

    let actual = scrape_tid_bar_view(&raw_js).unwrap();
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected = serde_json::from_str::<TidBarView>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }
}
