use etwin_core::types::AnyError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum ScraperError {
  #[error("HTTP Error")]
  HttpError(#[from] reqwest::Error),
  #[error("login response has neither `form#flogin` nor `form#logger`")]
  MissingLoginResponseForms,
  #[error("login response has both `form#flogin` and `form#logger`")]
  AmbiguousLoginResponseForms,
  #[error("Missing login form `action` attribute")]
  MissingLoginFormAction,
  #[error("Missing login form input `name` attribute")]
  MissingLoginFormInputName,
  #[error("Unexpected login form input `type` attribute: {0:?}")]
  UnexpectedLoginFormInputType(String),
  #[error("non-unique `__tid` config initializer")]
  NonUniqueTidConfig,
  #[error("failed to parse `__tid` config initializer")]
  InvalidTidConfig,
  #[error("Twinoid bar view is not a redirection")]
  NonRedirectTidBarView,
  #[error("invalid Twinoid bar view redirection argument")]
  InvalidRedirectArg,
  #[error("OtherScraperError")]
  Other(#[from] AnyError),
}
