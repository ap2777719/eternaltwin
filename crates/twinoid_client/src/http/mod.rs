mod errors;
mod scraper;
mod url;

pub use crate::http::errors::ScraperError;
use crate::http::scraper::{scrape_login_response, LoginResponse};
pub use crate::http::scraper::{scrape_tid_bar_view, scrape_tid_config, TidBarView, TidBarViewRedirect, TidConfig};
pub use crate::http::url::TwinoidUrls;
use ::url::Url;
use async_trait::async_trait;
use etwin_core::clock::Clock;
use etwin_core::twinoid::{
  api, ShortTwinoidUser, TwinoidClientCreateSessionError, TwinoidCredentials, TwinoidSession,
  TwinoidSessionKeyParseError, TwinoidUserDisplayName, TwinoidUserId, TwinoidUserIdParseError,
};
use etwin_core::twinoid::{TwinoidApiAuth, TwinoidClient};
use etwin_core::types::{to_any_error, AnyError};
use etwin_mt_querystring::UrlExt;
use reqwest::{Client, StatusCode};
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use std::time::Duration;
use thiserror::Error;

const USER_AGENT: &str = "EtwinTwinoidClient";
const TIMEOUT: Duration = Duration::from_millis(5000);

pub struct HttpTwinoidClient<TyClock> {
  client: Client,
  #[allow(unused)]
  clock: TyClock,
}

impl<TyClock> HttpTwinoidClient<TyClock>
where
  TyClock: Clock,
{
  pub fn new(clock: TyClock) -> Result<Self, AnyError> {
    Ok(Self {
      client: Client::builder()
        .user_agent(USER_AGENT)
        .timeout(TIMEOUT)
        .redirect(reqwest::redirect::Policy::none())
        .build()?,
      clock,
    })
  }
}

#[derive(Debug, Error)]
enum HttpTwinoidClientCreateSessionError {
  #[error("failed to send login request")]
  LoginRequest(#[source] reqwest::Error),
  #[error("unexpected status for login request: {0:?}")]
  LoginStatus(StatusCode),
  #[error("login response is missing session cookie")]
  SessionKeyNotFound,
  #[error("session cookie value is invalid")]
  InvalidSessionKey(#[source] TwinoidSessionKeyParseError),
  #[error("failed to download login response")]
  LoginResponseDownload(#[source] reqwest::Error),
  #[error("failed to read login response")]
  LoginResponseRead(#[source] ScraperError),
  #[error("malformed login redirector url")]
  MalformedRedirectorUrl(#[source] ::url::ParseError),
  #[error("login redirector has unexpected host")]
  RedirectorHost,
  #[error("failed to send redirector request")]
  RedirectorRequest(#[source] reqwest::Error),
  #[error("unexpected status for login redirector request: {0:?}")]
  RedirectorStatus(StatusCode),
  #[error("redirector response is missing location header")]
  RedirectorLocationNotFound,
  #[error("redirector response location header has invalid encoding")]
  RedirectorLocationEncoding(#[source] reqwest::header::ToStrError),
  #[error("malformed login return url")]
  MalformedReturnUrl(#[source] ::url::ParseError),
  #[error("login return has unexpected host")]
  ReturnHost,
  #[error("login return is missing user infos query parameter")]
  InfosNotFound,
  #[error("login return user infos value is invalid")]
  InvalidInfos(#[source] TwinoidUserInfosParseError),
}

impl From<HttpTwinoidClientCreateSessionError> for TwinoidClientCreateSessionError {
  fn from(err: HttpTwinoidClientCreateSessionError) -> Self {
    Self::Other(to_any_error(err))
  }
}

#[async_trait]
impl<TyClock> TwinoidClient for HttpTwinoidClient<TyClock>
where
  TyClock: Clock,
{
  async fn get_me_short(&self, auth: TwinoidApiAuth) -> Result<api::User, AnyError> {
    let mut url = TwinoidUrls::new().me();
    {
      let mut qs = url.query_pairs_mut();
      if let TwinoidApiAuth::Token(ref token) = &auth {
        qs.append_pair("access_token", token.as_str());
      };
      qs.append_pair("fields", "id,name,title");
    }

    let req = self.client.get(url);
    let res = req.send().await?;

    let body = res.bytes().await?;

    match serde_json::from_slice(&body) {
      Ok(res) => Ok(res),
      Err(_e) => {
        // TODO: Handle errors such as `{"error":"invalid_token"}`
        let body = String::from_utf8_lossy(body.as_ref());
        Err(body.as_ref().into())
      }
    }
  }

  async fn create_session(
    &self,
    options: &TwinoidCredentials,
  ) -> Result<TwinoidSession, TwinoidClientCreateSessionError> {
    let tid_urls = TwinoidUrls::new();

    #[derive(Serialize)]
    struct LoginForm<'a> {
      login: &'a str,
      pass: &'a str,
      #[serde(rename = "keepSession")]
      keep_session: &'static str,
      submit: &'static str,
      host: &'static str,
      sid: &'a str,
      r#ref: &'static str,
      refid: &'static str,
      url: &'static str,
      mode: &'static str,
      proto: &'static str,
      /// Machine id (for tracking)
      mid: &'static str,
      /// Flash version
      fver: &'static str,
    }

    let form = LoginForm {
      login: options.email.as_str(),
      pass: options.password.as_str(),
      keep_session: "on",
      submit: "Login",
      host: tid_urls.host(),
      sid: "",
      r#ref: "",
      refid: "",
      url: "/",
      mode: "",
      proto: "https:",
      mid: "",
      fver: "100.0.0",
    };

    let login_url = tid_urls.login();
    let req = self
      .client
      .post(login_url.clone())
      .form(&form)
      .header(reqwest::header::HOST, tid_urls.host());

    let res = req
      .send()
      .await
      .map_err(HttpTwinoidClientCreateSessionError::LoginRequest)?;

    if res.status() != StatusCode::OK {
      return Err(HttpTwinoidClientCreateSessionError::LoginStatus(res.status()).into());
    }

    let session_key = res
      .cookies()
      .find(|cookie| cookie.name() == "tw_sid")
      .map(|cookie| cookie.value().to_owned())
      .ok_or(HttpTwinoidClientCreateSessionError::SessionKeyNotFound)?;

    let body = res
      .text()
      .await
      .map_err(HttpTwinoidClientCreateSessionError::LoginResponseDownload)?;

    let response = scrape_login_response(&::scraper::Html::parse_document(&body))
      .map_err(HttpTwinoidClientCreateSessionError::LoginResponseRead)?;

    let response = match response {
      LoginResponse::WrongCredentials => return Err(TwinoidClientCreateSessionError::WrongCredentials),
      LoginResponse::Flash(response) => response,
    };

    let redir_url = Url::options()
      .base_url(Some(&login_url))
      .parse(&response.target)
      .map_err(HttpTwinoidClientCreateSessionError::MalformedRedirectorUrl)?;
    if redir_url.host() != login_url.host() {
      return Err(HttpTwinoidClientCreateSessionError::RedirectorHost.into());
    }

    let req = self
      .client
      .post(redir_url.clone())
      .form(&response.form)
      .header(reqwest::header::REFERER, login_url.to_string())
      .header(
        reqwest::header::COOKIE,
        format!("curFeat=1; tw_sid={}; tp_login=1", session_key),
      )
      .header(reqwest::header::HOST, tid_urls.host());

    let res = req
      .send()
      .await
      .map_err(HttpTwinoidClientCreateSessionError::RedirectorRequest)?;

    if res.status() != StatusCode::FOUND {
      return Err(HttpTwinoidClientCreateSessionError::RedirectorStatus(res.status()).into());
    }

    let return_url = res
      .headers()
      .get(reqwest::header::LOCATION)
      .ok_or(HttpTwinoidClientCreateSessionError::RedirectorLocationNotFound)?
      .to_str()
      .map_err(HttpTwinoidClientCreateSessionError::RedirectorLocationEncoding)?;

    let return_url = Url::options()
      .base_url(Some(&redir_url))
      .parse(return_url)
      .map_err(HttpTwinoidClientCreateSessionError::MalformedReturnUrl)?;
    if return_url.host() != redir_url.host() {
      return Err(HttpTwinoidClientCreateSessionError::ReturnHost.into());
    }

    let infos = return_url
      .mt_query_pairs()
      .find_map(|(k, v)| if k == "infos" { Some(v) } else { None });
    let infos = infos.ok_or(HttpTwinoidClientCreateSessionError::InfosNotFound)?;

    let infos: TwinoidUserInfos = infos
      .as_ref()
      .parse()
      .map_err(HttpTwinoidClientCreateSessionError::InvalidInfos)?;

    Ok(TwinoidSession {
      key: session_key
        .parse()
        .map_err(HttpTwinoidClientCreateSessionError::InvalidSessionKey)?,
      user: ShortTwinoidUser {
        id: infos.twin_id,
        display_name: infos.name,
      },
    })
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct TwinoidUserInfos {
  pub phash: String,
  pub name: TwinoidUserDisplayName,
  pub twin_id: TwinoidUserId,
  pub rights: u32,
  pub force_create: bool,
  pub avatar: String,
  pub real_id: Option<u32>,
}

#[derive(Debug, Error)]
pub enum TwinoidUserInfosParseError {
  #[error("invalid input")]
  Invalid(#[from] haxeformat::HaxeError),
  #[error("invalid user id")]
  TwinoidUserId(#[from] TwinoidUserIdParseError),
}

#[derive(Debug, Error)]
pub enum TwinoidUserInfosFromHaxeError {
  #[error("root type is not a document")]
  InvalidRootType,
  #[error("missing field: {0:?}")]
  MissingField(&'static str),
  #[error("field {0:?} has invalid type")]
  InvalidFieldType(&'static str),
}

impl FromStr for TwinoidUserInfos {
  type Err = TwinoidUserInfosParseError;

  fn from_str(input: &str) -> Result<Self, Self::Err> {
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
    pub struct RawTwinoidUserInfos {
      pub phash: String,
      pub name: TwinoidUserDisplayName,
      #[serde(rename = "twinId")]
      pub twin_id: u32,
      pub rights: u32,
      #[serde(rename = "forceCreate")]
      pub force_create: bool,
      pub avatar: String,
      #[serde(rename = "realId")]
      pub real_id: Option<u32>,
    }

    let raw: RawTwinoidUserInfos = haxeformat::from_str(input)?;

    Ok(Self {
      phash: raw.phash,
      name: raw.name,
      twin_id: TwinoidUserId::new(raw.twin_id)?,
      rights: raw.rights,
      force_create: raw.force_create,
      avatar: raw.avatar,
      real_id: raw.real_id,
    })
  }
}

#[cfg(feature = "neon")]
impl<TyClock> neon::prelude::Finalize for HttpTwinoidClient<TyClock> where TyClock: Clock {}

#[cfg(test)]
mod test {
  use crate::http::TwinoidUserInfos;
  use etwin_core::twinoid::TwinoidUserId;

  #[test]
  fn test_parse() {
    let input = "oy5:phashy32:JT9Yv5rA0QdxY1e0R8w6oNREte4MtGzey4:langy2:fry4:namey8:Demurgosy6:rightszy11:forceCreatefy6:avatary71:%2F%2Fimgup.motion-twin.com%2Ftwinoid%2F8%2F4%2Fd7cff3d0_38_100x100.pngy6:realIdny5:tokeny32:9300b2f34a55989c739981cf115cb7fdy8:redirectny6:twinIdi38g";
    let actual: TwinoidUserInfos = input.parse().unwrap();
    let expected = TwinoidUserInfos {
      phash: "JT9Yv5rA0QdxY1e0R8w6oNREte4MtGze".to_string(),
      name: "Demurgos".parse().unwrap(),
      twin_id: TwinoidUserId::new(38).unwrap(),
      rights: 0,
      force_create: false,
      avatar: "//imgup.motion-twin.com/twinoid/8/4/d7cff3d0_38_100x100.png".to_string(),
      real_id: None,
    };
    assert_eq!(actual, expected);
  }

  #[test]
  fn test_parse2() {
    let input = "oy5:phashy32:CjVZmvliqGfNlLizTnPaK1slB8KPdFeEy4:langy2:fry4:namey8:Demurgosy6:rightszy11:forceCreatefy6:avatary71:%2F%2Fimgup.motion-twin.com%2Ftwinoid%2F8%2F4%2Fd7cff3d0_38_100x100.pngy6:realIdi1171056y5:tokeny32:dfedea62f95463500534832933431c85y8:redirectny6:twinIdi38g";
    let actual: TwinoidUserInfos = input.parse().unwrap();
    let expected = TwinoidUserInfos {
      phash: "CjVZmvliqGfNlLizTnPaK1slB8KPdFeE".to_string(),
      name: "Demurgos".parse().unwrap(),
      twin_id: TwinoidUserId::new(38).unwrap(),
      rights: 0,
      force_create: false,
      avatar: "//imgup.motion-twin.com/twinoid/8/4/d7cff3d0_38_100x100.png".to_string(),
      real_id: Some(1171056),
    };
    assert_eq!(actual, expected);
  }
}
