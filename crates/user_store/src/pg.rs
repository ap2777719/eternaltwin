use async_trait::async_trait;
use etwin_core::api::ApiRef;
use etwin_core::clock::Clock;
use etwin_core::core::{Instant, PeriodLower, RawUserDot, Secret};
use etwin_core::email::{touch_email_address, EmailAddress};
use etwin_core::password::PasswordHash;
use etwin_core::types::AnyError;
use etwin_core::user::{
  CompleteSimpleUser, CreateUserOptions, DeleteUserError, GetShortUserOptions, GetUserOptions, HardDeleteUserError,
  RawDeleteUser, RawGetUserResult, RawHardDeleteUser, RawUpdateUserError, RawUpdateUserOptions, ShortUser,
  ShortUserWithPassword, UserDisplayName, UserDisplayNameVersion, UserDisplayNameVersions, UserFields, UserId, UserRef,
  UserStore, Username, USERNAME_LOCK_DURATION, USER_DISPLAY_NAME_LOCK_DURATION, USER_PASSWORD_LOCK_DURATION,
};
use etwin_core::uuid::UuidGenerator;
use sqlx::postgres::PgPool;

pub struct PgUserStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  clock: TyClock,
  database: TyDatabase,
  database_secret: Secret,
  uuid_generator: TyUuidGenerator,
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgUserStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  pub fn new(clock: TyClock, database: TyDatabase, database_secret: Secret, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      database,
      database_secret,
      uuid_generator,
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> UserStore for PgUserStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
  async fn create_user(&self, options: &CreateUserOptions) -> Result<CompleteSimpleUser, AnyError> {
    let user_id = UserId::from_uuid(self.uuid_generator.next());
    let now = self.clock.now();

    let mut tx = self.database.as_ref().begin().await?;

    let row = {
      let email_hash = match &options.email {
        Some(email) => Some(touch_email_address(&mut tx, &self.database_secret, email, self.clock.now()).await?),
        None => None,
      };

      let r = {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          period: PeriodLower,
          is_administrator: bool,
        }
        // language=PostgreSQL
        sqlx::query_as::<_, Row>(
          r"
          WITH administrator_exists AS (SELECT 1 FROM users WHERE is_administrator)
          INSERT
          INTO users(user_id, period, is_administrator)
          VALUES (
            $1::USER_ID, PERIOD($2::INSTANT, NULL), (NOT EXISTS(SELECT 1 FROM administrator_exists))
          )
          RETURNING period, is_administrator;
        ",
        )
        .bind(user_id)
        .bind(now)
        .fetch_one(&mut tx)
        .await?
      };
      {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          user_id: UserId,
        }
        // language=PostgreSQL
        let row = sqlx::query_as::<_, Row>(
          r"
          INSERT
          INTO users_history(user_id, period, _is_current, updated_by, display_name, email, password)
          VALUES (
            $1::USER_ID, PERIOD($2::INSTANT, NULL), TRUE, $1::USER_ID, $3::USER_DISPLAY_NAME, $4::EMAIL_ADDRESS_HASH, pgp_sym_encrypt_bytea($5::PASSWORD_HASH, $6::TEXT)
          )
          RETURNING user_id;
        ",
        )
          .bind(user_id)
          .bind(now)
          .bind(&options.display_name)
          .bind(email_hash)
          .bind(options.password.as_ref())
          .bind(self.database_secret.as_str())
          .fetch_one(&mut tx)
          .await?;
        debug_assert_eq!(row.user_id, user_id);
      }
      if let Some(username) = options.username.as_ref() {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          user_id: UserId,
        }
        // language=PostgreSQL
        let row = sqlx::query_as::<_, Row>(
          r"
          INSERT
          INTO user_username_history(period, user_id, username, created_by, deleted_by)
          VALUES (
            PERIOD($1::INSTANT, NULL), $2::USER_ID, $3::USERNAME, $2::USER_ID, NULL
          )
          RETURNING user_id;
        ",
        )
        .bind(now)
        .bind(user_id)
        .bind(username)
        .fetch_one(&mut tx)
        .await?;
        debug_assert_eq!(row.user_id, user_id);
      }
      r
    };
    tx.commit().await?;

    let user = CompleteSimpleUser {
      id: user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: options.display_name.clone(),
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: options.username.clone(),
      email_address: None,
      has_password: options.password.is_some(),
    };

    Ok(user)
  }

  async fn get_user(&self, options: &GetUserOptions) -> Result<Option<RawGetUserResult>, AnyError> {
    let time = options.time.unwrap_or_else(|| self.clock.now());

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      period: PeriodLower,
      is_administrator: bool,
      display_name: UserDisplayName,
      username: Option<Username>,
      has_password: bool,
    }

    let mut ref_id: Option<UserId> = None;
    let mut ref_username: Option<Username> = None;
    let mut ref_email: Option<EmailAddress> = None;
    match &options.r#ref {
      UserRef::Id(r) => ref_id = Some(r.id),
      UserRef::Username(r) => ref_username = Some(r.username.clone()),
      UserRef::Email(r) => ref_email = Some(r.email.clone()),
    }
    if ref_email.is_some() {
      return Ok(None);
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r#"
        SELECT u.user_id, u.period, u.is_administrator, uh.display_name, uh.password IS NOT NULL AS has_password, uuh.username
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN user_username_history AS uuh USING (user_id)
        WHERE (user_id = $2::USER_ID OR uuh.username = $3::USERNAME)
          AND ($4::BOOLEAN OR u.period @> $1::INSTANT)
          AND (uh.period @> $1::INSTANT OR ($4::BOOLEAN AND uh._is_current))
          AND COALESCE(uuh.period @> $1::INSTANT, true);
      "#,
    )
    .bind(time)
    .bind(ref_id)
    .bind(ref_username)
    .bind(!options.skip_deleted)
    .fetch_optional(self.database.as_ref())
    .await?;

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Ok(None);
    };

    let user: CompleteSimpleUser = CompleteSimpleUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: row.username,
      email_address: None,
      has_password: row.has_password,
    };

    let user = match options.fields {
      UserFields::Complete => RawGetUserResult::Complete(user),
      UserFields::CompleteIfSelf { self_user_id } => {
        if self_user_id == user.id {
          RawGetUserResult::Complete(user)
        } else {
          RawGetUserResult::Default(user.into())
        }
      }
      UserFields::Default => RawGetUserResult::Default(user.into()),
      UserFields::Short => RawGetUserResult::Short(user.into()),
    };

    Ok(Some(user))
  }

  async fn get_user_with_password(&self, options: &GetUserOptions) -> Result<Option<ShortUserWithPassword>, AnyError> {
    let time = self.clock.now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      period: PeriodLower,
      deleted_by: Option<UserId>,
      display_name: UserDisplayName,
      password: Option<PasswordHash>,
    }

    let mut ref_id: Option<UserId> = None;
    let mut ref_username: Option<Username> = None;
    let mut ref_email: Option<EmailAddress> = None;
    match &options.r#ref {
      UserRef::Id(r) => ref_id = Some(r.id),
      UserRef::Username(r) => ref_username = Some(r.username.clone()),
      UserRef::Email(r) => ref_email = Some(r.email.clone()),
    }
    if ref_email.is_some() {
      return Ok(None);
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r#"
        SELECT u.user_id, u.period, u.deleted_by, u.is_administrator, uh.display_name, pgp_sym_decrypt_bytea(uh.password, $1::TEXT) AS password, uuh.username
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN user_username_history AS uuh USING (user_id)
        WHERE (user_id = $3::USER_ID OR uuh.username = $4::USERNAME)
          AND ($5::BOOLEAN OR u.period @> $2::INSTANT)
          AND (uh.period @> $2::INSTANT OR ($5::BOOLEAN AND uh._is_current))
          AND COALESCE(uuh.period @> $2::INSTANT, true);
      "#,
    )
      .bind(self.database_secret.as_str())
      .bind(time)
      .bind(ref_id)
      .bind(ref_username)
      .bind(!options.skip_deleted)
      .fetch_optional(self.database.as_ref())
      .await?;

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Ok(None);
    };

    if row.period.end().is_some() && options.skip_deleted {
      return Err(AnyError::from(
        "assertion error: expected user to not be deleted".to_string(),
      ));
    }

    let user = ShortUserWithPassword {
      id: row.user_id,
      created_at: row.period.start(),
      deleted: match (row.period.end(), row.deleted_by) {
        (Some(time), Some(user)) => Some(RawUserDot {
          time,
          user: user.into(),
        }),
        (None, None) => None,
        _ => panic!("data consistency error: expected `upper(period)` and `deleted_by` to have same nullability"),
      },
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
      password: row.password,
    };

    Ok(Some(user))
  }

  async fn get_short_user(&self, options: &GetShortUserOptions) -> Result<Option<ShortUser>, AnyError> {
    let time = options.time.unwrap_or_else(|| self.clock.now());

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      period: PeriodLower,
      display_name: UserDisplayName,
    }

    let mut ref_id: Option<UserId> = None;
    let mut ref_username: Option<Username> = None;
    let mut ref_email: Option<EmailAddress> = None;
    match &options.r#ref {
      UserRef::Id(r) => ref_id = Some(r.id),
      UserRef::Username(r) => ref_username = Some(r.username.clone()),
      UserRef::Email(r) => ref_email = Some(r.email.clone()),
    }
    if ref_email.is_some() {
      return Ok(None);
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r#"
        SELECT u.user_id, u.period, uh.display_name
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN user_username_history AS uuh USING (user_id)
        WHERE (user_id = $2::USER_ID OR uuh.username = $3::USERNAME)
          AND ($4::BOOLEAN OR u.period @> $1::INSTANT)
          AND (uh.period @> $1::INSTANT OR ($4::BOOLEAN AND uh._is_current))
          AND COALESCE(uuh.period @> $1::INSTANT, true);
      "#,
    )
    .bind(time)
    .bind(ref_id)
    .bind(ref_username)
    .bind(!options.skip_deleted)
    .fetch_optional(self.database.as_ref())
    .await?;

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Ok(None);
    };

    if row.period.end().is_some() && options.skip_deleted {
      return Err(AnyError::from(
        "assertion error: expected user to not be deleted".to_string(),
      ));
    }

    let user = ShortUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
    };

    Ok(Some(user))
  }

  async fn update_user(&self, options: &RawUpdateUserOptions) -> Result<CompleteSimpleUser, RawUpdateUserError> {
    let now = self.clock.now();

    let mut tx = self
      .database
      .as_ref()
      .begin()
      .await
      .map_err(RawUpdateUserError::other)?;

    if options.patch.display_name.is_some() {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        start_time: Instant,
        // TODO: Skip if new is same as old
        // value: UserDisplayName,
        created_at: Instant,
      }

      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
        SELECT
          lower(cur.period) AS start_time,
          display_name AS value,
          lower(users.period) AS created_at
        FROM
          users_history AS cur
          INNER JOIN users USING (user_id)
        WHERE
          user_id = $1::USER_ID
          AND NOT EXISTS(
            SELECT 1
            FROM users_history AS next
            WHERE
              next.user_id = $1::USER_ID
              AND next.period >> cur.period
              AND NOT (next.display_name = cur.display_name)
          )
        ORDER BY start_time ASC
        LIMIT 1;
      ",
      )
      .bind(options.r#ref.id)
      .fetch_one(&mut tx)
      .await
      .map_err(RawUpdateUserError::other)?;

      let lock_period = row.start_time..(row.start_time + *USER_DISPLAY_NAME_LOCK_DURATION);
      // Do not lock the display name on creation
      if lock_period.start != row.created_at && lock_period.contains(&now) {
        return Err(RawUpdateUserError::LockedDisplayName(
          options.r#ref,
          lock_period.into(),
          now,
        ));
      }
    }

    if let Some(new_username) = options.patch.username.as_ref() {
      // Note: when you have no username, you can always set the username immediately. Once set, it can't be changed
      // or removed until the lock expires.

      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        period: PeriodLower,
        username: Username,
      }

      // language=PostgreSQL
      let row: Option<Row> = sqlx::query_as::<_, Row>(
        r"
        SELECT period AS start_time, username
        FROM user_username_history AS uuh
        WHERE
          user_id = $2::USER_ID
          AND period @> $1::INSTANT;
      ",
      )
      .bind(now)
      .bind(options.r#ref.id)
      .fetch_optional(&mut tx)
      .await
      .map_err(RawUpdateUserError::other)?;

      let changed = match &row {
        None => new_username.is_some(),
        Some(row) => {
          let changed = new_username.as_ref() != Some(&row.username);
          let lock_period = row.period.start()..(row.period.start() + *USERNAME_LOCK_DURATION);
          if changed && lock_period.contains(&now) {
            return Err(RawUpdateUserError::LockedUsername(
              options.r#ref,
              lock_period.into(),
              now,
            ));
          }
          changed
        }
      };

      if changed {
        if row.is_some() {
          // language=PostgreSQL
          let res = sqlx::query(
            r"
            UPDATE user_username_history
            SET period = PERIOD(LOWER(period), $1::INSTANT),
              deleted_by = $3::USER_ID
            WHERE user_id = $2::USER_ID
              AND @> $1::INSTANT;
            ",
          )
          .bind(now)
          .bind(options.r#ref.id)
          .bind(options.actor.id)
          .execute(&mut tx)
          .await
          .map_err(RawUpdateUserError::other)?;
          assert!(res.rows_affected() == 1);
        }

        if let Some(new_username) = new_username {
          // language=PostgreSQL
          let res = sqlx::query(
            r"
            INSERT INTO user_username_history(period, user_id, username, created_by, deleted_by)
            VALUES (PERIOD($1::INSTANT, NULL), $2::USER_ID, $4::USERNAME, $3::USER_ID, NULL);
            ",
          )
          .bind(now)
          .bind(options.r#ref.id)
          .bind(options.actor.id)
          .bind(new_username)
          .execute(&mut tx)
          .await
          .map_err(RawUpdateUserError::other)?;
          assert!(res.rows_affected() == 1);
        }
      }
    }

    if options.patch.password.is_some() {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        start_time: Instant,
        value: Option<PasswordHash>,
      }

      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
        SELECT lower(period) AS start_time, password AS value FROM users_history AS cur
        WHERE
          user_id = $1::USER_ID
          AND NOT EXISTS(
            SELECT 1
            FROM users_history AS next
            WHERE
              next.user_id = $1::USER_ID
              AND next.period >> cur.period
              AND NOT ((next.password IS NULL AND cur.password IS NULL) OR (next.password IS NOT NULL AND cur.password IS NOT NULL AND next.password = cur.password))
          )
        ORDER BY start_time ASC
        LIMIT 1;
      ",
      )
        .bind(options.r#ref.id)
        .fetch_one(&mut tx)
        .await
        .map_err(RawUpdateUserError::other)?;

      let lock_period = row.start_time..(row.start_time + *USER_PASSWORD_LOCK_DURATION);
      if row.value.is_some() && lock_period.contains(&now) {
        return Err(RawUpdateUserError::LockedPassword(
          options.r#ref,
          lock_period.into(),
          now,
        ));
      }
    }

    {
      // language=PostgreSQL
      let res = sqlx::query(
        r"
      WITH prev_state AS (
        UPDATE users_history SET period = PERIOD(lower(period), $1::INSTANT), _is_current = NULL
        WHERE user_id = $2::USER_ID AND upper_inf(period)
        RETURNING display_name, password
      )
      INSERT INTO users_history(
        user_id, period, _is_current, updated_by,
        display_name,
        password
      )
      SELECT
        $2::USER_ID, PERIOD($1::INSTANT, NULL), TRUE, $3::USER_ID,
        CASE WHEN $4::BOOLEAN THEN $5::USER_DISPLAY_NAME ELSE prev_state.display_name END,
        CASE WHEN $6::BOOLEAN THEN pgp_sym_encrypt_bytea($7::PASSWORD_HASH, $8::TEXT) ELSE prev_state.password END
      FROM prev_state
      RETURNING user_id;
      ",
      )
      .bind(now)
      .bind(options.r#ref.id)
      .bind(options.actor.id)
      .bind(options.patch.display_name.is_some())
      .bind(options.patch.display_name.as_ref())
      .bind(options.patch.password.is_some())
      .bind(options.patch.password.as_ref())
      .bind(self.database_secret.as_str())
      .execute(&mut tx)
      .await
      .map_err(RawUpdateUserError::other)?;
      assert!(res.rows_affected() == 1);
    }

    let row = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        user_id: UserId,
        period: PeriodLower,
        is_administrator: bool,
        display_name: UserDisplayName,
        username: Option<Username>,
        has_password: bool,
      }

      // language=PostgreSQL
      sqlx::query_as::<_, Row>(
        r"
      SELECT user_id, period, is_administrator, display_name, username, password IS NOT NULL as has_password
      FROM user_current
      WHERE user_id = $1::USER_ID;
      ",
      )
      .bind(options.r#ref.id)
      .fetch_one(&mut tx)
      .await
      .map_err(RawUpdateUserError::other)?
    };

    tx.commit().await.map_err(RawUpdateUserError::other)?;

    let user: CompleteSimpleUser = CompleteSimpleUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: row.username,
      email_address: None,
      has_password: row.has_password,
    };

    Ok(user)
  }

  async fn delete_user(&self, cmd: &RawDeleteUser) -> Result<CompleteSimpleUser, DeleteUserError> {
    let now = cmd.now;

    let mut tx = self.database.as_ref().begin().await.map_err(DeleteUserError::other)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      period: PeriodLower,
      is_administrator: bool,
      display_name: UserDisplayName,
      has_password: bool,
      username: Option<Username>,
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r#"
        SELECT u.user_id, u.period, u.is_administrator, uh.display_name, uh.password IS NOT NULL AS has_password, uuh.username
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN user_username_history AS uuh USING (user_id)
        WHERE user_id = $2::USER_ID
          AND uh._is_current
          AND upper_inf(uuh.period);
      "#,
    )
    .bind(now)
    .bind(cmd.user.id)
    .fetch_optional(self.database.as_ref())
    .await.map_err(DeleteUserError::other)?;

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Err(DeleteUserError::NotFound(cmd.user));
    };

    if row.period.end().is_some() {
      return Err(DeleteUserError::Gone(cmd.user));
    }

    {
      // language=PostgreSQL
      let res = sqlx::query(
        r"
      UPDATE users AS u
      SET
        period = PERIOD(LOWER(u.period), $1::INSTANT),
        deleted_by = $3::USER_ID
      WHERE u.user_id = $2::USER_ID AND upper_inf(u.period)
      RETURNING user_id;
      ",
      )
      .bind(now)
      .bind(cmd.user.id)
      .bind(cmd.actor.id)
      .execute(&mut tx)
      .await
      .map_err(DeleteUserError::other)?;
      assert!(res.rows_affected() == 1);
    }

    {
      // language=PostgreSQL
      let res = sqlx::query(
        r"
      UPDATE user_username_history AS uuh
      SET
        period = PERIOD(LOWER(uuh.period), $1::INSTANT),
        deleted_by = $3::USER_ID
      WHERE uuh.user_id = $2::USER_ID AND upper_inf(uuh.period)
      RETURNING user_id;
      ",
      )
      .bind(now)
      .bind(cmd.user.id)
      .bind(cmd.actor.id)
      .execute(&mut tx)
      .await
      .map_err(DeleteUserError::other)?;
      assert!(res.rows_affected() <= 1);
    }

    tx.commit().await.map_err(DeleteUserError::other)?;

    let user: CompleteSimpleUser = CompleteSimpleUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: row.username,
      email_address: None,
      has_password: row.has_password,
    };

    Ok(user)
  }

  async fn hard_delete_user(&self, cmd: &RawHardDeleteUser) -> Result<(), HardDeleteUserError> {
    // language=PostgreSQL
    let res = sqlx::query(
      r"
        DELETE
        FROM users
        WHERE user_id = $1::USER_ID;
    ",
    )
    .bind(cmd.user.id)
    .execute(self.database.as_ref())
    .await
    .map_err(HardDeleteUserError::other)?;

    match res.rows_affected() {
      0 => Err(HardDeleteUserError::NotFound(cmd.user)),
      1 => Ok(()),
      _ => panic!("AssertionError: Expected 0 or 1 rows to be affected"),
    }
  }
}

#[cfg(feature = "neon")]
impl<TyClock, TyDatabase, TyUuidGenerator> neon::prelude::Finalize for PgUserStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: Clock,
  TyDatabase: ApiRef<PgPool>,
  TyUuidGenerator: UuidGenerator,
{
}

#[cfg(test)]
mod test {
  use super::PgUserStore;
  use crate::test::TestApi;
  use etwin_core::clock::VirtualClock;
  use etwin_core::core::{Instant, Secret};
  use etwin_core::user::UserStore;
  use etwin_core::uuid::Uuid4Generator;
  use etwin_db_schema::force_create_latest;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn UserStore>> {
    let config = etwin_config::find_config(std::env::current_dir().unwrap()).unwrap();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.db.host)
          .port(config.db.port)
          .database(&config.db.name)
          .username(&config.db.admin_user)
          .password(&config.db.admin_password),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.db.host)
          .port(config.db.port)
          .database(&config.db.name)
          .username(&config.db.user)
          .password(&config.db.password),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let database_secret = Secret::new("dev_secret".to_string());
    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(
      clock.clone(),
      database,
      database_secret,
      uuid_generator,
    ));

    TestApi { clock, user_store }
  }

  test_user_store!(
    #[serial]
    || make_test_api().await
  );
}
