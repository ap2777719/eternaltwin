use async_trait::async_trait;
use auto_impl::auto_impl;
use etwin_core::oauth::RfcOauthAccessToken;
use thiserror::Error;
use url::Url;

#[cfg(feature = "http")]
pub mod http;
#[cfg(feature = "mem")]
pub mod mem;

#[derive(Debug, Error)]
pub enum GetAccessTokenError {
  #[error("failed to send request")]
  SendRequest(#[source] reqwest::Error),
  #[error("failed to receive response")]
  ReceiveResponse(#[source] reqwest::Error),
  #[error("failed to parse response")]
  ParseResponse(#[source] serde_json::Error),
}

#[async_trait]
#[auto_impl(&, Arc)]
pub trait RfcOauthClient: Send + Sync {
  fn authorization_uri(&self, scope: &str, state: &str) -> Url;

  /// Get the hostname for the `authorization_uri` endpoint.
  fn authorization_server(&self) -> &str;

  async fn get_access_token(&self, code: &str) -> Result<RfcOauthAccessToken, GetAccessTokenError>;
}
