use crate::{GetAccessTokenError, RfcOauthClient};
use ::url::Url;
use async_trait::async_trait;
use etwin_core::oauth::RfcOauthAccessToken;
use etwin_core::types::AnyError;
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::time::Duration;

const USER_AGENT: &str = "EtwinOauth";
const TIMEOUT: Duration = Duration::from_millis(5000);

pub struct HttpRfcOauthClientOptions {
  pub authorization_endpoint: Url,
  pub token_endpoint: Url,
  pub callback_endpoint: Url,
  pub client_id: String,
  pub client_secret: String,
}

impl HttpRfcOauthClientOptions {
  pub fn authorization_uri(&self, scope: &str, state: &str) -> Url {
    let mut url = self.authorization_endpoint.clone();
    url
      .query_pairs_mut()
      .append_pair("access_type", "offline")
      .append_pair("response_type", "code")
      .append_pair("redirect_uri", self.callback_endpoint.as_str())
      .append_pair("client_id", self.client_id.as_str())
      .append_pair("scope", scope)
      .append_pair("state", state);
    url
  }
}

pub struct HttpRfcOauthClient {
  options: HttpRfcOauthClientOptions,
  client: Client,
}

impl HttpRfcOauthClient {
  pub fn new(options: HttpRfcOauthClientOptions) -> Result<Self, AnyError> {
    Ok(Self {
      options,
      client: Client::builder()
        .user_agent(USER_AGENT)
        .timeout(TIMEOUT)
        .redirect(reqwest::redirect::Policy::none())
        .build()?,
    })
  }
}

#[async_trait]
impl RfcOauthClient for HttpRfcOauthClient {
  fn authorization_uri(&self, scope: &str, state: &str) -> Url {
    self.options.authorization_uri(scope, state)
  }

  fn authorization_server(&self) -> &str {
    self
      .options
      .authorization_endpoint
      .host_str()
      .expect("invalid authorization endpoint")
  }

  async fn get_access_token(&self, code: &str) -> Result<RfcOauthAccessToken, GetAccessTokenError> {
    let res = self
      .client
      .post(self.options.token_endpoint.clone())
      .basic_auth(
        self.options.client_id.as_str(),
        Some(self.options.client_secret.as_str()),
      )
      .form(&OauthAccessTokenRequest {
        client_id: self.options.client_id.clone(),
        client_secret: self.options.client_secret.clone(),
        redirect_uri: self.options.callback_endpoint.clone(),
        code: code.to_string(),
        grant_type: RfcOauthGrantType::AuthorizationCode,
      })
      .send()
      .await
      .map_err(GetAccessTokenError::SendRequest)?;

    // TODO: Check content type (application/json or text/html)
    let body = res.bytes().await.map_err(GetAccessTokenError::ReceiveResponse)?;
    let parsed: RfcOauthAccessToken = serde_json::from_slice(&body).map_err(GetAccessTokenError::ParseResponse)?;
    Ok(parsed)
  }
}

#[cfg(feature = "neon")]
impl neon::prelude::Finalize for HttpRfcOauthClient {}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct OauthAccessTokenRequest {
  client_id: String,
  client_secret: String,
  redirect_uri: Url,
  code: String,
  grant_type: RfcOauthGrantType,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum RfcOauthGrantType {
  #[serde(rename = "authorization_code")]
  AuthorizationCode,
}
