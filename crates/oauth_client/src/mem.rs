use crate::{GetAccessTokenError, RfcOauthClient};
use ::url::Url;
use async_trait::async_trait;
use etwin_core::oauth::RfcOauthAccessToken;

pub struct MemRfcOauthClient {}

impl MemRfcOauthClient {
  pub const fn new() -> Self {
    Self {}
  }
}

#[async_trait]
impl RfcOauthClient for MemRfcOauthClient {
  fn authorization_uri(&self, _scope: &str, _state: &str) -> Url {
    Url::parse("http://localhost/todo").unwrap()
  }

  fn authorization_server(&self) -> &str {
    "localhost"
  }

  async fn get_access_token(&self, _code: &str) -> Result<RfcOauthAccessToken, GetAccessTokenError> {
    unimplemented!("get_access_token")
  }
}

#[cfg(feature = "neon")]
impl neon::prelude::Finalize for MemRfcOauthClient {}
