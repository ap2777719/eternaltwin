use crate::extract::Extractor;
use crate::RouterApi;
use axum::body::Body;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Redirect, Response};
use axum::routing::post;
use axum::{Extension, Form, Router};
use etwin_core::auth::AuthContext;
use etwin_core::dinoparc::{DinoparcCredentials, DinoparcPassword, DinoparcServer, DinoparcUsername};
use etwin_core::hammerfest::{HammerfestCredentials, HammerfestPassword, HammerfestServer, HammerfestUsername};
use serde::{Deserialize, Serialize};
use thiserror::Error;

pub fn router() -> Router<(), Body> {
  Router::new()
    .route("/dinoparc", post(archive_dinoparc))
    .route("/hammerfest", post(archive_hammerfest))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub(crate) struct ArchiveDinoparcBody {
  pub dinoparc_server: DinoparcServer,
  pub dinoparc_username: DinoparcUsername,
  pub dinoparc_password: DinoparcPassword,
}

#[derive(Debug, Error)]
enum ArchiveDinoparcError {
  #[error("inner error")]
  Inner(#[from] etwin_services::user::LinkToDinoparcError),
}

impl IntoResponse for ArchiveDinoparcError {
  fn into_response(self) -> Response {
    let (code, msg) = match self {
      Self::Inner(_) => (StatusCode::INTERNAL_SERVER_ERROR, "internal server error".to_string()),
    };
    (code, msg).into_response()
  }
}

async fn archive_dinoparc(
  Extension(api): Extension<RouterApi>,
  acx: Extractor<AuthContext>,
  Form(body): Form<ArchiveDinoparcBody>,
) -> Result<Redirect, ArchiveDinoparcError> {
  match api
    .dinoparc
    .as_ref()
    .archive_with_credentials(
      &acx.value(),
      &DinoparcCredentials {
        server: body.dinoparc_server,
        username: body.dinoparc_username,
        password: body.dinoparc_password,
      },
    )
    .await
  {
    Ok(_) => Ok(Redirect::to("/settings")),
    Err(_) => Ok(Redirect::to("/settings")),
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub(crate) struct ArchiveHammerfestBody {
  pub hammerfest_server: HammerfestServer,
  pub hammerfest_username: HammerfestUsername,
  pub hammerfest_password: HammerfestPassword,
}

#[derive(Debug, Error)]
enum ArchiveHammerfestError {
  #[error("inner error")]
  Inner(#[from] etwin_services::user::LinkToHammerfestError),
}

impl IntoResponse for ArchiveHammerfestError {
  fn into_response(self) -> Response {
    let (code, msg) = match self {
      Self::Inner(_) => (StatusCode::INTERNAL_SERVER_ERROR, "internal server error".to_string()),
    };
    (code, msg).into_response()
  }
}

async fn archive_hammerfest(
  Extension(api): Extension<RouterApi>,
  acx: Extractor<AuthContext>,
  Form(body): Form<ArchiveHammerfestBody>,
) -> Result<Redirect, ArchiveHammerfestError> {
  match api
    .hammerfest
    .as_ref()
    .archive_with_credentials(
      &acx.value(),
      &HammerfestCredentials {
        server: body.hammerfest_server,
        username: body.hammerfest_username,
        password: body.hammerfest_password,
      },
    )
    .await
  {
    Ok(_) => Ok(Redirect::to("/settings")),
    Err(_) => Ok(Redirect::to("/settings")),
  }
}
