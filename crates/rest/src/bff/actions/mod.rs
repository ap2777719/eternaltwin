use axum::body::Body;
use axum::routing::post;
use axum::Router;

pub mod archive;
pub mod link;
pub mod login;
pub mod register;

pub fn router() -> Router<(), Body> {
  Router::new()
    .route("/register", post(register::register))
    .nest("/archive", archive::router())
    .nest("/link", link::router())
    .nest("/login", login::router())
}
