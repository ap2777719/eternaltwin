use crate::RouterApi;
use axum::body::Body;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Redirect, Response};
use axum::routing::post;
use axum::{Extension, Router};
use etwin_core::oauth::EtwinOauthStateAction;
use etwin_core::types::AnyError;
use thiserror::Error;

pub fn router() -> Router<(), Body> {
  Router::new().route("/twinoid", post(login_with_twinoid))
}

#[derive(Debug, Error)]
enum LoginWithTwinoidError {
  #[error("inner error")]
  Inner(#[from] AnyError),
}

impl IntoResponse for LoginWithTwinoidError {
  fn into_response(self) -> Response {
    let (code, msg) = match self {
      Self::Inner(_) => (StatusCode::INTERNAL_SERVER_ERROR, "internal server error".to_string()),
    };
    (code, msg).into_response()
  }
}

async fn login_with_twinoid(Extension(api): Extension<RouterApi>) -> Result<Redirect, LoginWithTwinoidError> {
  let authorization_uri = api
    .auth
    .as_ref()
    .request_twinoid_auth(EtwinOauthStateAction::Login)
    .await?;
  Ok(Redirect::to(authorization_uri.as_str()))
}
