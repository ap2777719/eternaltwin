use crate::RouterApi;
use axum::body::Body;
use axum::routing::get;
use axum::{Extension, Json, Router};
use etwin_services::forum::ForumConfig;
use serde::{Deserialize, Serialize};

pub fn router() -> Router<(), Body> {
  Router::new().route("/", get(get_config))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Config {
  forum: ForumConfig,
}

async fn get_config(Extension(api): Extension<RouterApi>) -> Json<Config> {
  Json(Config {
    forum: api.forum.config(),
  })
}
