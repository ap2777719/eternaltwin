use etwin_core::clock::{Clock, VirtualClock};
use etwin_hammerfest_client::MemHammerfestClient;
use std::sync::Arc;

pub(crate) mod hammerfest_client;

#[derive(Clone)]
pub struct DevApi {
  pub clock: Option<Arc<VirtualClock>>,
  pub hammerfest_client: Option<Arc<MemHammerfestClient<Arc<dyn Clock>>>>,
}

impl DevApi {
  pub fn new() -> Self {
    Self {
      clock: None,
      hammerfest_client: None,
    }
  }
}

impl Default for DevApi {
  fn default() -> Self {
    Self::new()
  }
}
