mod dinoparc;
mod hammerfest;
mod twinoid;

use axum::body::Body;
use axum::Router;

pub fn router() -> Router<(), Body> {
  Router::new()
    .nest("/dinoparc", dinoparc::router())
    .nest("/hammerfest", hammerfest::router())
    .nest("/twinoid", twinoid::router())
}
