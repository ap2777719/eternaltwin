# `etwin_cli`

## Usage

```
cargo run --package etwin_cli --bin etwin_cli -- dinoparc
cargo run --package etwin_cli --bin etwin_cli -- dinorpg
cargo run --package etwin_cli --bin etwin_cli -- dump
cargo run --package etwin_cli --bin etwin_cli -- twinoid
```
