use clap::Parser;
use etwin_cli::cmd::dump;
use std::error::Error;
use std::process::exit;

#[tokio::main]
async fn main() {
  let args: dump::Args = dump::Args::parse();

  let res = dump::run(&args).await;

  if let Err(e) = res {
    eprintln!("ERROR: {}", &e);
    let mut source: Option<&dyn Error> = e.source();
    for _ in 0..100 {
      match source {
        Some(src) => {
          eprintln!("-> {}", src);
          source = src.source();
        }
        None => break,
      }
    }
    exit(1)
  }
}
