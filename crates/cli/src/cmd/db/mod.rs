use clap::Parser;
use etwin_core::types::AnyError;

pub mod check;
pub mod reset;
pub mod sync;

#[derive(Debug, Parser)]
pub struct Args {
  #[clap(subcommand)]
  command: Command,
}

#[derive(Debug, Parser)]
pub enum Command {
  /// Check the database state
  #[clap(name = "check")]
  Check(check::Args),
  /// Reset the database to the empty state
  #[clap(name = "reset")]
  Reset(reset::Args),
  /// Synchronize the DB schema to the latest version
  #[clap(name = "sync")]
  Sync(sync::Args),
}

pub async fn run(args: &Args) -> Result<(), AnyError> {
  match &args.command {
    Command::Check(ref args) => check::run(args).await,
    Command::Reset(ref args) => reset::run(args).await,
    Command::Sync(ref args) => sync::run(args).await,
  }
}
