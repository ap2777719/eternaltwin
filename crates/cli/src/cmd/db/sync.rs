use clap::Parser;
use etwin_config::Config;
use etwin_core::types::AnyError;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;
use std::env;

#[derive(Debug, Parser)]
pub struct Args {}

pub async fn run(_args: &Args) -> Result<(), AnyError> {
  let working_dir = env::current_dir()?;
  let config: Config = etwin_config::find_config(working_dir.clone()).unwrap();
  let admin_database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.db.host)
        .port(config.db.port)
        .database(&config.db.name)
        .username(&config.db.admin_user)
        .password(&config.db.admin_password),
    )
    .await
    .unwrap();
  eprintln!("Starting sync");
  etwin_db_schema::sync(&admin_database).await.unwrap();
  eprintln!("Sync complete");
  admin_database.close().await;
  Ok(())
}
