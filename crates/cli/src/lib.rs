use clap::Parser;
use etwin_core::types::AnyError;
use std::error::Error;
use std::process::exit;

pub mod cmd;

#[derive(Debug, Parser)]
#[clap(author = "Eternaltwin")]
pub struct CliArgs {
  #[clap(subcommand)]
  command: CliCommand,
}

#[derive(Debug, Parser)]
pub enum CliCommand {
  /// Run the Eternaltwin backend
  #[clap(name = "backend")]
  Backend(cmd::backend::Args),
  /// Manage the db schema and state
  #[clap(name = "db")]
  Db(cmd::db::Args),
  /// Run the Dinoparc client demo
  #[clap(name = "dinoparc")]
  Dinoparc(cmd::dinoparc::Args),
  /// Run the DinoRPG client demo
  #[clap(name = "dinorpg")]
  Dinorpg(cmd::dinorpg::Args),
  /// Dump the DB state into a directory
  #[clap(name = "dump")]
  Dump(cmd::dump::Args),
  /// Run the full Eternaltwin server
  #[clap(name = "start")]
  Start(cmd::start::Args),
  /// Start REST server
  #[clap(name = "rest")]
  Rest(cmd::rest::Args),
  /// Run the Twinoid client demo
  #[clap(name = "twinoid")]
  Twinoid(cmd::twinoid::Args),
}

pub async fn run(args: &CliArgs) -> Result<(), AnyError> {
  match &args.command {
    CliCommand::Backend(ref args) => cmd::backend::run(args).await,
    CliCommand::Start(ref args) => cmd::start::run(args).await,
    CliCommand::Db(ref args) => cmd::db::run(args).await,
    CliCommand::Dinoparc(ref args) => cmd::dinoparc::run(args).await,
    CliCommand::Dinorpg(ref args) => cmd::dinorpg::run(args).await,
    CliCommand::Dump(ref args) => cmd::dump::run(args).await,
    CliCommand::Rest(ref args) => cmd::rest::run(args).await,
    CliCommand::Twinoid(ref args) => cmd::twinoid::run(args).await,
  }
}

#[tokio::main]
pub async fn main() {
  let args: CliArgs = CliArgs::parse();

  let res = run(&args).await;

  if let Err(e) = res {
    eprintln!("ERROR: {}", &e);
    let mut source: Option<&dyn Error> = e.source();
    for _ in 0..100 {
      match source {
        Some(src) => {
          eprintln!("-> {}", src);
          source = src.source();
        }
        None => break,
      }
    }
    exit(1)
  }
}
