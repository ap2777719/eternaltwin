use crate::user::archive_dinoparc;
use etwin_core::auth::AuthContext;
use etwin_core::core::UserDot;
use etwin_core::dinoparc::{
  ArchivedDinoparcDinoz, ArchivedDinoparcUser, DinoparcClient, DinoparcCredentials, DinoparcStore, DinoparcUserIdRef,
  EtwinDinoparcDinoz, EtwinDinoparcUser, GetDinoparcDinozOptions, GetDinoparcUserOptions, ShortDinoparcUser,
};
use etwin_core::link::{EtwinLink, GetLinkOptions, LinkStore, VersionedEtwinLink, VersionedRawLink};
use etwin_core::token::TokenStore;
use etwin_core::types::AnyError;
use etwin_core::user::{GetShortUserOptions, ShortUser, UserRef, UserStore};
use std::error::Error;
use std::sync::Arc;

pub struct DinoparcService<TyDinoparcClient, TyDinoparcStore, TyLinkStore, TyTokenStore, TyUserStore>
where
  TyDinoparcClient: DinoparcClient,
  TyDinoparcStore: DinoparcStore,
  TyLinkStore: LinkStore,
  TyTokenStore: TokenStore,
  TyUserStore: UserStore,
{
  dinoparc_client: TyDinoparcClient,
  dinoparc_store: TyDinoparcStore,
  link_store: TyLinkStore,
  token_store: TyTokenStore,
  user_store: TyUserStore,
}

pub type DynDinoparcService = DinoparcService<
  Arc<dyn DinoparcClient>,
  Arc<dyn DinoparcStore>,
  Arc<dyn LinkStore>,
  Arc<dyn TokenStore>,
  Arc<dyn UserStore>,
>;

impl<TyDinoparcClient, TyDinoparcStore, TyLinkStore, TyTokenStore, TyUserStore>
  DinoparcService<TyDinoparcClient, TyDinoparcStore, TyLinkStore, TyTokenStore, TyUserStore>
where
  TyDinoparcClient: DinoparcClient + Clone + 'static,
  TyDinoparcStore: DinoparcStore + Clone + 'static,
  TyLinkStore: LinkStore,
  TyTokenStore: TokenStore,
  TyUserStore: UserStore,
{
  pub fn new(
    dinoparc_client: TyDinoparcClient,
    dinoparc_store: TyDinoparcStore,
    link_store: TyLinkStore,
    token_store: TyTokenStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      dinoparc_client,
      dinoparc_store,
      link_store,
      token_store,
      user_store,
    }
  }

  pub async fn get_user(
    &self,
    _acx: &AuthContext,
    options: &GetDinoparcUserOptions,
  ) -> Result<Option<EtwinDinoparcUser>, Box<dyn Error + Send + Sync + 'static>> {
    let user: Option<ArchivedDinoparcUser> = self.dinoparc_store.get_user(options).await?;
    let user: ArchivedDinoparcUser = match user {
      Some(user) => user,
      None => return Ok(None),
    };
    let etwin_link: VersionedRawLink<DinoparcUserIdRef> = {
      let options: GetLinkOptions<DinoparcUserIdRef> = GetLinkOptions {
        remote: DinoparcUserIdRef {
          server: user.server,
          id: user.id,
        },
        time: None,
      };
      self.link_store.get_link_from_dinoparc(&options).await?
    };
    let etwin_link: VersionedEtwinLink = {
      let current = match etwin_link.current {
        None => None,
        Some(l) => {
          let user: ShortUser = self
            .user_store
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.link.user),
              time: options.time,
              skip_deleted: false,
            })
            .await?
            .unwrap();
          let etwin: ShortUser = self
            .user_store
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.etwin),
              time: options.time,
              skip_deleted: false,
            })
            .await?
            .unwrap();
          Some(EtwinLink {
            link: UserDot {
              time: l.link.time,
              user,
            },
            unlink: (),
            etwin,
          })
        }
      };
      VersionedEtwinLink { current, old: vec![] }
    };
    let dparc_user = EtwinDinoparcUser {
      server: user.server,
      id: user.id,
      archived_at: user.archived_at,
      username: user.username,
      coins: user.coins,
      dinoz: user.dinoz,
      inventory: user.inventory,
      collection: user.collection,
      etwin: etwin_link,
    };
    Ok(Some(dparc_user))
  }

  pub async fn get_dinoz(
    &self,
    _acx: &AuthContext,
    options: &GetDinoparcDinozOptions,
  ) -> Result<Option<EtwinDinoparcDinoz>, Box<dyn Error + Send + Sync + 'static>> {
    let dinoz: Option<ArchivedDinoparcDinoz> = self.dinoparc_store.get_dinoz(options).await?;
    // TODO: Map owner data to include etwin ref
    Ok(dinoz)
  }

  pub async fn archive_with_credentials(
    &self,
    _acx: &AuthContext,
    options: &DinoparcCredentials,
  ) -> Result<ShortDinoparcUser, AnyError> {
    let remote_session = self.dinoparc_client.create_session(options).await?;
    self.dinoparc_store.touch_short_user(&remote_session.user).await?;
    tokio::spawn(archive_dinoparc(
      self.dinoparc_client.clone(),
      self.dinoparc_store.clone(),
      remote_session.clone(),
    ));
    self
      .token_store
      .touch_dinoparc(remote_session.user.as_ref(), &remote_session.key)
      .await?;

    Ok(remote_session.user)
  }
}

#[cfg(feature = "neon")]
impl<TyDinoparcClient, TyDinoparcStore, TyLinkStore, TyTokenStore, TyUserStore> neon::prelude::Finalize
  for DinoparcService<TyDinoparcClient, TyDinoparcStore, TyLinkStore, TyTokenStore, TyUserStore>
where
  TyDinoparcClient: DinoparcClient,
  TyDinoparcStore: DinoparcStore,
  TyLinkStore: LinkStore,
  TyTokenStore: TokenStore,
  TyUserStore: UserStore,
{
}
