use etwin_core::oauth::{OauthClient, OauthProviderStore, UpsertSystemClientError, UpsertSystemClientOptions};
use etwin_core::user::UserStore;
use std::sync::Arc;

pub struct OauthService<TyOauthProviderStore, TyUserStore>
where
  TyOauthProviderStore: OauthProviderStore,
  TyUserStore: UserStore,
{
  oauth_provider_store: TyOauthProviderStore,
  user_store: TyUserStore,
}

pub type DynOauthService = OauthService<Arc<dyn OauthProviderStore>, Arc<dyn UserStore>>;

impl<TyOauthProviderStore, TyUserStore> OauthService<TyOauthProviderStore, TyUserStore>
where
  TyOauthProviderStore: OauthProviderStore,
  TyUserStore: UserStore,
{
  pub fn new(oauth_provider_store: TyOauthProviderStore, user_store: TyUserStore) -> Self {
    Self {
      oauth_provider_store,
      user_store,
    }
  }

  pub async fn upsert_system_client(
    &self,
    options: &UpsertSystemClientOptions,
  ) -> Result<OauthClient, UpsertSystemClientError> {
    let client = self
      .oauth_provider_store
      .upsert_system_client(options)
      .await
      .map_err(UpsertSystemClientError::Other)?;
    let owner = match client.owner {
      None => None,
      Some(owner_id) => self
        .user_store
        .get_short_user(&owner_id.into())
        .await
        .map_err(UpsertSystemClientError::Other)?,
    };
    Ok(OauthClient {
      id: client.id,
      key: client.key,
      display_name: client.display_name,
      app_uri: client.app_uri,
      callback_uri: client.callback_uri,
      owner,
    })
  }
}
