use crate::helpers::link_resolver::LinkResolver;
use etwin_constants::dinoparc::MAX_SIDEBAR_DINOZ_COUNT;
use etwin_core::auth::AuthContext;
use etwin_core::clock::Clock;
use etwin_core::dinoparc::{
  DinoparcClient, DinoparcCredentials, DinoparcDinozId, DinoparcSession, DinoparcStore, DinoparcUserIdRef,
  ShortDinoparcUser,
};
use etwin_core::hammerfest::{
  HammerfestClient, HammerfestCredentials, HammerfestGetProfileByIdOptions, HammerfestSession, HammerfestStore,
  HammerfestUserIdRef, ShortHammerfestUser,
};
use etwin_core::link::{
  DeleteLinkError, DeleteLinkOptions, GetLinksFromEtwinOptions, LinkStore, RawDeleteAllLinks, TouchLinkError,
  TouchLinkOptions, VersionedLink, VersionedRawLinks,
};
use etwin_core::password::PasswordService;
use etwin_core::token::{TokenStore, TouchOauthTokenOptions};
use etwin_core::twinoid::{
  ShortTwinoidUser, TwinoidApiAuth, TwinoidClient, TwinoidStore, TwinoidUserId, TwinoidUserIdRef,
};
use etwin_core::types::AnyError;
pub use etwin_core::user::DeleteUserError;
use etwin_core::user::{
  CompleteUser, DeleteUserOptions, GetUserOptions, GetUserResult, LinkToDinoparcOptions, LinkToHammerfestOptions,
  LinkToTwinoidOptions, RawDeleteUser, RawGetUserResult, RawUpdateUserError, RawUpdateUserOptions, RawUpdateUserPatch,
  UnlinkFromDinoparcOptions, UnlinkFromHammerfestOptions, UnlinkFromTwinoidOptions, UpdateUserOptions, User,
  UserFields, UserStore,
};
use std::sync::Arc;
use std::time::Duration;
use thiserror::Error;

pub struct UserService<
  TyClock,
  TyDinoparcClient,
  TyDinoparcStore,
  TyHammerfestClient,
  TyHammerfestStore,
  TyLinkStore,
  TyPassword,
  TyTokenStore,
  TyTwinoidClient,
  TyTwinoidStore,
  TyUserStore,
> where
  TyClock: Clock,
  TyDinoparcClient: DinoparcClient,
  TyDinoparcStore: DinoparcStore,
  TyHammerfestClient: HammerfestClient,
  TyHammerfestStore: HammerfestStore,
  TyLinkStore: LinkStore,
  TyPassword: PasswordService,
  TyTokenStore: TokenStore,
  TyTwinoidClient: TwinoidClient,
  TyTwinoidStore: TwinoidStore,
  TyUserStore: UserStore,
{
  clock: TyClock,
  dinoparc_client: TyDinoparcClient,
  dinoparc_store: TyDinoparcStore,
  hammerfest_client: TyHammerfestClient,
  hammerfest_store: TyHammerfestStore,
  link_store: TyLinkStore,
  password: TyPassword,
  token_store: TyTokenStore,
  twinoid_client: TyTwinoidClient,
  twinoid_store: TyTwinoidStore,
  user_store: TyUserStore,
}

pub type DynUserService = UserService<
  Arc<dyn Clock>,
  Arc<dyn DinoparcClient>,
  Arc<dyn DinoparcStore>,
  Arc<dyn HammerfestClient>,
  Arc<dyn HammerfestStore>,
  Arc<dyn LinkStore>,
  Arc<dyn PasswordService>,
  Arc<dyn TokenStore>,
  Arc<dyn TwinoidClient>,
  Arc<dyn TwinoidStore>,
  Arc<dyn UserStore>,
>;

#[derive(Debug, Error)]
pub enum GetUserError {
  #[error("user not found")]
  NotFound,
  #[error("internal error")]
  Internal(#[source] AnyError),
}

#[derive(Debug, Error)]
pub enum UpdateUserError {
  #[error("forbidden")]
  Forbidden,
  #[error("raw update error")]
  Raw(#[from] RawUpdateUserError),
  #[error("internal error")]
  Internal(#[source] AnyError),
}

#[derive(Debug, Error)]
pub enum LinkToDinoparcError {
  #[error("internal `touch_link` error")]
  InternalTouchLink(#[from] TouchLinkError<DinoparcUserIdRef>),
  #[error("internal error")]
  Internal(#[source] AnyError),
}

#[derive(Debug, Error)]
pub enum UnlinkFromDinoparcError {
  #[error("internal `delete_link` error")]
  InternalDeleteLink(#[from] DeleteLinkError<DinoparcUserIdRef>),
  #[error("internal error")]
  Internal(#[source] AnyError),
}

#[derive(Debug, Error)]
pub enum LinkToHammerfestError {
  #[error("internal `touch_link` error")]
  InternalTouchLink(#[from] TouchLinkError<HammerfestUserIdRef>),
  #[error("internal error")]
  Internal(#[source] AnyError),
}

#[derive(Debug, Error)]
pub enum UnlinkFromHammerfestError {
  #[error("internal `delete_link` error")]
  InternalDeleteLink(#[from] DeleteLinkError<HammerfestUserIdRef>),
  #[error("internal error")]
  Internal(#[source] AnyError),
}

#[derive(Debug, Error)]
pub enum LinkToTwinoidError {
  #[error("internal `touch_link` error")]
  InternalTouchLink(#[from] TouchLinkError<TwinoidUserIdRef>),
  #[error("internal error")]
  Internal(#[source] AnyError),
}

#[derive(Debug, Error)]
pub enum UnlinkFromTwinoidError {
  #[error("internal `delete_link` error")]
  InternalDeleteLink(#[from] DeleteLinkError<TwinoidUserIdRef>),
  #[error("internal error")]
  Internal(#[source] AnyError),
}

impl<
    TyClock,
    TyDinoparcClient,
    TyDinoparcStore,
    TyHammerfestClient,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTokenStore,
    TyTwinoidClient,
    TyTwinoidStore,
    TyUserStore,
  >
  UserService<
    TyClock,
    TyDinoparcClient,
    TyDinoparcStore,
    TyHammerfestClient,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTokenStore,
    TyTwinoidClient,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyClock: Clock,
  TyDinoparcClient: DinoparcClient + Clone + 'static,
  TyDinoparcStore: DinoparcStore + Clone + 'static,
  TyHammerfestClient: HammerfestClient + Clone + 'static,
  TyHammerfestStore: HammerfestStore + Clone + 'static,
  TyLinkStore: LinkStore,
  TyPassword: PasswordService,
  TyTokenStore: TokenStore,
  TyTwinoidClient: TwinoidClient,
  TyTwinoidStore: TwinoidStore,
  TyUserStore: UserStore,
{
  #[allow(clippy::too_many_arguments)]
  pub fn new(
    clock: TyClock,
    dinoparc_client: TyDinoparcClient,
    dinoparc_store: TyDinoparcStore,
    hammerfest_client: TyHammerfestClient,
    hammerfest_store: TyHammerfestStore,
    link_store: TyLinkStore,
    password: TyPassword,
    token_store: TyTokenStore,
    twinoid_client: TyTwinoidClient,
    twinoid_store: TyTwinoidStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      clock,
      dinoparc_client,
      dinoparc_store,
      hammerfest_client,
      hammerfest_store,
      link_store,
      password,
      token_store,
      twinoid_client,
      twinoid_store,
      user_store,
    }
  }

  pub async fn get_user(&self, acx: &AuthContext, options: &GetUserOptions) -> Result<GetUserResult, GetUserError> {
    let time = options.time.unwrap_or_else(|| self.clock.now());
    let fields: UserFields = match acx {
      AuthContext::User(acx) => {
        if acx.is_administrator {
          UserFields::Complete
        } else {
          UserFields::CompleteIfSelf {
            self_user_id: acx.user.id,
          }
        }
      }
      _ => UserFields::Default,
    };
    let get_user_options = GetUserOptions {
      r#ref: options.r#ref.clone(),
      fields,
      time: Some(time),
      skip_deleted: false,
    };
    let raw_user = self.user_store.get_user(&get_user_options).await;
    let raw_user: RawGetUserResult = match raw_user {
      Ok(Some(u)) => u,
      Ok(None) => return Err(GetUserError::NotFound),
      Err(e) => return Err(GetUserError::Internal(e)),
    };
    let raw_links: VersionedRawLinks = self
      .link_store
      .get_links_from_etwin(&GetLinksFromEtwinOptions {
        etwin: raw_user.id().into(),
        time: Some(time),
      })
      .await
      .map_err(GetUserError::Internal)?;
    let links = self
      .resolve_links(raw_links, time)
      .await
      .map_err(GetUserError::Internal)?;
    Ok(match raw_user {
      RawGetUserResult::Short(_) => unreachable!("expected `default` or `complete` user"),
      RawGetUserResult::Default(raw_user) => GetUserResult::Default(User {
        id: raw_user.id,
        created_at: raw_user.created_at,
        display_name: raw_user.display_name,
        is_administrator: raw_user.is_administrator,
        links,
      }),
      RawGetUserResult::Complete(raw_user) => {
        let has_password = self
          .user_store
          .get_user_with_password(&get_user_options)
          .await
          .map_err(GetUserError::Internal)?
          .map(|u| u.password.is_some())
          .expect("user should exist");
        GetUserResult::Complete(CompleteUser {
          id: raw_user.id,
          created_at: raw_user.created_at,
          display_name: raw_user.display_name,
          is_administrator: raw_user.is_administrator,
          links,
          username: raw_user.username,
          email_address: raw_user.email_address,
          has_password,
        })
      }
    })
  }

  pub async fn update_user(
    &self,
    acx: AuthContext,
    options: UpdateUserOptions,
  ) -> Result<CompleteUser, UpdateUserError> {
    let time = self.clock.now();
    let actor = match acx {
      AuthContext::User(acx) => acx,
      _ => return Err(UpdateUserError::Forbidden),
    };
    if !(actor.is_administrator || actor.user.as_ref() == options.r#ref) {
      return Err(UpdateUserError::Forbidden);
    }
    let raw_patch = RawUpdateUserPatch {
      display_name: options.patch.display_name.clone(),
      username: options.patch.username.clone(),
      password: match options.patch.password {
        Some(Some(new_password)) => {
          let new_hash = self.password.hash(new_password);
          Some(Some(new_hash))
        }
        Some(None) => Some(None),
        None => None,
      },
    };
    let raw_user = self
      .user_store
      .update_user(&RawUpdateUserOptions {
        actor: actor.user.as_ref(),
        r#ref: options.r#ref,
        patch: raw_patch,
      })
      .await?;
    let raw_links: VersionedRawLinks = self
      .link_store
      .get_links_from_etwin(&GetLinksFromEtwinOptions {
        etwin: options.r#ref,
        time: Some(time),
      })
      .await
      .map_err(UpdateUserError::Internal)?;
    let links = self
      .resolve_links(raw_links, time)
      .await
      .map_err(UpdateUserError::Internal)?;
    let has_password = self
      .user_store
      .get_user_with_password(&GetUserOptions {
        r#ref: options.r#ref.into(),
        fields: UserFields::Default,
        time: Some(time),
        skip_deleted: false,
      })
      .await
      .map_err(UpdateUserError::Internal)?
      .map(|u| u.password.is_some())
      .expect("user should exist");
    Ok(CompleteUser {
      id: raw_user.id,
      created_at: raw_user.created_at,
      display_name: raw_user.display_name,
      is_administrator: raw_user.is_administrator,
      links,
      username: raw_user.username,
      email_address: raw_user.email_address,
      has_password,
    })
  }

  pub async fn delete_user(
    &self,
    acx: AuthContext,
    options: DeleteUserOptions,
  ) -> Result<CompleteUser, DeleteUserError> {
    let time = self.clock.now();
    let actor = match acx {
      AuthContext::User(acx) => acx,
      _ => return Err(DeleteUserError::Forbidden),
    };
    if !(actor.is_administrator || actor.user.as_ref() == options.r#ref) {
      return Err(DeleteUserError::Forbidden);
    }
    let raw_links: VersionedRawLinks = self
      .link_store
      .get_links_from_etwin(&GetLinksFromEtwinOptions {
        etwin: options.r#ref,
        time: Some(time),
      })
      .await
      .map_err(DeleteUserError::Other)?;
    let links = self
      .resolve_links(raw_links, time)
      .await
      .map_err(DeleteUserError::Other)?;
    let raw_user = self
      .user_store
      .delete_user(&RawDeleteUser {
        actor: actor.user.as_ref(),
        r#user: options.r#ref,
        now: time,
      })
      .await?;
    self
      .link_store
      .delete_all_links(&RawDeleteAllLinks {
        etwin: options.r#ref,
        now: time,
        unlinked_by: actor.user.as_ref(),
      })
      .await
      .map_err(DeleteUserError::Other)?;
    Ok(CompleteUser {
      id: raw_user.id,
      created_at: raw_user.created_at,
      display_name: raw_user.display_name,
      is_administrator: raw_user.is_administrator,
      links,
      username: raw_user.username,
      email_address: raw_user.email_address,
      has_password: raw_user.has_password,
    })
  }

  pub async fn link_to_dinoparc(
    &self,
    acx: AuthContext,
    options: LinkToDinoparcOptions,
  ) -> Result<VersionedLink<ShortDinoparcUser>, LinkToDinoparcError> {
    let time = self.clock.now();
    let (acx, etwin, remote) = match options {
      LinkToDinoparcOptions::Credentials(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.user.id == options.user_id => acx,
          _ => return Err(LinkToDinoparcError::Internal("forbidden".into())),
        };
        let remote_session = self
          .dinoparc_client
          .create_session(&DinoparcCredentials {
            server: options.dinoparc_server,
            username: options.dinoparc_username,
            password: options.dinoparc_password,
          })
          .await
          .map_err(LinkToDinoparcError::Internal)?;
        self
          .dinoparc_store
          .touch_short_user(&remote_session.user)
          .await
          .map_err(LinkToDinoparcError::Internal)?;
        tokio::spawn(archive_dinoparc(
          self.dinoparc_client.clone(),
          self.dinoparc_store.clone(),
          remote_session.clone(),
        ));
        self
          .token_store
          .touch_dinoparc(remote_session.user.as_ref(), &remote_session.key)
          .await
          .map_err(LinkToDinoparcError::Internal)?;
        (acx, options.user_id, remote_session.user.as_ref())
      }
      LinkToDinoparcOptions::Ref(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.is_administrator => acx,
          _ => return Err(LinkToDinoparcError::Internal("forbidden".into())),
        };
        let remote = DinoparcUserIdRef {
          server: options.dinoparc_server,
          id: options.dinoparc_user_id,
        };
        (acx, options.user_id, remote)
      }
    };
    let raw = self
      .link_store
      .touch_dinoparc_link(&TouchLinkOptions {
        etwin: etwin.into(),
        remote,
        linked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_dinoparc_link(raw, time)
      .await
      .map_err(LinkToDinoparcError::Internal)
  }

  pub async fn unlink_from_dinoparc(
    &self,
    acx: AuthContext,
    options: UnlinkFromDinoparcOptions,
  ) -> Result<VersionedLink<ShortDinoparcUser>, UnlinkFromDinoparcError> {
    let time = self.clock.now();
    let acx = match acx {
      AuthContext::User(acx) if acx.user.id == options.user_id || acx.is_administrator => acx,
      _ => return Err(UnlinkFromDinoparcError::Internal("forbidden".into())),
    };
    let raw = self
      .link_store
      .delete_dinoparc_link(&DeleteLinkOptions {
        etwin: options.user_id.into(),
        remote: DinoparcUserIdRef {
          server: options.dinoparc_server,
          id: options.dinoparc_user_id,
        },
        unlinked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_dinoparc_link(raw, time)
      .await
      .map_err(UnlinkFromDinoparcError::Internal)
  }

  pub async fn link_to_hammerfest(
    &self,
    acx: AuthContext,
    options: LinkToHammerfestOptions,
  ) -> Result<VersionedLink<ShortHammerfestUser>, LinkToHammerfestError> {
    let time = self.clock.now();
    let (acx, etwin, remote) = match options {
      LinkToHammerfestOptions::Credentials(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.user.id == options.user_id => acx,
          _ => return Err(LinkToHammerfestError::Internal("forbidden".into())),
        };
        let remote_session = self
          .hammerfest_client
          .create_session(&HammerfestCredentials {
            server: options.hammerfest_server,
            username: options.hammerfest_username,
            password: options.hammerfest_password,
          })
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        self
          .hammerfest_store
          .touch_short_user(&remote_session.user)
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        tokio::spawn(archive_hammerfest(
          self.hammerfest_client.clone(),
          self.hammerfest_store.clone(),
          remote_session.clone(),
        ));
        self
          .token_store
          .touch_hammerfest(remote_session.user.as_ref(), &remote_session.key)
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        (acx, options.user_id, remote_session.user.as_ref())
      }
      LinkToHammerfestOptions::SessionKey(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.user.id == options.user_id => acx,
          _ => return Err(LinkToHammerfestError::Internal("forbidden".into())),
        };
        let remote_session = self
          .hammerfest_client
          .test_session(options.hammerfest_server, &options.hammerfest_session_key)
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        let remote_session = match remote_session {
          Some(p) => p,
          None => return Err(LinkToHammerfestError::Internal("invalid remote session key".into())),
        };
        self
          .hammerfest_store
          .touch_short_user(&remote_session.user)
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        tokio::spawn(archive_hammerfest(
          self.hammerfest_client.clone(),
          self.hammerfest_store.clone(),
          remote_session.clone(),
        ));
        self
          .token_store
          .touch_hammerfest(remote_session.user.as_ref(), &remote_session.key)
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        (acx, options.user_id, remote_session.user.as_ref())
      }
      LinkToHammerfestOptions::Ref(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.is_administrator => acx,
          _ => return Err(LinkToHammerfestError::Internal("forbidden".into())),
        };
        let remote_profile = self
          .hammerfest_client
          .get_profile_by_id(
            None,
            &HammerfestGetProfileByIdOptions {
              server: options.hammerfest_server,
              user_id: options.hammerfest_user_id,
            },
          )
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        let remote_profile = match remote_profile.profile {
          Some(p) => p,
          None => return Err(LinkToHammerfestError::Internal("remote not found".into())),
        };
        self
          .hammerfest_store
          .touch_short_user(&remote_profile.user)
          .await
          .map_err(LinkToHammerfestError::Internal)?;
        (acx, options.user_id, remote_profile.user.as_ref())
      }
    };
    let raw = self
      .link_store
      .touch_hammerfest_link(&TouchLinkOptions {
        etwin: etwin.into(),
        remote,
        linked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_hammerfest_link(raw, time)
      .await
      .map_err(LinkToHammerfestError::Internal)
  }

  pub async fn unlink_from_hammerfest(
    &self,
    acx: AuthContext,
    options: UnlinkFromHammerfestOptions,
  ) -> Result<VersionedLink<ShortHammerfestUser>, UnlinkFromHammerfestError> {
    let time = self.clock.now();
    let acx = match acx {
      AuthContext::User(acx) if acx.user.id == options.user_id || acx.is_administrator => acx,
      _ => return Err(UnlinkFromHammerfestError::Internal("forbidden".into())),
    };
    let raw = self
      .link_store
      .delete_hammerfest_link(&DeleteLinkOptions {
        etwin: options.user_id.into(),
        remote: HammerfestUserIdRef {
          server: options.hammerfest_server,
          id: options.hammerfest_user_id,
        },
        unlinked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_hammerfest_link(raw, time)
      .await
      .map_err(UnlinkFromHammerfestError::Internal)
  }

  pub async fn link_to_twinoid(
    &self,
    acx: AuthContext,
    options: LinkToTwinoidOptions,
  ) -> Result<VersionedLink<ShortTwinoidUser>, LinkToTwinoidError> {
    let time = self.clock.now();
    let (acx, etwin, remote) = match options {
      LinkToTwinoidOptions::Oauth(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.user.id == options.user_id => acx,
          _ => return Err(LinkToTwinoidError::Internal("forbidden".into())),
        };
        let remote_user = self
          .twinoid_client
          .get_me_short(TwinoidApiAuth::Token(options.access_token.access_token.clone()))
          .await
          .map_err(LinkToTwinoidError::Internal)?;
        let remote_user = ShortTwinoidUser {
          id: TwinoidUserId::new(remote_user.id).expect("invalid id"),
          display_name: remote_user.name,
        };
        self
          .twinoid_store
          .touch_short_user(&remote_user)
          .await
          .map_err(LinkToTwinoidError::Internal)?;
        self
          .token_store
          .touch_twinoid_oauth(&TouchOauthTokenOptions {
            access_token: options.access_token.access_token,
            refresh_token: options.access_token.refresh_token,
            expiration_time: time + chrono::Duration::seconds(options.access_token.expires_in),
            twinoid_user_id: remote_user.id,
          })
          .await
          .map_err(LinkToTwinoidError::Internal)?;
        (acx, options.user_id, remote_user.as_ref())
      }
      LinkToTwinoidOptions::Ref(options) => {
        let acx = match acx {
          AuthContext::User(acx) if acx.is_administrator => acx,
          _ => return Err(LinkToTwinoidError::Internal("forbidden".into())),
        };
        // TODO: Use scraping client to touch user.
        let remote = TwinoidUserIdRef {
          id: options.twinoid_user_id,
        };
        (acx, options.user_id, remote)
      }
    };
    let raw = self
      .link_store
      .touch_twinoid_link(&TouchLinkOptions {
        etwin: etwin.into(),
        remote,
        linked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_twinoid_link(raw, time)
      .await
      .map_err(LinkToTwinoidError::Internal)
  }

  pub async fn unlink_from_twinoid(
    &self,
    acx: AuthContext,
    options: UnlinkFromTwinoidOptions,
  ) -> Result<VersionedLink<ShortTwinoidUser>, UnlinkFromTwinoidError> {
    let time = self.clock.now();
    let acx = match acx {
      AuthContext::User(acx) if acx.user.id == options.user_id || acx.is_administrator => acx,
      _ => return Err(UnlinkFromTwinoidError::Internal("forbidden".into())),
    };
    let raw = self
      .link_store
      .delete_twinoid_link(&DeleteLinkOptions {
        etwin: options.user_id.into(),
        remote: TwinoidUserIdRef {
          id: options.twinoid_user_id,
        },
        unlinked_by: acx.user.as_ref(),
      })
      .await?;
    self
      .resolve_twinoid_link(raw, time)
      .await
      .map_err(UnlinkFromTwinoidError::Internal)
  }
}

impl<
    TyClock,
    TyDinoparcClient,
    TyDinoparcStore,
    TyHammerfestClient,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTokenStore,
    TyTwinoidClient,
    TyTwinoidStore,
    TyUserStore,
  > LinkResolver
  for UserService<
    TyClock,
    TyDinoparcClient,
    TyDinoparcStore,
    TyHammerfestClient,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTokenStore,
    TyTwinoidClient,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyClock: Clock,
  TyDinoparcClient: DinoparcClient,
  TyDinoparcStore: DinoparcStore,
  TyHammerfestClient: HammerfestClient,
  TyHammerfestStore: HammerfestStore,
  TyLinkStore: LinkStore,
  TyPassword: PasswordService,
  TyTokenStore: TokenStore,
  TyTwinoidClient: TwinoidClient,
  TyTwinoidStore: TwinoidStore,
  TyUserStore: UserStore,
{
  type DinoparcStore = TyDinoparcStore;
  type HammerfestStore = TyHammerfestStore;
  type UserStore = TyUserStore;
  type TwinoidStore = TyTwinoidStore;

  fn dinoparc_store(&self) -> &Self::DinoparcStore {
    &self.dinoparc_store
  }

  fn hammerfest_store(&self) -> &Self::HammerfestStore {
    &self.hammerfest_store
  }

  fn user_store(&self) -> &Self::UserStore {
    &self.user_store
  }

  fn twinoid_store(&self) -> &Self::TwinoidStore {
    &self.twinoid_store
  }
}

#[cfg(feature = "neon")]
impl<
    TyClock,
    TyDinoparcClient,
    TyDinoparcStore,
    TyHammerfestClient,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTokenStore,
    TyTwinoidClient,
    TyTwinoidStore,
    TyUserStore,
  > neon::prelude::Finalize
  for UserService<
    TyClock,
    TyDinoparcClient,
    TyDinoparcStore,
    TyHammerfestClient,
    TyHammerfestStore,
    TyLinkStore,
    TyPassword,
    TyTokenStore,
    TyTwinoidClient,
    TyTwinoidStore,
    TyUserStore,
  >
where
  TyClock: Clock,
  TyDinoparcClient: DinoparcClient,
  TyDinoparcStore: DinoparcStore,
  TyHammerfestClient: HammerfestClient,
  TyHammerfestStore: HammerfestStore,
  TyLinkStore: LinkStore,
  TyPassword: PasswordService,
  TyTokenStore: TokenStore,
  TyTwinoidClient: TwinoidClient,
  TyTwinoidStore: TwinoidStore,
  TyUserStore: UserStore,
{
}

pub(crate) async fn archive_dinoparc<TyDinoparcClient, TyDinoparcStore>(
  client: TyDinoparcClient,
  store: TyDinoparcStore,
  remote_session: DinoparcSession,
) where
  TyDinoparcClient: DinoparcClient,
  TyDinoparcStore: DinoparcStore,
{
  let inventory = client.get_inventory(&remote_session).await.unwrap();
  store.touch_inventory(&inventory).await.unwrap();
  let collection = client.get_collection(&remote_session).await.unwrap();
  store.touch_collection(&collection).await.unwrap();
  if inventory.session_user.dinoz.len() < usize::from(MAX_SIDEBAR_DINOZ_COUNT) {
    let dinoz_list = inventory.session_user.dinoz.iter().map(|d| d.id);
    archive_dinoparc_dinoz_list(client, store, remote_session, dinoz_list).await
  } else {
    let targets = client.get_preferred_exchange_with(remote_session.user.server).await;
    let target = if targets[0] != remote_session.user.id {
      targets[0]
    } else {
      targets[1]
    };
    let exchange_with = client.get_exchange_with(&remote_session, target).await.unwrap();
    store.touch_exchange_with(&exchange_with).await.unwrap();
    let dinoz_list = exchange_with.own_dinoz.iter().map(|d| d.id);
    archive_dinoparc_dinoz_list(client, store, remote_session, dinoz_list).await
  };
}

async fn archive_dinoparc_dinoz_list<TyDinoparcClient, TyDinoparcStore, It>(
  client: TyDinoparcClient,
  store: TyDinoparcStore,
  remote_session: DinoparcSession,
  list: It,
) where
  TyDinoparcClient: DinoparcClient,
  TyDinoparcStore: DinoparcStore,
  It: Iterator<Item = DinoparcDinozId>,
{
  for dinoz_id in list {
    tokio::time::sleep(Duration::from_millis(100)).await;
    let dinoz = client.get_dinoz(&remote_session, dinoz_id).await.unwrap();
    store.touch_dinoz(&dinoz).await.unwrap();
  }
}

pub(crate) async fn archive_hammerfest<TyHammerfestClient, TyHammerfestStore>(
  client: TyHammerfestClient,
  store: TyHammerfestStore,
  remote_session: HammerfestSession,
) where
  TyHammerfestClient: HammerfestClient,
  TyHammerfestStore: HammerfestStore,
{
  {
    let res = client.get_own_items(&remote_session).await.unwrap();
    store.touch_inventory(&res).await.unwrap();
  }
  {
    let res = client
      .get_profile_by_id(
        Some(&remote_session),
        &HammerfestGetProfileByIdOptions {
          server: remote_session.user.server,
          user_id: remote_session.user.id,
        },
      )
      .await
      .unwrap();
    store.touch_profile(&res).await.unwrap();
  }
  {
    let res = client.get_own_shop(&remote_session).await.unwrap();
    store.touch_shop(&res).await.unwrap();
  }
  {
    let res = client.get_own_godchildren(&remote_session).await.unwrap();
    store.touch_godchildren(&res).await.unwrap();
  }
}
