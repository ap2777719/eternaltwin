use etwin_core::auth::AuthContext;
use etwin_core::core::UserDot;
use etwin_core::link::{EtwinLink, GetLinkOptions, LinkStore, VersionedEtwinLink, VersionedRawLink};
use etwin_core::twinoid::{
  ArchivedTwinoidUser, EtwinTwinoidUser, GetTwinoidUserOptions, TwinoidStore, TwinoidUserIdRef,
};
use etwin_core::types::AnyError;
use etwin_core::user::{GetShortUserOptions, ShortUser, UserRef, UserStore};
use std::sync::Arc;
use thiserror::Error;

pub struct TwinoidService<TyLinkStore, TyTwinoidStore, TyUserStore>
where
  TyLinkStore: LinkStore,
  TyTwinoidStore: TwinoidStore,
  TyUserStore: UserStore,
{
  link_store: TyLinkStore,
  twinoid_store: TyTwinoidStore,
  user_store: TyUserStore,
}

pub type DynTwinoidService = TwinoidService<Arc<dyn LinkStore>, Arc<dyn TwinoidStore>, Arc<dyn UserStore>>;

#[derive(Debug, Error)]
pub enum GetUserError {
  #[error("twinoid user not found")]
  NotFound,
  #[error("other error")]
  Other(#[source] AnyError),
}

impl<TyLinkStore, TyTwinoidStore, TyUserStore> TwinoidService<TyLinkStore, TyTwinoidStore, TyUserStore>
where
  TyLinkStore: LinkStore,
  TyTwinoidStore: TwinoidStore,
  TyUserStore: UserStore,
{
  pub fn new(link_store: TyLinkStore, twinoid_store: TyTwinoidStore, user_store: TyUserStore) -> Self {
    Self {
      link_store,
      twinoid_store,
      user_store,
    }
  }

  pub async fn get_user(
    &self,
    _acx: &AuthContext,
    options: &GetTwinoidUserOptions,
  ) -> Result<EtwinTwinoidUser, GetUserError> {
    let user: Option<ArchivedTwinoidUser> = self
      .twinoid_store
      .get_user(options)
      .await
      .map_err(GetUserError::Other)?;
    let user: ArchivedTwinoidUser = match user {
      Some(user) => user,
      None => return Err(GetUserError::NotFound),
    };
    let etwin_link: VersionedRawLink<TwinoidUserIdRef> = {
      let options: GetLinkOptions<TwinoidUserIdRef> = GetLinkOptions {
        remote: TwinoidUserIdRef { id: user.id },
        time: None,
      };
      self
        .link_store
        .get_link_from_twinoid(&options)
        .await
        .map_err(GetUserError::Other)?
    };
    let etwin_link: VersionedEtwinLink = {
      let current = match etwin_link.current {
        None => None,
        Some(l) => {
          let user: ShortUser = self
            .user_store
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.link.user),
              time: options.time,
              skip_deleted: false,
            })
            .await
            .map_err(GetUserError::Other)?
            .unwrap();
          let etwin: ShortUser = self
            .user_store
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.etwin),
              time: options.time,
              skip_deleted: false,
            })
            .await
            .map_err(GetUserError::Other)?
            .unwrap();
          Some(EtwinLink {
            link: UserDot {
              time: l.link.time,
              user,
            },
            unlink: (),
            etwin,
          })
        }
      };
      VersionedEtwinLink { current, old: vec![] }
    };
    Ok(EtwinTwinoidUser {
      id: user.id,
      archived_at: user.archived_at,
      display_name: user.display_name,
      etwin: etwin_link,
    })
  }
}

#[cfg(feature = "neon")]
impl<TyLinkStore, TyTwinoidStore, TyUserStore> neon::prelude::Finalize
  for TwinoidService<TyLinkStore, TyTwinoidStore, TyUserStore>
where
  TyLinkStore: LinkStore,
  TyTwinoidStore: TwinoidStore,
  TyUserStore: UserStore,
{
}
