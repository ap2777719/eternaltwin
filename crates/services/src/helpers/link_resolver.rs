use async_trait::async_trait;
use etwin_core::core::{Instant, UserDot};
use etwin_core::dinoparc::{DinoparcStore, DinoparcUserIdRef, GetDinoparcUserOptions, ShortDinoparcUser};
use etwin_core::hammerfest::{GetHammerfestUserOptions, HammerfestStore, HammerfestUserIdRef, ShortHammerfestUser};
use etwin_core::link::{Link, VersionedLink, VersionedLinks, VersionedRawLink, VersionedRawLinks};
use etwin_core::twinoid::{GetTwinoidUserOptions, ShortTwinoidUser, TwinoidStore, TwinoidUserIdRef};
use etwin_core::types::AnyError;
use etwin_core::user::{GetShortUserOptions, UserRef, UserStore};

#[async_trait]
pub trait LinkResolver {
  type DinoparcStore: DinoparcStore;
  type HammerfestStore: HammerfestStore;
  type UserStore: UserStore;
  type TwinoidStore: TwinoidStore;

  fn dinoparc_store(&self) -> &Self::DinoparcStore;
  fn hammerfest_store(&self) -> &Self::HammerfestStore;
  fn user_store(&self) -> &Self::UserStore;
  fn twinoid_store(&self) -> &Self::TwinoidStore;

  async fn resolve_links(&self, raw_links: VersionedRawLinks, time: Instant) -> Result<VersionedLinks, AnyError> {
    Ok(VersionedLinks {
      dinoparc_com: self.resolve_dinoparc_link(raw_links.dinoparc_com, time).await?,
      en_dinoparc_com: self.resolve_dinoparc_link(raw_links.en_dinoparc_com, time).await?,
      hammerfest_es: self.resolve_hammerfest_link(raw_links.hammerfest_es, time).await?,
      hammerfest_fr: self.resolve_hammerfest_link(raw_links.hammerfest_fr, time).await?,
      hfest_net: self.resolve_hammerfest_link(raw_links.hfest_net, time).await?,
      sp_dinoparc_com: self.resolve_dinoparc_link(raw_links.sp_dinoparc_com, time).await?,
      twinoid: self.resolve_twinoid_link(raw_links.twinoid, time).await?,
    })
  }

  async fn resolve_dinoparc_link(
    &self,
    raw: VersionedRawLink<DinoparcUserIdRef>,
    time: Instant,
  ) -> Result<VersionedLink<ShortDinoparcUser>, AnyError> {
    let current = match raw.current {
      Some(current) => {
        let linked_by = self
          .user_store()
          .get_short_user(&GetShortUserOptions {
            r#ref: UserRef::Id(current.link.user),
            time: Some(time),
            skip_deleted: false,
          })
          .await?
          .ok_or_else(|| AnyError::from("current.link.user not found"))?;
        let remote = self
          .dinoparc_store()
          .get_short_user(&GetDinoparcUserOptions {
            server: current.remote.server,
            id: current.remote.id,
            time: Some(time),
          })
          .await?
          .ok_or_else(|| AnyError::from("current.remote not found"))?;
        Some(Link {
          link: UserDot {
            time: current.link.time,
            user: linked_by,
          },
          unlink: (),
          remote,
        })
      }
      None => None,
    };
    Ok(VersionedLink { current, old: vec![] })
  }

  async fn resolve_hammerfest_link(
    &self,
    raw: VersionedRawLink<HammerfestUserIdRef>,
    time: Instant,
  ) -> Result<VersionedLink<ShortHammerfestUser>, AnyError> {
    let current = match raw.current {
      Some(current) => {
        let linked_by = self
          .user_store()
          .get_short_user(&GetShortUserOptions {
            r#ref: UserRef::Id(current.link.user),
            time: Some(time),
            skip_deleted: false,
          })
          .await?
          .ok_or_else(|| AnyError::from("current.link.user not found"))?;
        let remote = self
          .hammerfest_store()
          .get_short_user(&GetHammerfestUserOptions {
            server: current.remote.server,
            id: current.remote.id,
            time: Some(time),
          })
          .await?
          .ok_or_else(|| AnyError::from("current.remote not found"))?;
        Some(Link {
          link: UserDot {
            time: current.link.time,
            user: linked_by,
          },
          unlink: (),
          remote,
        })
      }
      None => None,
    };
    Ok(VersionedLink { current, old: vec![] })
  }

  async fn resolve_twinoid_link(
    &self,
    raw: VersionedRawLink<TwinoidUserIdRef>,
    time: Instant,
  ) -> Result<VersionedLink<ShortTwinoidUser>, AnyError> {
    let current = match raw.current {
      Some(current) => {
        let linked_by = self
          .user_store()
          .get_short_user(&GetShortUserOptions {
            r#ref: UserRef::Id(current.link.user),
            time: Some(time),
            skip_deleted: false,
          })
          .await?
          .ok_or_else(|| AnyError::from("current.link.user not found"))?;
        let remote = self
          .twinoid_store()
          .get_short_user(&GetTwinoidUserOptions {
            id: current.remote.id,
            time: Some(time),
          })
          .await?
          .ok_or_else(|| AnyError::from("current.remote not found"))?;
        Some(Link {
          link: UserDot {
            time: current.link.time,
            user: linked_by,
          },
          unlink: (),
          remote,
        })
      }
      None => None,
    };
    Ok(VersionedLink { current, old: vec![] })
  }
}
