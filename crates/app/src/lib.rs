use include_dir::include_dir;
pub use include_dir::{Dir, DirEntry, File};

pub static BROWSER: Dir = include_dir!("$CARGO_MANIFEST_DIR/browser");
