use etwin_core::dinorpg::{DinorpgDinozId, DinorpgServer, DinorpgUserId};
use etwin_core::dns::DnsResolver;
use reqwest::Url;

pub struct DinorpgUrls {
  root: Url,
  host: &'static str,
}

impl DinorpgUrls {
  pub fn new<TyDnsResolver>(dns_resolver: &TyDnsResolver, server: DinorpgServer) -> Self
  where
    TyDnsResolver: DnsResolver<DinorpgServer>,
  {
    let host = match server {
      DinorpgServer::DinorpgCom => "www.dinorpg.com",
      DinorpgServer::EnDinorpgCom => "en.dinorpg.com",
      DinorpgServer::EsDinorpgCom => "es.dinorpg.com",
    };
    let root = if let Some(addr) = dns_resolver.resolve4(&server) {
      format!("http://{}", addr)
    } else {
      format!("http://{}", host)
    };
    Self {
      root: Url::parse(&root).expect("failed to parse dinorpg root URL"),
      host,
    }
  }

  fn make_url(&self, segments: &[&str]) -> Url {
    let mut url = self.root.clone();
    url.path_segments_mut().expect("invalid root url").extend(segments);
    url
  }

  pub fn root(&self) -> Url {
    self.root.clone()
  }

  #[allow(unused)]
  pub fn user(&self, user_id: DinorpgUserId) -> Url {
    user_id.with_str(|user_id| self.make_url(&["user", user_id]))
  }

  #[allow(unused)]
  pub fn demon_shop(&self) -> Url {
    self.make_url(&["shop", "demon"])
  }

  #[allow(unused)]
  pub fn missions(&self) -> Url {
    self.make_url(&["dino", "missions"])
  }

  #[allow(unused)]
  pub fn dino(&self, dino_id: DinorpgDinozId) -> Url {
    dino_id.with_str(|dino_id| self.make_url(&["user", dino_id]))
  }

  pub fn ingredients(&self) -> Url {
    self.make_url(&["user", "ingr"])
  }

  pub fn host(&self) -> &'static str {
    self.host
  }

  #[allow(unused)]
  pub fn parse_from_root(&self, href: &str) -> Result<Url, url::ParseError> {
    Url::options().base_url(Some(&self.root)).parse(href)
  }
}
