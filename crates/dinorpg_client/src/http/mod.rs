mod errors;
mod locale;
mod scraper;
mod url;

use crate::http::url::DinorpgUrls;
use ::scraper::Html;
use async_trait::async_trait;
use etwin_core::clock::Clock;
use etwin_core::dinorpg::{
  DinorpgClient, DinorpgClientCreateSessionError, DinorpgDemonShopResponse, DinorpgDinozMissionsResponse,
  DinorpgIngredientResponse, DinorpgServer, DinorpgSession, DinorpgSessionKey, DinorpgSessionKeyParseError,
  DinorpgUserId, DinorpgUserIdParseError, ShortDinorpgUser,
};
use etwin_core::dns::DnsResolver;
use etwin_core::twinoid::TwinoidSessionKey;
use etwin_core::types::{to_any_error, AnyError};
use etwin_mt_dns::MtDnsResolver;
use etwin_mt_querystring::UrlExt;
use etwin_twinoid_client::http::{
  scrape_tid_bar_view, scrape_tid_config, TidBarView, TidConfig, TwinoidUrls, TwinoidUserInfos,
  TwinoidUserInfosParseError,
};
use reqwest::{Client, StatusCode};
use std::time::Duration;
use thiserror::Error;

const USER_AGENT: &str = "EtwinDinorpgScraper";
const TIMEOUT: Duration = Duration::from_millis(5000);

pub struct HttpDinorpgClient<TyClock, TyDnsResolver> {
  client: Client,
  #[allow(unused)]
  clock: TyClock,
  dns_resolver: TyDnsResolver,
}

impl<TyClock> HttpDinorpgClient<TyClock, MtDnsResolver>
where
  TyClock: Clock,
{
  pub fn new(clock: TyClock) -> Result<Self, AnyError> {
    Self::new_with_resolver(clock, MtDnsResolver)
  }
}

impl<TyClock, TyDnsResolver> HttpDinorpgClient<TyClock, TyDnsResolver>
where
  TyClock: Clock,
  TyDnsResolver: DnsResolver<DinorpgServer>,
{
  pub fn new_with_resolver(clock: TyClock, dns_resolver: TyDnsResolver) -> Result<Self, AnyError> {
    Ok(Self {
      client: Client::builder()
        .user_agent(USER_AGENT)
        .timeout(TIMEOUT)
        .redirect(reqwest::redirect::Policy::none())
        .build()?,
      clock,
      dns_resolver,
    })
  }

  async fn get_html(
    &self,
    host: &str,
    url: reqwest::Url,
    session: Option<&DinorpgSessionKey>,
  ) -> reqwest::Result<Html> {
    let mut builder = self.client.get(url);
    if let Some(key) = session {
      // No need to escape, per HammerfestSessionKey invariants.
      let session_cookie = "sid=".to_owned() + key.as_str();
      builder = builder.header(reqwest::header::COOKIE, session_cookie);
    }
    let resp = builder.header(reqwest::header::HOST, host).send().await?;
    let text = resp.error_for_status()?.text().await?;
    Ok(Html::parse_document(&text))
  }
}

#[derive(Debug, Error)]
enum HttpDinorpgClientCreateSessionError {
  #[error("failed to send initial (session key & tid config) request")]
  InitialRequest(#[source] reqwest::Error),
  #[error("unexpected status for initial (session key & tid config) request: {0:?}")]
  InitialStatus(StatusCode),
  #[error("initial (session key & tid config) response is missing session cookie")]
  SessionKeyNotFound,
  #[error("session cookie value is invalid")]
  InvalidSessionKey(#[source] DinorpgSessionKeyParseError),
  #[error("failed to download initial (session key & tid config) response")]
  InitialResponseDownload(#[source] reqwest::Error),
  #[error("failed to read Twinoid config from initial response")]
  InitialResponseRead(#[source] etwin_twinoid_client::http::ScraperError),
  #[error("failed to send redirector request")]
  RedirectorRequest(#[source] reqwest::Error),
  #[error("unexpected status for login redirector request: {0:?}")]
  RedirectorStatus(StatusCode),
  #[error("failed to download Twinoid redirector response")]
  RedirectorResponseDownload(#[source] reqwest::Error),
  #[error("failed to read url from Twinoid redirector response")]
  RedirectorResponseRead(#[source] etwin_twinoid_client::http::ScraperError),
  #[error("twinoid redirect has unexpected host")]
  ReturnHost,
  #[error("twinoid redirect is missing user infos query parameter")]
  InfosNotFound,
  #[error("twinoid redirect user infos value is invalid")]
  InvalidInfos(#[source] TwinoidUserInfosParseError),
  #[error("twinoid redirect user infos value is missing dinorpg id")]
  MissingDinorpgId,
  #[error("twinoid redirect user infos value has an invalid dinorpg id")]
  InvalidDinorpgId(#[source] DinorpgUserIdParseError),
  #[error("failed to send initial confirm request")]
  ConfirmRequest(#[source] reqwest::Error),
  #[error("unexpected status for confirm request: {0:?}")]
  ConfirmStatus(StatusCode),
}

impl From<HttpDinorpgClientCreateSessionError> for DinorpgClientCreateSessionError {
  fn from(err: HttpDinorpgClientCreateSessionError) -> Self {
    Self::Other(to_any_error(err))
  }
}

#[async_trait]
impl<TyClock, TyDnsResolver> DinorpgClient for HttpDinorpgClient<TyClock, TyDnsResolver>
where
  TyClock: Clock,
  TyDnsResolver: DnsResolver<DinorpgServer>,
{
  async fn get_own_ingredients(&self, session: &DinorpgSession) -> Result<DinorpgIngredientResponse, AnyError> {
    let urls = DinorpgUrls::new(&self.dns_resolver, session.user.server);
    let html = self
      .get_html(urls.host(), urls.ingredients(), Some(&session.key))
      .await?;
    let response = scraper::scrape_ingredients(&html)?;
    Ok(response)
  }

  async fn get_own_missions(&self, session: &DinorpgSession) -> Result<DinorpgDinozMissionsResponse, AnyError> {
    let urls = DinorpgUrls::new(&self.dns_resolver, session.user.server);
    let mut builder = self.client.get(urls.missions());
    // No need to escape, per DinorpgSessionKey invariants.
    builder = builder.header(reqwest::header::COOKIE, format!("sid={}", session.key.as_str()));
    let resp = builder.header(reqwest::header::HOST, urls.host()).send().await?;
    if resp.status() == StatusCode::FOUND {
      return Err("MissingMissions".into());
    }
    let text = resp.error_for_status()?.text().await?;
    let html = Html::parse_document(&text);
    let response = scraper::scrape_missions(&html)?;
    Ok(response)
  }

  async fn get_own_demon_shop(&self, session: &DinorpgSession) -> Result<DinorpgDemonShopResponse, AnyError> {
    let urls = DinorpgUrls::new(&self.dns_resolver, session.user.server);
    let mut builder = self.client.get(urls.demon_shop());
    // No need to escape, per DinorpgSessionKey invariants.
    builder = builder.header(reqwest::header::COOKIE, format!("sid={}", session.key.as_str()));
    let resp = builder.header(reqwest::header::HOST, urls.host()).send().await?;
    if resp.status() == StatusCode::FOUND {
      return Err("MissingDemonShop".into());
    }
    let text = resp.error_for_status()?.text().await?;
    let html = Html::parse_document(&text);
    let response = scraper::scrape_demon_shop(&html)?;
    Ok(response)
  }

  async fn create_session(
    &self,
    server: DinorpgServer,
    tid_key: &TwinoidSessionKey,
  ) -> Result<DinorpgSession, DinorpgClientCreateSessionError> {
    let urls = DinorpgUrls::new(&self.dns_resolver, server);
    let root_url = urls.root();

    let req = self
      .client
      .get(root_url.clone())
      .header(reqwest::header::HOST, urls.host());

    let res = req
      .send()
      .await
      .map_err(HttpDinorpgClientCreateSessionError::InitialRequest)?;

    if res.status() != StatusCode::OK {
      return Err(HttpDinorpgClientCreateSessionError::InitialStatus(res.status()).into());
    }

    let session_key = res
      .cookies()
      .find(|cookie| cookie.name() == "sid")
      .map(|cookie| cookie.value().to_owned())
      .ok_or(HttpDinorpgClientCreateSessionError::SessionKeyNotFound)?;
    let session_key: DinorpgSessionKey = session_key
      .parse()
      .map_err(HttpDinorpgClientCreateSessionError::InvalidSessionKey)?;

    let body = res
      .text()
      .await
      .map_err(HttpDinorpgClientCreateSessionError::InitialResponseDownload)?;

    let tid_config: TidConfig = scrape_tid_config(&::scraper::Html::parse_document(&body))
      .map_err(HttpDinorpgClientCreateSessionError::InitialResponseRead)?;

    let tid_urls = TwinoidUrls::new();
    let mut bar_view_url = tid_urls.bar_view();
    bar_view_url
      .mt_query_pairs_mut()
      .append_pair("lang", &tid_config.lang)
      .append_pair("chk", &tid_config.chk)
      .append_pair("ver", &tid_config.ver)
      .append_pair("infos", &tid_config.infos)
      .append_pair("tz", "-60")
      .append_pair("fver", "100.0.0")
      .append_pair("jsm", "1")
      .append_pair("url", root_url.path())
      .append_pair("host", urls.host())
      .append_pair("proto", root_url.scheme());

    let req = self
      .client
      .get(bar_view_url.clone())
      .header(reqwest::header::REFERER, root_url.to_string())
      .header(
        reqwest::header::COOKIE,
        format!("curFeat=1; tw_sid={}; tp_login=1", tid_key),
      )
      .header(reqwest::header::HOST, tid_urls.host());

    let res = req
      .send()
      .await
      .map_err(HttpDinorpgClientCreateSessionError::RedirectorRequest)?;

    if res.status() != StatusCode::OK {
      return Err(HttpDinorpgClientCreateSessionError::RedirectorStatus(res.status()).into());
    }

    let body = res
      .text()
      .await
      .map_err(HttpDinorpgClientCreateSessionError::RedirectorResponseDownload)?;

    let bar = scrape_tid_bar_view(&body).map_err(HttpDinorpgClientCreateSessionError::RedirectorResponseRead)?;
    let redirect_url = match bar {
      TidBarView::Redirect(r) => r.url,
    };

    if redirect_url.domain() != Some(urls.host()) {
      return Err(HttpDinorpgClientCreateSessionError::ReturnHost.into());
    }

    let infos = redirect_url
      .mt_query_pairs()
      .find_map(|(k, v)| if k == "infos" { Some(v) } else { None });
    let infos = infos.ok_or(HttpDinorpgClientCreateSessionError::InfosNotFound)?;

    let infos: TwinoidUserInfos = infos
      .as_ref()
      .parse()
      .map_err(HttpDinorpgClientCreateSessionError::InvalidInfos)?;

    let req = self
      .client
      .get(redirect_url.clone())
      .header(reqwest::header::REFERER, root_url.to_string())
      .header(reqwest::header::COOKIE, format!("sid={}", session_key))
      .header(reqwest::header::HOST, urls.host());

    let res = req
      .send()
      .await
      .map_err(HttpDinorpgClientCreateSessionError::ConfirmRequest)?;

    if res.status() != StatusCode::FOUND {
      return Err(HttpDinorpgClientCreateSessionError::ConfirmStatus(res.status()).into());
    }

    let id = infos
      .real_id
      .ok_or(HttpDinorpgClientCreateSessionError::MissingDinorpgId)?;

    Ok(DinorpgSession {
      key: session_key,
      user: ShortDinorpgUser {
        server,
        id: DinorpgUserId::new(id).map_err(HttpDinorpgClientCreateSessionError::InvalidDinorpgId)?,
        display_name: infos.name,
      },
    })
  }
}

#[cfg(feature = "neon")]
impl<TyClock, TyDnsResolver> neon::prelude::Finalize for HttpDinorpgClient<TyClock, TyDnsResolver>
where
  TyClock: Clock,
  TyDnsResolver: DnsResolver<DinorpgServer>,
{
}
