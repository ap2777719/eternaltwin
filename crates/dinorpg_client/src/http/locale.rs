use std::collections::HashMap;

use etwin_constants::dinorpg::{CHARACTERS, INGREDIENTS, MISSIONS, SKILLS};
use etwin_core::dinorpg::{DinorpgIngredient, DinorpgMission, DinorpgNpc, DinorpgServer, DinorpgSkill};
use once_cell::sync::Lazy;

pub struct ScraperLocale {
  pub skill_names: HashMap<&'static str, DinorpgSkill>,
  pub npc_names: HashMap<&'static str, DinorpgNpc>,
  pub mission: HashMap<&'static str, DinorpgMission>,
  pub ingredient_name: HashMap<&'static str, DinorpgIngredient>,
}

impl ScraperLocale {
  pub fn get(server: DinorpgServer) -> &'static Self {
    match server {
      DinorpgServer::DinorpgCom => &SCRAPER_LOCALE_FR,
      DinorpgServer::EsDinorpgCom => &SCRAPER_LOCALE_ES,
      DinorpgServer::EnDinorpgCom => &SCRAPER_LOCALE_EN,
    }
  }
}

#[allow(dead_code)]
static SCRAPER_LOCALE_FR: Lazy<ScraperLocale> = Lazy::new(|| ScraperLocale {
  skill_names: SKILLS
    .into_iter()
    .filter_map(|s| {
      if s.name_fr.is_empty() {
        None
      } else {
        Some((s.name_fr, s.skill))
      }
    })
    .collect(),
  npc_names: CHARACTERS.into_iter().map(|l| (l.name_fr, l.npc)).collect(),
  mission: MISSIONS.into_iter().map(|l| (l.name_fr, l.mission)).collect(),
  ingredient_name: INGREDIENTS.into_iter().map(|l| (l.name_fr, l.ingredient)).collect(),
});

#[allow(dead_code)]
static SCRAPER_LOCALE_ES: Lazy<ScraperLocale> = Lazy::new(|| ScraperLocale {
  skill_names: SKILLS
    .into_iter()
    .filter_map(|s| {
      if s.name_es.is_empty() {
        None
      } else {
        Some((s.name_es, s.skill))
      }
    })
    .collect(),
  npc_names: CHARACTERS.into_iter().map(|l| (l.name_es, l.npc)).collect(),
  mission: MISSIONS.into_iter().map(|l| (l.name_es, l.mission)).collect(),
  ingredient_name: INGREDIENTS.into_iter().map(|l| (l.name_es, l.ingredient)).collect(),
});

#[allow(dead_code)]
static SCRAPER_LOCALE_EN: Lazy<ScraperLocale> = Lazy::new(|| ScraperLocale {
  skill_names: SKILLS
    .into_iter()
    .filter_map(|s| {
      if s.name_en.is_empty() {
        None
      } else {
        Some((s.name_en, s.skill))
      }
    })
    .collect(),
  npc_names: CHARACTERS.into_iter().map(|l| (l.name_en, l.npc)).collect(),
  mission: MISSIONS.into_iter().map(|l| (l.name_en, l.mission)).collect(),
  ingredient_name: INGREDIENTS.into_iter().map(|l| (l.name_en, l.ingredient)).collect(),
});
