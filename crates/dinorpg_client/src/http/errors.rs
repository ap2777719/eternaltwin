use thiserror::Error;

#[allow(unused)]
#[derive(Debug, Error)]
pub enum ScraperError {
  #[error("HTTP Error")]
  HttpError(#[from] reqwest::Error),
  #[error("Missing before last script tag")]
  MissingBeforeLastScriptTag,
  #[error("Text is missing inside the script tag")]
  MissingTextFromScriptTag,
  #[error("Invalid language")]
  InvalidLanguage,
  #[error("Invalid player id {:?}", .0)]
  InvalidPlayerId(String),
  #[error("Missing profile tag")]
  MissingProfileTag,
  #[error("Missing href tag when scraping user id")]
  MissingHrefTag,
  #[error("Missing place image")]
  MissingPlaceImage,
  #[error("The link to go to dinoz page is missing")]
  MissingDinozLink,
  #[error("Invalid dinoz id {:?}", .0)]
  InvalidDinozId(String),
  #[error("Invalid dinoz skill name {:?}", .0)]
  InvalidSkillName(String),
  #[error("Missing span tag")]
  MissingSpanTag,
  #[error("Attribute onmouseover is missing")]
  MissingAttributeOnMouseOver,
  #[error("Cannot split text {:?}", .0)]
  CannotSplitText(String),
  #[error("Non unique sacrified dinoz name")]
  NonUniqueSacrificedDinozName,
  #[error("Non unique sacrified dinoz race")]
  NonUniqueSacrificedDinozRace,
  #[error("Missing sacrified dinoz name")]
  MissingSacrificedDinozName,
  #[error("Invalid sacrified dinoz name : {:?}", .0)]
  InvalidSacrificedDinozName(String),
  #[error("Non unique sacrified dinoz level")]
  NonUniqueSacrificedDinozLevel,
  #[error("Non unique strong level tag")]
  NonUniqueStrongLevelTag,
  #[error("Non unique buy button in demon dinoz shop")]
  NonUniqueBuyButton,
  #[error("Invalid dinoz element : {:?}", .0)]
  InvalidDinozElement(String),
  #[error("Invalid id tag")]
  InvalidIdTag,
  #[error("Invalid element text")]
  InvalidElementText,
  #[error("Missing dinoz id")]
  MissingDinozId,
  #[error("Missing character name")]
  MissingCharacterName,
  #[error("Invalid character name {:?}", .0)]
  InvalidCharacterName(String),
  #[error("Invalid mission name {:?}", .0)]
  InvalidMissionName(String),
  #[error("Invalid ingredient name {:?}", .0)]
  InvalidIngredientName(String),
  #[error("Missing ingredient name")]
  MissingIngredientName,
  #[error("Invalid ingredient name")]
  InvalidIngredientNameText,
  #[error("Invalid ingredient quantity {:?}", .0)]
  InvalidQuantity(String),
  #[error("Missing ingredient quantity")]
  MissingIngredientQuantity,
  #[error("Invalid ingredient quantity")]
  InvalidIngredientQuantityText,
}
