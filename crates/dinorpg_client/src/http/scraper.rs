use crate::http::errors::ScraperError;
use crate::http::locale::ScraperLocale;
use etwin_core::dinorpg::{
  DinorpgDemonShopResponse, DinorpgDinozElements, DinorpgDinozId, DinorpgDinozMission, DinorpgDinozMissionsResponse,
  DinorpgDinozName, DinorpgDinozSkillsResponse, DinorpgDinozSkinCheck, DinorpgIngredient, DinorpgIngredientCount,
  DinorpgIngredientResponse, DinorpgMission, DinorpgNpc, DinorpgProfile, DinorpgServer, DinorpgSkill, DinorpgUserId,
  SacrificedDinoz, ShortDinorpgUser,
};
use etwin_scraper_tools::ElementRefExt;
use itertools::Itertools;
use scraper::{ElementRef, Html, Selector};
use std::collections::HashMap;
use std::str::FromStr;

#[derive(Debug)]
struct ScraperContext {
  server: DinorpgServer,
  id: DinorpgUserId,
}

fn scrape_context(doc: ElementRef) -> Result<ScraperContext, ScraperError> {
  // Récupération du serveur
  let script_tag_array = doc.select(&Selector::parse("script").unwrap()).collect_vec();

  if script_tag_array.len() < 2 {
    return Err(ScraperError::MissingBeforeLastScriptTag);
  }

  let before_last_script_tag = match script_tag_array.get(script_tag_array.len() - 2) {
    Some(e) => *e,
    None => return Err(ScraperError::MissingBeforeLastScriptTag),
  };

  let text_script_tag = before_last_script_tag
    .get_one_text()
    .map_err(|_| ScraperError::MissingTextFromScriptTag)?;

  let server = match text_script_tag.get(15..17) {
    Some("fr") => DinorpgServer::DinorpgCom,
    Some("en") => DinorpgServer::EnDinorpgCom,
    Some("es") => DinorpgServer::EsDinorpgCom,
    _ => return Err(ScraperError::InvalidLanguage),
  };

  // Récupération de l'id du joueur
  let footer = doc
    .select(&Selector::parse("div.footer ul > li a").unwrap())
    .skip(5)
    .collect_vec();

  let user_link = match footer.first() {
    Some(e) => *e,
    None => return Err(ScraperError::MissingProfileTag),
  };

  let user_link = user_link.value().attr("href").ok_or(ScraperError::MissingHrefTag)?;

  let id =
    DinorpgUserId::from_str(&user_link[6..]).map_err(|_e| ScraperError::InvalidPlayerId(user_link.to_string()))?;

  Ok(ScraperContext { server, id })
}

#[allow(unused)]
pub(crate) fn scrape_skills(doc: &Html) -> Result<DinorpgDinozSkillsResponse, ScraperError> {
  let root = doc.root_element();

  let ScraperContext { server, id } = scrape_context(root)?;

  let place_image = doc
    .select(&Selector::parse("div.view a").unwrap())
    .exactly_one()
    .map_err(|_| ScraperError::MissingPlaceImage)?;

  let dinoz_page_link = place_image.value().attr("href").ok_or(ScraperError::MissingDinozLink)?;

  let dinoz_id = DinorpgDinozId::from_str(&dinoz_page_link[6..])
    .map_err(|_| ScraperError::InvalidDinozId(dinoz_page_link.to_string()))?;

  let skills: Vec<DinorpgSkill> = {
    let skills: Result<Vec<DinorpgSkill>, ScraperError> = doc
      .select(&Selector::parse("div.details table tbody > tr").unwrap())
      .skip(1)
      .map(|skill| -> Result<DinorpgSkill, ScraperError> {
        let img_tag = skill.select(&Selector::parse(":scope td span").unwrap()).collect_vec();

        let span_tag = match img_tag.first() {
          Some(e) => *e,
          None => return Err(ScraperError::MissingSpanTag),
        };

        let on_mouse_over_attr = span_tag
          .value()
          .attr("onmouseover")
          .ok_or(ScraperError::MissingAttributeOnMouseOver)?;

        let name = match on_mouse_over_attr.to_string().split("</h1>").next() {
          Some(e) => e.to_string(),
          None => return Err(ScraperError::CannotSplitText(on_mouse_over_attr.to_string())),
        };

        Ok(match name.split("<h1>").last() {
          Some(e) => ScraperLocale::get(server)
            .skill_names
            .get(e)
            .cloned()
            .ok_or_else(|| ScraperError::InvalidSkillName(e.to_string()))?,
          None => return Err(ScraperError::CannotSplitText(name.to_string())),
        })
      })
      .collect();

    skills?
  };

  let profile = DinorpgProfile {
    user: ShortDinorpgUser {
      server,
      id,
      display_name: "todo".parse().unwrap(),
    },
  };

  Ok(DinorpgDinozSkillsResponse {
    profile,
    dinoz_id,
    skills,
  })
}

fn get_dinoz_number(data: &ElementRef) -> Result<String, ScraperError> {
  let dinoz_number = data
    .select(&Selector::parse("div.swf").unwrap())
    .exactly_one()
    .map_err(|_| ScraperError::NonUniqueBuyButton)?;

  let dinoz_number = dinoz_number.value().attr("id").ok_or(ScraperError::InvalidIdTag)?;

  let dinoz_number = dinoz_number.to_string()[9..].to_string();

  Ok(dinoz_number)
}

pub(crate) fn scrape_demon_shop(doc: &Html) -> Result<DinorpgDemonShopResponse, ScraperError> {
  let root = doc.root_element();

  let ScraperContext { server, id } = scrape_context(root)?;

  let profile = DinorpgProfile {
    user: ShortDinorpgUser {
      server,
      id,
      display_name: "todo".parse().unwrap(),
    },
  };

  let dinoz: Vec<SacrificedDinoz> = {
    let sacrified_dinoz: Result<Vec<SacrificedDinoz>, ScraperError> = doc
      .select(&Selector::parse("div.enclos > div.sheet").unwrap())
      .filter(|data| -> bool {
        let dinoz_number = get_dinoz_number(data);

        let dinoz_number = dinoz_number.unwrap();

        let mut skills_div = doc
          .select(&Selector::parse("div.enclos > div.skills").unwrap())
          .collect_vec();

        skills_div.retain(|el| {
          el.value()
            .attr("id")
            .eq(&Some(&format!("{}{}", "details_", dinoz_number)))
        });

        skills_div.get(0).is_some()
      })
      .map(|data| -> Result<SacrificedDinoz, ScraperError> {
        let race_tag = data
          .select(&Selector::parse("div.race").unwrap())
          .exactly_one()
          .map_err(|_| ScraperError::NonUniqueSacrificedDinozRace)?;

        let name = race_tag.text().next().ok_or(ScraperError::NonUniqueSacrificedDinozName);

        let name = match name.ok() {
          Some(e) => e.trim(),
          None => return Err(ScraperError::MissingSacrificedDinozName),
        };

        let name = DinorpgDinozName::from_str(&name[0..name.len() - 1])
          .map_err(|_| ScraperError::InvalidSacrificedDinozName(name.to_string()))?;

        let level_tag = race_tag
          .select(&Selector::parse("strong").unwrap())
          .exactly_one()
          .map_err(|_| ScraperError::NonUniqueStrongLevelTag)?;

        let level = level_tag
          .get_one_text()
          .map_err(|_| ScraperError::NonUniqueSacrificedDinozLevel)?;

        let level = match level.split(' ').last() {
          Some(e) => e.to_string(),
          None => return Err(ScraperError::CannotSplitText(level.to_string())),
        };

        let level: u8 = level.as_str().parse().expect("Sacrified dinoz level must be u8");

        let price_tag = data
          .select(&Selector::parse("div.infos > a").unwrap())
          .exactly_one()
          .map_err(|_| ScraperError::NonUniqueBuyButton)?;

        let attr_href = price_tag.value().attr("href").ok_or(ScraperError::MissingHrefTag)?;

        let chk = match attr_href.split("chk=").last() {
          Some(e) => e.to_string(),
          None => return Err(ScraperError::CannotSplitText(attr_href.to_string())),
        };

        let skin_check = DinorpgDinozSkinCheck::from_str(chk.split(";sk=").next().unwrap())
          .map_err(|_| ScraperError::CannotSplitText(chk))?;

        let dinoz_number = get_dinoz_number(&data);

        let dinoz_number = dinoz_number.unwrap();

        let mut skills_div = doc
          .select(&Selector::parse("div.enclos > div.skills").unwrap())
          .collect_vec();

        skills_div.retain(|el| {
          el.value()
            .attr("id")
            .eq(&Some(&format!("{}{}", "details_", dinoz_number)))
        });

        let elements: DinorpgDinozElements = {
          let elements: Result<Vec<u8>, ScraperError> = skills_div
            .get(0)
            .unwrap()
            .select(&Selector::parse("ul.elements > li").unwrap())
            .map(|element| -> Result<u8, ScraperError> {
              let element = element.text().last().ok_or(ScraperError::InvalidElementText);

              let element = element.unwrap().trim();

              let element: u8 = element
                .parse()
                .map_err(|_| ScraperError::InvalidDinozElement(element.to_string()))?;

              Ok(element)
            })
            .collect();

          let elements = elements?;

          DinorpgDinozElements {
            fire: elements[0],
            wood: elements[1],
            water: elements[2],
            thunder: elements[3],
            air: elements[4],
          }
        };

        let skills: Vec<DinorpgSkill> = {
          let skills: Result<Vec<DinorpgSkill>, ScraperError> = skills_div
            .get(0)
            .unwrap()
            .select(&Selector::parse("div.skills table tbody > tr").unwrap())
            .skip(1)
            .map(|skill| -> Result<DinorpgSkill, ScraperError> {
              let img_tag = skill.select(&Selector::parse(":scope td span").unwrap()).collect_vec();

              let span_tag = match img_tag.first() {
                Some(e) => *e,
                None => return Err(ScraperError::MissingSpanTag),
              };

              let on_mouse_over_attr = span_tag
                .value()
                .attr("onmouseover")
                .ok_or(ScraperError::MissingAttributeOnMouseOver)?;

              let name = match on_mouse_over_attr.to_string().split("</h1>").next() {
                Some(e) => e.to_string(),
                None => return Err(ScraperError::CannotSplitText(on_mouse_over_attr.to_string())),
              };

              Ok(match name.split("<h1>").last() {
                Some(e) => ScraperLocale::get(server)
                  .skill_names
                  .get(e)
                  .cloned()
                  .ok_or_else(|| ScraperError::InvalidSkillName(e.to_string()))?,
                None => return Err(ScraperError::CannotSplitText(name.to_string())),
              })
            })
            .collect();

          skills?
        };

        Ok(SacrificedDinoz {
          name,
          level,
          skin_check,
          elements,
          skills,
        })
      })
      .collect();

    sacrified_dinoz?
  };

  let dinoz = &dinoz[..dinoz.len() - 6];

  let dinoz = dinoz.to_vec();

  Ok(DinorpgDemonShopResponse { profile, dinoz })
}

pub(crate) fn scrape_missions(doc: &Html) -> Result<DinorpgDinozMissionsResponse, ScraperError> {
  let root = doc.root_element();

  let ScraperContext { server, id } = scrape_context(root)?;

  let profile = DinorpgProfile {
    user: ShortDinorpgUser {
      server,
      id,
      display_name: "todo".parse().unwrap(),
    },
  };

  let dinoz: Option<Vec<DinorpgDinozMission>> = {
    let dinoz: Result<Option<Vec<DinorpgDinozMission>>, ScraperError> = doc
      .select(&Selector::parse("div.overview table tbody > tr").unwrap())
      .skip(1)
      .map(|mission| -> Result<Option<DinorpgDinozMission>, ScraperError> {
        let doesnt_have_pmi = mission
          .select(&Selector::parse("div.overview").unwrap())
          .next()
          .is_some();

        if doesnt_have_pmi {
          return Ok(None);
        }

        let span_tag = mission
          .select(&Selector::parse("td span").unwrap())
          .exactly_one()
          .map_err(|_| ScraperError::MissingSpanTag)?;

        let dinoz_id = span_tag.value().attr("id").ok_or(ScraperError::MissingDinozId);

        let dinoz_id = &dinoz_id.unwrap()[16..];

        let id = DinorpgDinozId::from_str(dinoz_id).map_err(|_| ScraperError::InvalidDinozId(dinoz_id.to_string()))?;

        let missions: HashMap<DinorpgNpc, Vec<DinorpgMission>> = {
          let missions: Result<HashMap<DinorpgNpc, Vec<DinorpgMission>>, ScraperError> = mission
            .select(&Selector::parse("td ul > li").unwrap())
            .filter(|charac| charac.value().attr("onmouseover").is_some())
            .map(|character| -> Result<(DinorpgNpc, Vec<DinorpgMission>), ScraperError> {
              let character_name = character
                .get_one_text()
                .map_err(|_| ScraperError::MissingCharacterName)?;

              let character_name = character_name.trim().split_once(' ').unwrap().1;

              let character_name = ScraperLocale::get(server)
                .npc_names
                .get(character_name)
                .cloned()
                .ok_or_else(|| ScraperError::InvalidCharacterName(character_name.to_string()))?;

              let name = character
                .value()
                .attr("onmouseover")
                .ok_or(ScraperError::MissingAttributeOnMouseOver)?;

              let name = name.to_string().trim().to_string();

              let name_list = name.split("<li>").collect_vec();

              let mut mission_list: Vec<DinorpgMission> = Vec::new();

              for item in name_list {
                if !item.contains("<h1>") {
                  let mission_name = &item
                    .split("<p>")
                    .next()
                    .unwrap()
                    .to_string()
                    .trim()
                    .replace('/', " ")
                    .replace('\t', "")
                    .replace("\\r", "")
                    .replace("\\n", "")
                    .replace('\\', "");
                  mission_list.push(
                    ScraperLocale::get(server)
                      .mission
                      .get(mission_name as &str)
                      .cloned()
                      .ok_or_else(|| ScraperError::InvalidMissionName(mission_name.to_string()))?,
                  );
                }
              }

              Ok((character_name, mission_list))
            })
            .collect();

          missions?
        };

        Ok(Some(DinorpgDinozMission { id, missions }))
      })
      .collect();

    dinoz?
  };

  // println!("{:?}", dinoz_skill_div);

  Ok(DinorpgDinozMissionsResponse { profile, dinoz })
}

pub(crate) fn scrape_ingredients(doc: &Html) -> Result<DinorpgIngredientResponse, ScraperError> {
  let root = doc.root_element();

  let ScraperContext { server, id } = scrape_context(root)?;

  let profile = DinorpgProfile {
    user: ShortDinorpgUser {
      server,
      id,
      display_name: "todo".parse().unwrap(),
    },
  };

  let ingredients: HashMap<DinorpgIngredient, DinorpgIngredientCount> = {
    let ingredients: Result<HashMap<DinorpgIngredient, DinorpgIngredientCount>, ScraperError> = doc
      .select(&Selector::parse("form table tbody > tr").unwrap())
      .filter(|ingredient| ingredient.value().attr("onmouseover").is_some())
      .map(
        |ingredient| -> Result<(DinorpgIngredient, DinorpgIngredientCount), ScraperError> {
          let ingredient_name_vec = ingredient.select(&Selector::parse("td").unwrap()).skip(1).collect_vec();

          let ingredient_name = match ingredient_name_vec.first() {
            Some(e) => *e,
            None => return Err(ScraperError::MissingIngredientName),
          };

          let ingredient_name = ingredient_name
            .get_one_text()
            .map_err(|_| ScraperError::InvalidIngredientNameText)?;

          let ingredient_name = ScraperLocale::get(server)
            .ingredient_name
            .get(ingredient_name)
            .cloned()
            .ok_or_else(|| ScraperError::InvalidIngredientName(ingredient_name.to_string()))?;

          let ingredient_quantity_vec = ingredient.select(&Selector::parse("td").unwrap()).skip(2).collect_vec();

          let quantity = match ingredient_quantity_vec.first() {
            Some(e) => *e,
            None => return Err(ScraperError::MissingIngredientQuantity),
          };

          let quantity = quantity
            .get_one_text()
            .map_err(|_| ScraperError::InvalidIngredientQuantityText)?;

          let quantity = quantity.split('/').next().unwrap().trim();

          let quantity = if quantity == "--" {
            DinorpgIngredientCount::new(0).expect("0 is a valid ingredient count")
          } else {
            DinorpgIngredientCount::new(
              quantity
                .parse()
                .map_err(|_| ScraperError::InvalidQuantity(quantity.to_string()))?,
            )
            .map_err(|_| ScraperError::InvalidQuantity(quantity.to_string()))?
          };

          Ok((ingredient_name, quantity))
        },
      )
      .collect();

    ingredients?
  };

  Ok(DinorpgIngredientResponse { profile, ingredients })
}

#[cfg(test)]
mod test {
  use crate::http::scraper::scrape_demon_shop;
  use crate::http::scraper::scrape_ingredients;
  use crate::http::scraper::scrape_missions;
  use crate::http::scraper::scrape_skills;
  use etwin_core::dinorpg::DinorpgDemonShopResponse;
  use etwin_core::dinorpg::DinorpgDinozMissionsResponse;
  use etwin_core::dinorpg::DinorpgDinozSkillsResponse;
  use etwin_core::dinorpg::DinorpgIngredientResponse;
  use scraper::Html;
  use std::path::{Path, PathBuf};
  use test_generator::test_resources;

  #[test_resources("./test-resources/scraping/dinorpg/dinoz/*/")]
  fn test_scrape_dinoz_skills(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let html_path = path.join("input.html");
    let actual_path = path.join("rs.actual.json");

    let raw_html = ::std::fs::read_to_string(html_path).expect("Failed to read html file");

    let html = Html::parse_document(&raw_html);

    let actual = scrape_skills(&html).unwrap();
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected = serde_json::from_str::<DinorpgDinozSkillsResponse>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }

  #[test_resources("./test-resources/scraping/dinorpg/demon_shop/*/")]
  fn test_scrape_sacrificed_dinoz(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let html_path = path.join("input.html");
    let actual_path = path.join("rs.actual.json");

    let raw_html = ::std::fs::read_to_string(html_path).expect("Failed to read html file");

    let html = Html::parse_document(&raw_html);

    let actual = scrape_demon_shop(&html).unwrap();
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected = serde_json::from_str::<DinorpgDemonShopResponse>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }

  #[test_resources("./test-resources/scraping/dinorpg/missions/*/")]
  fn test_scrape_missions(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let html_path = path.join("input.html");
    let actual_path = path.join("rs.actual.json");

    let raw_html = ::std::fs::read_to_string(html_path).expect("Failed to read html file");

    let html = Html::parse_document(&raw_html);

    let actual = scrape_missions(&html).unwrap();
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected =
      serde_json::from_str::<DinorpgDinozMissionsResponse>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }

  #[test_resources("./test-resources/scraping/dinorpg/ingredients/*/")]
  fn test_scrape_ingredients(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let html_path = path.join("input.html");
    let actual_path = path.join("rs.actual.json");

    let raw_html = ::std::fs::read_to_string(html_path).expect("Failed to read html file");

    let html = Html::parse_document(&raw_html);

    let actual = scrape_ingredients(&html).unwrap();
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected = serde_json::from_str::<DinorpgIngredientResponse>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }
}
