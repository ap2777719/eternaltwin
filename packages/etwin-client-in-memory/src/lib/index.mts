import { AuthContext } from "@eternaltwin/core/auth/auth-context";
import { EtwinClientService } from "@eternaltwin/core/etwin-client/service";
import { RfcOauthAccessTokenKey } from "@eternaltwin/core/oauth/rfc-oauth-access-token-key";
import { ShortUser } from "@eternaltwin/core/user/short-user";
import { UserId } from "@eternaltwin/core/user/user-id";

export class InMemoryEtwinClient implements EtwinClientService {
  constructor() {
  }

  public async getAuthSelf(_accessToken: RfcOauthAccessTokenKey): Promise<AuthContext> {
    throw new Error("NotImplemented");
  }

  public async getUserById(_accessToken: RfcOauthAccessTokenKey | null, _userId: UserId): Promise<ShortUser> {
    throw new Error("NotImplemented");
  }
}
