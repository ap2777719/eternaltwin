# In-memory Eternaltwin API client

In-memory eternaltwin client. This implementation enables to use the application in a development environment without having to configure a database.
