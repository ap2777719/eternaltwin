import assert from "node:assert";
import test from "node:test";

import { getExecutableUri } from "../lib/index.mjs";

test("`executableUri` if a `file://` URL", async () => {
  const actual = await getExecutableUri();
  assert.ok(actual instanceof URL, "`executableUri` is an instance of `URL`");
  assert.strictEqual(actual.protocol, "file:");
});
