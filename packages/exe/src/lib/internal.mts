import { arch, platform } from "process";

/**
 * If a precompiled executable is available for the current platform, return its Rust target
 */
export function getRustTarget(): string | null {
  switch (`${arch}.${platform}`) {
    case "arm.linux":
      return "armv7-unknown-linux-gnueabihf";
    // case "x64.darwin":
    //   return "x86_64-apple-darwin";
    case "x64.linux":
      return "x86_64-unknown-linux-gnu";
    case "x64.win32":
      return "x86_64-pc-windows-msvc";
    default:
      return null;
  }
}
