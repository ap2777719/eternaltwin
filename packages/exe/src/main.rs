use clap::Parser;
use etwin_cli::{run, CliArgs};
use std::error::Error;
use std::process::exit;

#[tokio::main]
async fn main() {
  let args: CliArgs = CliArgs::parse();

  let res = run(&args).await;

  if let Err(e) = res {
    eprintln!("ERROR: {}", &e);
    let mut source: Option<&dyn Error> = e.source();
    for _ in 0..100 {
      match source {
        Some(src) => {
          eprintln!("-> {}", src);
          source = src.source();
        }
        None => break,
      }
    }
    exit(1)
  }
}
