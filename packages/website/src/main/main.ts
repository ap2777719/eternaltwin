import { Config, getLocalConfig } from "@eternaltwin/local-config";

import { withApi } from "./api.js";
import { main } from "./index.js";

async function realMain(): Promise<void> {
  const config: Config = await getLocalConfig();

  return withApi(config, (internalAuthKey: Uint8Array): Promise<never> => {
    // Create a never-resolving promise so the API is never closed
    return new Promise<never>(() => {
      main(internalAuthKey);
    });
  });
}

realMain()
  .catch((err: Error): never => {
    console.error(err.stack);
    process.exit(1);
  });
