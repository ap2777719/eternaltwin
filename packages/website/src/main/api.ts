import { Config } from "@eternaltwin/local-config";

async function createApi(config: Config): Promise<{ internalAuthKey: Uint8Array; teardown(): Promise<void> }> {
  const internalAuthKeyStr: string = config.etwin.secret;
  const internalAuthKey: Uint8Array = Buffer.from(internalAuthKeyStr);

  return {
    internalAuthKey,
    async teardown(): Promise<void> {},
  };
}

/**
 * Async resource manager for the Eternalfest API backend.
 *
 * @param config Server config
 * @param fn Inner function to call with an API pool.
 */
export async function withApi<R>(config: Readonly<Config>, fn: (internalAuthKey: Uint8Array) => Promise<R>): Promise<R> {
  const {internalAuthKey, teardown} = await createApi(config);
  try {
    return await fn(internalAuthKey);
  } finally {
    await teardown();
  }
}
