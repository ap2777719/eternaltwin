import { Injectable } from "@angular/core";
import { AuthContext } from "@eternaltwin/core/auth/auth-context";
import { RawUserCredentials } from "@eternaltwin/core/auth/raw-user-credentials";
import { RegisterWithUsernameOptions } from "@eternaltwin/core/auth/register-with-username-options";
import { DinoparcCredentials } from "@eternaltwin/core/dinoparc/dinoparc-credentials";
import { HammerfestCredentials } from "@eternaltwin/core/hammerfest/hammerfest-credentials";
import { User } from "@eternaltwin/core/user/user";
import { Observable } from "rxjs";

@Injectable()
export abstract class AuthService {
  abstract auth(): Observable<AuthContext>;

  abstract registerWithUsername(options: Readonly<RegisterWithUsernameOptions>): Observable<User>;

  abstract loginWithCredentials(options: Readonly<RawUserCredentials>): Observable<User>;

  abstract loginWithDinoparcCredentials(credentials: Readonly<DinoparcCredentials>): Observable<User>;

  abstract loginWithHammerfestCredentials(credentials: Readonly<HammerfestCredentials>): Observable<User>;

  abstract logout(): Observable<null>;
}
