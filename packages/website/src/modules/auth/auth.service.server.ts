import { Inject, Injectable } from "@angular/core";
import { $AuthContext, AuthContext } from "@eternaltwin/core/auth/auth-context";
import { GUEST_AUTH } from "@eternaltwin/core/auth/guest-auth-context";
import { RegisterWithUsernameOptions } from "@eternaltwin/core/auth/register-with-username-options";
import { UserCredentials } from "@eternaltwin/core/auth/user-credentials";
import { DinoparcCredentials } from "@eternaltwin/core/dinoparc/dinoparc-credentials";
import { HammerfestCredentials } from "@eternaltwin/core/hammerfest/hammerfest-credentials";
import { User } from "@eternaltwin/core/user/user";
import { concat as rxConcat, NEVER, Observable, of as rxOf, ReplaySubject } from "rxjs";
import type { Request } from "koa";

import { BACKEND_URI, REQUEST } from "../../server/tokens";
import { AuthService } from "./auth.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AUTH_CONTEXT_KEY } from "./state-keys";
import { NEVER as RX_NEVER } from "rxjs/internal/observable/never";
import urljoin from "url-join";
import { map as rxMap } from "rxjs/operators";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { TransferState } from "@angular/platform-browser";

@Injectable()
export class ServerAuthService extends AuthService {
  // private readonly auth$: Observable<AuthContext>;
  readonly #auth$: ReplaySubject<AuthContext>;
  readonly #backendUri: string;
  readonly #request: Request;
  readonly #transferState: TransferState;

  constructor(
    @Inject(BACKEND_URI) backendUri: string,
    @Inject(REQUEST) request: Request,
    httpClient: HttpClient,
    transferState: TransferState,
  ) {
    super();
    this.#backendUri = backendUri;
    this.#request = request;
    this.#transferState = transferState;

    const headers: Record<string, string | string[]> = {};
    for (const [key, value] of Object.entries(request.headers)) {
      if (value !== undefined && key === "cookie") {
        headers[key] = value;
      }
    }

    const firstAuth: Observable<AuthContext> = httpClient.request(
      "GET",
      urljoin(backendUri, "api/v1/auth/self"),
      {
        headers: new HttpHeaders(headers),
        responseType: "json",
      }
    ).pipe(rxMap((raw): AuthContext => {
      let acx: AuthContext;
      try {
        acx = $AuthContext.read(JSON_VALUE_READER, raw);
        this.#transferState.set<unknown>(AUTH_CONTEXT_KEY, raw);
      } catch (err) {
        console.error("AuthError:");
        console.error(err);
        acx = GUEST_AUTH;
      }
      return acx;
    }));

    // Prevent the `complete` event.
    const infFirstAuth: Observable<AuthContext> = rxConcat(firstAuth, RX_NEVER);

    this.#auth$ = new ReplaySubject(1);
    infFirstAuth.subscribe(this.#auth$);
  }

  auth(): Observable<AuthContext> {
    return this.#auth$;
  }

  logout(): Observable<null> {
    throw new Error("NotImplemented");
  }

  registerWithUsername(options: Readonly<RegisterWithUsernameOptions>): Observable<User> {
    throw new Error("NotImplemented");
  }

  loginWithCredentials(options: Readonly<UserCredentials>): Observable<User> {
    throw new Error("NotImplemented");
  }

  loginWithDinoparcCredentials(credentials: Readonly<DinoparcCredentials>): Observable<User> {
    throw new Error("NotImplemented");
  }

  loginWithHammerfestCredentials(credentials: Readonly<HammerfestCredentials>): Observable<User> {
    throw new Error("NotImplemented");
  }
}
