import { NgModule } from "@angular/core";

// import { RestModule } from "../rest/rest.module";
import { ForumService } from "./forum.service";

@NgModule({
  imports: [
    // RestModule,
  ],
  providers: [
    ForumService,
  ],
})
export class ForumModule {
}
