import {Injectable} from "@angular/core";
import {ObjectType} from "@eternaltwin/core/core/object-type";
import {$DinoparcUserIdRef} from "@eternaltwin/core/dinoparc/dinoparc-user-id-ref";
import {$HammerfestUserIdRef} from "@eternaltwin/core/hammerfest/hammerfest-user-id-ref";
import {$TwinoidUserIdRef} from "@eternaltwin/core/twinoid/twinoid-user-id-ref";
import {$MaybeCompleteUser, MaybeCompleteUser} from "@eternaltwin/core/user/maybe-complete-user";
import {UnlinkFromDinoparcOptions} from "@eternaltwin/core/user/unlink-from-dinoparc-options";
import {UnlinkFromHammerfestOptions} from "@eternaltwin/core/user/unlink-from-hammerfest-options";
import {UnlinkFromTwinoidOptions} from "@eternaltwin/core/user/unlink-from-twinoid-options";
import {$UpdateUserPatch, UpdateUserPatch} from "@eternaltwin/core/user/update-user-patch";
import {$User, User} from "@eternaltwin/core/user/user";
import {UserId} from "@eternaltwin/core/user/user-id";
import {$UserIdRef} from "@eternaltwin/core/user/user-id-ref";
import {$Any} from "kryo/any";
import {Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError} from "rxjs/internal/operators/catchError";
import {map as rxMap} from "rxjs/operators";

import {RestService} from "../rest/rest.service";

@Injectable()
export class UserService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getUserById(userId: UserId): Observable<MaybeCompleteUser | null> {
    return this.#rest
      .get(["users", userId], {resType: $MaybeCompleteUser})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }

  updateUser(userId: UserId, patch: Readonly<UpdateUserPatch>): Observable<User> {
    return this.#rest.patch(["users", userId], {
      reqType: $UpdateUserPatch,
      req: patch,
      resType: $User,
    });
  }

  unlinkFromDinoparc(options: Readonly<UnlinkFromDinoparcOptions>): Observable<null> {
    return this.#rest.delete(["users", options.userId, "links", options.dinoparcServer], {
      reqType: $DinoparcUserIdRef,
      req: {
        type: ObjectType.DinoparcUser,
        server: options.dinoparcServer,
        id: options.dinoparcUserId,
      },
      resType: $Any,
    }).pipe(rxMap(() => null));
  }

  unlinkFromHammerfest(options: Readonly<UnlinkFromHammerfestOptions>): Observable<null> {
    return this.#rest.delete(["users", options.userId, "links", options.hammerfestServer], {
      reqType: $HammerfestUserIdRef,
      req: {
        type: ObjectType.HammerfestUser,
        server: options.hammerfestServer,
        id: options.hammerfestUserId,
      },
      resType: $Any,
    }).pipe(rxMap(() => null));
  }

  unlinkFromTwinoid(options: Readonly<UnlinkFromTwinoidOptions>): Observable<null> {
    return this.#rest.delete(["users", options.userId, "links", "twinoid.com"], {
      reqType: $TwinoidUserIdRef,
      req: {
        type: ObjectType.TwinoidUser,
        id: options.twinoidUserId,
      },
      resType: $Any,
    }).pipe(rxMap(() => null));
  }

  deleteUser(userId: UserId): Observable<null> {
    return this.#rest.delete(["users", userId], {
      reqType: $UserIdRef,
      req: {
        type: ObjectType.User,
        id: userId,
      },
      resType: $Any,
    }).pipe(rxMap(() => null));
  }
}
