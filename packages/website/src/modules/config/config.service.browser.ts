import { Injectable } from "@angular/core";
import { TransferState } from "@angular/platform-browser";
import { $Config, Config } from "@eternaltwin/core/config/config";
import { ForumConfig } from "@eternaltwin/core/forum/forum-config";
import { JSON_READER } from "kryo-json/json-reader";
import { JSON_WRITER } from "kryo-json/json-writer";

import { ConfigService } from "./config.service";
import { CONFIG_KEY } from "./state-keys";

@Injectable({providedIn: "root"})
export class BrowserConfigService extends ConfigService {
  readonly #config: Config;

  constructor(transferState: TransferState) {
    super();
    const defaultConfig = {forum: {threadsPerPage: 20, postsPerPage: 10}};
    const rawConfig: unknown = transferState.get(CONFIG_KEY, $Config.write(JSON_WRITER, defaultConfig));
    this.#config = $Config.read(JSON_READER, rawConfig);
  }

  forum(): ForumConfig {
    return this.#config.forum;
  }
}
