import { NgModule } from "@angular/core";

import { DinoparcService } from "./dinoparc.service";

@NgModule({
  providers: [
    DinoparcService,
  ],
  imports: [
  ],
})
export class DinoparcModule {
}
