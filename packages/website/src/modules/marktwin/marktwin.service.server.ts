import { Injectable } from "@angular/core";
import { Grammar } from "@eternal-twin/marktwin/grammar";

import { MarktwinService } from "./marktwin.service";

@Injectable()
export class ServerMarktwinService extends MarktwinService {
  public constructor() {
    super();
  }

  public renderMarktwin(grammar: Readonly<Grammar>, input: string): string {
    return "marktwin rendering is not supported by server-side rendering";
  }
}
