import { Injectable } from "@angular/core";
import * as marktwin from "@eternal-twin/marktwin";
import { Grammar } from "@eternal-twin/marktwin/grammar";

import { MarktwinService } from "./marktwin.service";

@Injectable()
export class BrowserMarktwinService extends MarktwinService {
  public constructor() {
    super();
  }

  public renderMarktwin(grammar: Readonly<Grammar>, input: string): string {
    return marktwin.renderMarktwin(grammar, input);
  }
}
