import { Injectable } from "@angular/core";
import { $TimeQuery } from "@eternaltwin/core/core/time-query";
import { GetHammerfestUserOptions } from "@eternaltwin/core/hammerfest/get-hammerfest-user-options";
import { $HammerfestUser, HammerfestUser } from "@eternaltwin/core/hammerfest/hammerfest-user";
import { Observable, of as rxOf } from "rxjs";
import { catchError as rxCatchError } from "rxjs/operators";

import { RestService } from "../rest/rest.service";

@Injectable()
export class HammerfestService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getUser(options: Readonly<GetHammerfestUserOptions>): Observable<HammerfestUser | null> {
    return this.#rest
      .get(
        ["archive", "hammerfest", options.server, "users", options.id],
        {
          queryType: $TimeQuery,
          query: {time: options.time},
          resType: $HammerfestUser,
        },
      )
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }
}
