import { NgModule } from "@angular/core";

import { HammerfestService } from "./hammerfest.service";

@NgModule({
  providers: [
    HammerfestService
  ],
  imports: [ ],
})
export class HammerfestModule {
}
