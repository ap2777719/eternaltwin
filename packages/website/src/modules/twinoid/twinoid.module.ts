import { NgModule } from "@angular/core";

import { TwinoidService } from "./twinoid.service";

@NgModule({
  providers: [
    TwinoidService,
  ],
  imports: [],
})
export class TwinoidModule {
}
