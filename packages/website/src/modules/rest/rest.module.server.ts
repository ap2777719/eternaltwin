import { NgModule } from "@angular/core";

import { RestService } from "./rest.service";
import { ServerRestService } from "./rest.service.server";

@NgModule({
  imports: [],
  providers: [
    {provide: RestService, useClass: ServerRestService},
  ],
})
export class ServerRestModule {
}
