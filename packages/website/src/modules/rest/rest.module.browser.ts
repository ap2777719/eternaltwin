import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";

import { RestService } from "./rest.service";
import { BrowserRestService } from "./rest.service.browser";

@NgModule({
  imports: [
    HttpClientModule,
  ],
  providers: [
    {provide: RestService, useClass: BrowserRestService},
  ],
})
export class BrowserRestModule {
}
