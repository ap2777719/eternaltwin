import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {AuthType} from "@eternaltwin/core/auth/auth-type";
import {ObjectType} from "@eternaltwin/core/core/object-type";
import {MaybeCompleteUser} from "@eternaltwin/core/user/maybe-complete-user";
import {NEVER as RX_NEVER, Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError} from "rxjs/internal/operators/catchError";
import {map as rxMap, startWith as rxStartWith} from "rxjs/operators";

import {AuthService} from "../../modules/auth/auth.service";

const USER_NOT_FOUND: unique symbol = Symbol("USER_NOT_FOUND");

@Component({
  selector: "etwin-user-view",
  templateUrl: "./user-view.component.html",
  styleUrls: [],
})
export class UserViewComponent implements OnInit {
  private readonly route: ActivatedRoute;

  public user$: Observable<MaybeCompleteUser | typeof USER_NOT_FOUND>;
  public readonly ObjectType = ObjectType;
  public readonly USER_NOT_FOUND = USER_NOT_FOUND;
  public isAdministrator$: Observable<boolean>;

  constructor(
    auth: AuthService,
    route: ActivatedRoute,
  ) {
    this.route = route;
    this.user$ = RX_NEVER;
    this.isAdministrator$ = auth.auth().pipe(
      rxMap((acx) => acx.type === AuthType.User && acx.isAdministrator),
      rxStartWith(false),
      rxCatchError(() => rxOf(false)),
    );
  }

  ngOnInit(): void {
    interface RouteData {
      user: MaybeCompleteUser | null;
    }

    const routeData$: Observable<RouteData> = this.route.data as any;
    this.user$ = routeData$.pipe(rxMap(({user}: RouteData) => user !== null ? user : USER_NOT_FOUND));
  }
}
