import { NgModule } from "@angular/core";
import { ServerModule, ServerTransferStateModule } from "@angular/platform-server";

import { ServerAuthModule } from "../modules/auth/auth.module.server";
import { ServerConfigModule } from "../modules/config/config.module.server";
import { DinoparcModule } from "../modules/dinoparc/dinoparc.module";
import { ForumModule } from "../modules/forum/forum.module";
import { HammerfestModule } from "../modules/hammerfest/hammerfest.module";
import { ServerRestModule } from "../modules/rest/rest.module.server";
import { TwinoidModule } from "../modules/twinoid/twinoid.module";
import { UserModule } from "../modules/user/user.module";
import { AppComponent } from "./app.component";
import { AppModule } from "./app.module";
import { XhrFactory } from "@angular/common";
import xhr2 from "xhr2";

/**
 * Workaround for https://github.com/angular/angular/issues/15730
 *
 * (allow sending cookies)
 */
class ServerXhr implements XhrFactory {
  build(): XMLHttpRequest {
    const xhr = new xhr2.XMLHttpRequest();
    xhr._restrictedHeaders.cookie = false;
    return xhr;
  }
}

@NgModule({
  imports: [
    AppModule,
    ServerAuthModule,
    ServerConfigModule,
    DinoparcModule,
    ForumModule,
    HammerfestModule,
    ServerModule,
    ServerRestModule,
    ServerTransferStateModule,
    TwinoidModule,
    UserModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: XhrFactory, useClass: ServerXhr, deps: []},
  ],
})
export class AppServerModule {
}
