import { APP_BASE_HREF } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserTransferStateModule } from "@angular/platform-browser";

import { BrowserAuthModule } from "../modules/auth/auth.module.browser";
import { BrowserConfigModule } from "../modules/config/config.module.browser";
import { DinoparcModule } from "../modules/dinoparc/dinoparc.module";
import { ForumModule } from "../modules/forum/forum.module";
import { HammerfestModule } from "../modules/hammerfest/hammerfest.module";
import { BrowserRestModule } from "../modules/rest/rest.module.browser";
import { TwinoidModule } from "../modules/twinoid/twinoid.module";
import { UserModule } from "../modules/user/user.module";
import { AppComponent } from "./app.component";
import { AppModule } from "./app.module";

@NgModule({
  imports: [
    AppModule,
    BrowserAuthModule,
    BrowserConfigModule,
    DinoparcModule,
    ForumModule,
    HammerfestModule,
    BrowserTransferStateModule,
    TwinoidModule,
    UserModule,
    BrowserRestModule,
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: "/"},
  ],
  bootstrap: [AppComponent],
})
export class AppBrowserModule {
}
