import "zone.js/dist/zone-node";
import "@angular/localize/init";

import { APP_BASE_HREF } from "@angular/common";
import { StaticProvider } from "@angular/core";
import { Url } from "@eternaltwin/core/core/url";
import { ForumConfig } from "@eternaltwin/core/forum/forum-config";
import Router, { RouterContext } from "@koa/router";
import * as furi from "furi";
import Koa from "koa";
import koaStaticCache from "koa-static-cache";

import { AppServerModule } from "../app/app.server.module";
import { ROUTES } from "../routes";
import { ServerAppConfig } from "./config";
import { NgKoaEngine } from "./ng-koa-engine";
import { BACKEND_URI, CONFIG, INTERNAL_AUTH_KEY, REQUEST } from "./tokens";

function resolveServerOptions(options?: Partial<ServerAppConfig>): ServerAppConfig {
  let isProduction: boolean = false;
  if (options === undefined) {
    options = {};
  } else {
    isProduction = options.isProduction === true;
  }
  const externalUri: Url | undefined = options.externalUri;
  const isIndexNextToServerMain: boolean = options.isIndexNextToServerMain === true;
  if (isProduction) {
    if (externalUri === undefined) {
      throw new Error("Aborting: Missing server option `externalBaseUri` in production mode");
    }
    if (!isIndexNextToServerMain) {
      throw new Error("Aborting: Index.html must be located next to server's main in production mode");
    }
  }
  if (options.internalAuthKey === undefined) {
    throw new Error("Missing `internalAuthKey` configuration");
  }
  const internalAuthKey: Uint8Array = options.internalAuthKey;
  if (options.forum === undefined) {
    throw new Error("Missing `forum` configuration");
  }
  const forum: ForumConfig = options.forum;
  const backendPort: undefined | number = options.backendPort;
  if (typeof backendPort !== "number") {
    throw new Error("Missing `backendPort` configuration");
  }
  return {externalUri, isIndexNextToServerMain, isProduction, internalAuthKey, forum, backendPort};
}

/**
 * Resolves the fully qualified URL from the path and query
 */
function fullyQualifyUrl(options: ServerAppConfig, pathAndQuery: string): Url {
  if (options.externalUri !== undefined) {
    return new Url(pathAndQuery, options.externalUri);
  } else {
    return new Url(pathAndQuery, "http://localhost/");
  }
}

// The Express app is exported so that it can be used by serverless Functions.
export async function app(options?: Partial<ServerAppConfig>): Promise<Koa> {
  const config: ServerAppConfig = resolveServerOptions(options);

  const serverDir = furi.fromSysPath(__dirname);
  const indexFuri = config.isIndexNextToServerMain
    ? furi.join(serverDir, "index.html")
    : furi.join(serverDir, "../../browser", furi.basename(serverDir), "index.html");
  const staticFuri = furi.join(serverDir, "../../browser", furi.basename(serverDir));

  const app = new Koa();

  const providers: StaticProvider[] = [];
  if (config.externalUri !== undefined) {
    providers.push({provide: APP_BASE_HREF, useValue: config.externalUri.toString()});
  }
  providers.push({provide: CONFIG, useValue: {forum: config.forum}});

  const engine: NgKoaEngine = await NgKoaEngine.create({
    indexFuri,
    bootstrap: AppServerModule,
    providers,
    staticFuri,
  });

  const router = new Router();
  // TODO: Fix `koajs/router` type definitions to accept a readonly ROUTES.
  router.get([...ROUTES], ngRender);
  app.use(router.routes());
  app.use(router.allowedMethods());

  async function ngRender(cx: RouterContext): Promise<void> {
    const backendUri = new URL("http://localhost/");
    backendUri.port = config.backendPort.toString(10);
    const reqUrl: Url = fullyQualifyUrl(config, cx.request.originalUrl);
    cx.response.body = await engine.render({
      url: reqUrl,
      providers: [
        {provide: REQUEST, useValue: cx.request},
        {provide: BACKEND_URI, useValue: backendUri.toString()},
        {provide: INTERNAL_AUTH_KEY, useValue: Buffer.from(config.internalAuthKey).toString("utf8")},
      ],
    });
  }

  if (!config.isIndexNextToServerMain) {
    const browserDir = furi.join(serverDir, "../../browser", furi.basename(serverDir));
    const ONE_DAY: number = 24 * 3600;
    app.use(koaStaticCache(furi.toSysPath(browserDir), {maxAge: ONE_DAY}));
  }

  return app;
}

async function run() {
  const port = process.env.PORT || 4000;

  // Start up the Node server
  const server = await app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || "";
if (moduleFilename === __filename || moduleFilename.includes("iisnode")) {
  run();
}
