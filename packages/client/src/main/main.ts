import { ClockState } from "@eternaltwin/core/clock/clock-state";

import {EternaltwinClient} from "../lib/index.mjs";
import { RequestType } from "../lib/request-type.mjs";

async function main() {
  const client = new EternaltwinClient(new URL("https://eternal-twin.net/"));
  const clock: ClockState = await client.execute({type: RequestType.GetClock});
  console.log(clock);
}

main();
