import { User } from "@eternaltwin/core/user/user";

import { Request as BaseRequest } from "./index.mjs";
import { RequestType } from "./request-type.mjs";

export const TYPE: RequestType.GetUser = RequestType.GetUser as const;

export interface Request extends BaseRequest {
  readonly type: typeof TYPE;
}

export type Response = User;
