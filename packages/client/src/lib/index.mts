import superagent from "superagent";
import urlJoin from "url-join";

import * as getClock from "./get-clock.mjs";
import * as getUser from "./get-user.mjs";
import { RequestType } from "./request-type.mjs";

export type Agent = superagent.SuperAgent<superagent.SuperAgentRequest>;

export interface Request {
  readonly type: RequestType,
}

export interface RequestToResponse {
  // readonly [RequestType.LoginWithEternaltwin]: LoginWithEternaltwinResponse;
  readonly [getClock.TYPE]: getClock.Response;
  readonly [getUser.TYPE]: getUser.Response;
}

export class EternaltwinClient {
  readonly #agent: Agent;
  readonly #apiBase: URL;

  public constructor(apiBase: URL) {
    this.#agent = superagent.agent();
    this.#apiBase = apiBase;
  }

  public async execute<Req extends Request>(req: Req): Promise<RequestToResponse[Req["type"]]> {
    console.log(req);
    console.log(this.#agent);
    console.log(this.#resolve("foo"));
    return null as any;
    // switch (req.type) {
    //   case RequestType.LoginWithEternaltwin: return this.#loginWithEternaltwin(req);
    //   default: throw new Error("unexpected request type");
    // }
  }

  // async #loginWithEternaltwin(_req: LoginWithEternaltwin): Promise<LoginWithEternaltwinResponse> {
  //   const res = await this.#postForm("/actions/login/etwin");
  //   console.log(res);
  //   return {redirectUri: new URL("https://eternal-twin.net")};
  // }

  // async #postForm(
  //   url: string,
  // ): Promise<URL> {
  //   const rawReq: unknown = {};
  //   try {
  //     const res: superagent.Response = await this.#agent
  //       .post(this.#resolve(url).toString())
  //       .redirects(0)
  //       .send(rawReq as any);
  //     console.debug(res);
  //     throw new Error("expected status 302 (\"Found\"), got success");
  //   } catch (rawErr: unknown) {
  //     if (!(rawErr instanceof Error)) {
  //       throw rawErr;
  //     }
  //     const err: superagent.ResponseError = rawErr;
  //     if (err.status !== 302) {
  //       throw new Error("expected status 302 (\"Found\"), got other error");
  //     }
  //     const redirectUri: string | undefined = err.response?.get("location");
  //     if (redirectUri === undefined) {
  //       throw new Error("missing `location` header");
  //     }
  //     return new URL(redirectUri);
  //   }
  // }
  //
  #resolve(uri: string): URL {
    return new URL(urlJoin(this.#apiBase.toString(), uri));
  }
}
