import { ClockState } from "@eternaltwin/core/clock/clock-state";

import { Request as BaseRequest } from "./index.mjs";
import { RequestType } from "./request-type.mjs";

export const TYPE: RequestType.GetClock = RequestType.GetClock as const;

export interface Request extends BaseRequest {
  readonly type: typeof TYPE;
}

export type Response = ClockState;
