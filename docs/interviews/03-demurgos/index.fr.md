# Demurgos

**Salut Demurgos ! Merci de prendre quelques instants pour répondre à ces questions. Quasiment tout Eternaltwin a déjà entendu parler de toi mais peux tu te présenter ?**

_Coucou 👋_

_Dans la vraie vie je m'appelle Charles et je suis ingénieur informaticien, mais sur Internet je suis plus connu sous le pseudo Demurgos pour mes contributions Open Source et mon travail sur Eternalfest et Eternaltwin. J'habite en France (vers Lyon) et j'aurai 28 ans en septembre. J'aime les puzzle games, faire du vélo et les loutres._

**Tu es un des membres fondateurs de EternalTwin mais également de EternalFest ! Peux tu nous en dire plus sur ce projet qui fête déjà ses 10 ans ?**

_Depuis tout petit j'ai toujours joué sur ordi, mais je n'ai commencé à utiliser internet que vers le milieu des années 2000 quand j'avais une dizaine d'années. C'était alors l'âge d'or des jeux Flash et je m'amusais beaucoup sur Miniclip ou Armor Games. J'ai assez vite découvert Prizee (qui collaborait avec la MT), puis Hammerfest en tant que premier jeu MT. J'ai tout de suite adoré ce jeu de plate-forme où on incarne un bonhomme de neige pour son aspect arcade et ses mécaniques (surprenamment complexes). Hammerfest, grâce à son forum, est aussi la première communauté internet que j'ai rejointe. C'est donc un jeu auquel je suis attaché._

**Du coup Hammerfest tu as commencé à y jouer dès le départ quasiment ? Étais tu une personne qui utilisait beaucoup de code pour avoir des flocons ?**

_Oui, j'ai rejoint Hammerfest au tout début (mon id est "127" sur Hammerfest; aussi j'ai l'id "38" sur Twinoid : c'est mes ids les plus bas donc j'aime bien frimer un peu avec même si c'est pas si important 😛 )._

_En effet, le modèle "free to play" n'était pas encore très mature à l'époque. On avait 5 parties offertes à la création du compte puis rien. Il fallait payer. Au début je fonctionnais donc beaucoup avec le reset du compte. 5 parties, reset, et on recommence. J'ai fini par acheter un code SMS qui donnait accès à quelques parties par semaine. J'ai ensuite mis Hammerfest un peu de côté, et à mon retour j'avais un tas de parties accumulées._

_Derrière j'ai joué à de nombreux jeux Motion-Twin, et était un membre actif de Muxxu puis Twinoid. En 2012, le streaming devenait populaire. Hammerfest étant le jeu MT le plus intéressant à regarder en live, il y avait une période où beaucoup de monde streamait ses parties. Après avoir était inactif sur Hammerfest pendant quelques années, cet engouement m'a permis de reprendre contact avec des joueurs d'Hammerfest. Lors d'un stream fin octobre 2012, j'ai rejoint un groupe Skype qui allait changer ma vie : "La cachette des carotteux"._

**La cachette des carotteux, c'est pas le groupe de génies d'hammerfest qu'on peut voir dans le jeu au tout début ?**

_Oui, le nom est basé sur ça, dans le jeu officiel c'est "le repère ces carotteux", mais sur Skype on était "la cachette"._

**Au bout de combien de temps es tu devenu détenteur de la carotte, Saint Graal des hammerfestiens ?**

_J'étais plutôt mauvais. 😅 J'ai eu ma carotte assez tard. Il faudrait que je retrouve la date mais il est possible que ça soit fin 2012 après avoir rejoint le groupe Skype (et grâce à leur conseils). Il m'a ensuite encore fallu 2 ou 3 ans pour avoir la neige-o-glycérine (quête finale du jeu)._

**Je te laisse continuer l'historique.**

_J'étais aussi Augure Hammerfest (c'était le système d'interlocuteur avec les admins MT) et j'ai commencé à coder en 2010, en seconde (HTML/PHP), puis j'ai fait des extensions de navigateur pour Muxxu/Twinoid ("MotionMax") et fait la carte interactive OdysseyMap (comme Fevermap mais pour Odyssey)._

_Hammerfest avait une communauté active de créateurs de niveaux. Il n'y avait pas d'éditeur de niveau, mais les gens utilisaient Paint pour dessiner leurs niveaux puis les partageaient sur leurs blogs. En 2010 environ, un projet a fait pas mal de bruit sur Hammerfest : le joueur gary380 a créé le "Temple Oublié", une version hackée d'Hammerfest avec des niveaux inédits. La MT a interdit la diffusion de ce hack._

_Vers la fin de l'année, le joueur GlobZOsiris (un joueur très connu, il est le premier a avoir complété la quête finale du jeu) a rejoint le groupe Skype. Il avait accès au Temple Oublié et nous a expliqué comment y jouer et faire nos propres niveaux. Je n'était pas enthousiaste car la MT avait interdit d'y jouer, et le fonctionnement permettait de facilement tricher sur le vrai jeu (classements, quêtes). En revanche les autres membres du groupe ont adoré et commencé à créer leurs propres niveaux, se les envoyer, y jouer, etc._

_Arrivé en mars 2013, j'étais la dernière personne à encore refuser d'y jouer. La MT a l'habitude de ne plus retoucher à ses jeux une fois finis, du coup avoir la possibilité d'avoir du nouveau contenu était très tentante. Je me suis donc décidé à coder mon propre système qui permettrait de partager des niveaux et y jouer, mais le partage serait automatique (site web au lieu de s'envoyer des ZIPs manuellement) et ça serait isolé du site officiel (pas de possibilité de s'en servir pour envoyer des résultats truqués sur le site officiel). J'ai donc modifié le loader, codé un système d'hébergement basique et envoyé le tout sur un hébergeur gratuit ; de la grosse bidouille mais ça marchait._

_Et j'ai finalement partagé ce site avec le groupe et posté ce message :_

_**[05/03/2013 21:22:56] Demurgos: Ce sera eternalfest**_

_Après mars 2013, on a beaucoup travaillé sur le côté technique (il y avait beaucoup de défis, mais on va éviter de se perdre dedans). Là où le Temple Oublié a échoué à obtenir l'approbation de la MT, notre but était d'avoir quelque chose de bien plus propre et avoir le droit de partager notre travail._

_Pendant l'été on a créé on a créé un compte Twinoid pour partager les avancées : [Eternalfest](https://twinoid.com/user/7019378)._

_Le jeudi 5 septembre, on a ouvert un sujet sur le forum pour annoncer le projet à la communauté : [Projet Eternalfest](http://www.hammerfest.fr/forum.html/thread/476324/)_

_Vu que la MT avait interdit le Temple Oublié, et que ce qu'on faisait n'est techniquement pas autorisé par le règlement, on avait peur de se faire bannir. On utilisait donc un compte anonyme et parlaient de nous avec des alias. ça faisait un peu "société secrète" (le logo pyramide d'Eternalfest y était aussi pour quelque chose...). C'était marrant 😛._

_On a aussi posté un sujet sur Twinoid ou on présentait le jeu et demander de pouvoir discuter avec les admins. Je me souviens que ça a causé pas mal de soucis aux modos 😄._

_On envoyé un mail assez détaillé à la MT qui décrivait le fonctionnement et comment y jouer. Après quelques relances, on a finalement eu une réponse fin 2013 : "gardez ça pour vous, interdit de diffuser"._

_C'était dommage, mais on a accepté. On a tout de même continuer de créer des niveaux, faire des lives, partager des nouvelles. Pendant les trois premières années on a créé quelques dizaines de contrées, refait le site 3 fois (j'apprenais beaucoup au niveau informatique), mis en place un système pour modder le jeu, etc. Au fil du temps, les membres du groupe sont aussi devenus des amis proches._

_En 2015 ou 2016, il y a eu l'élément déclencheur suivant : il y a eu un gros dysfonctionnement sur le serveur officiel qui l'a rendu inaccessible pendant une semaine. Blobzone, Conquisla ou Pioupiouz ayant déjà fermé, et Hammerfest ayant une dizaine d'années (sortie le 27 février 2006 si je me souviens bien), on craignait que ce soit la fin du jeu. Heureusement le site a fini par être réparé, mais ça nous a poussé à revoir un peu le fonctionnement:_

1. _Il fallait qu'Eternalfest soit 100% indépendant d'Hammerfest (jusque là ça utilisait une extension de navigateur qui ajoutait un menu au site officiel)_
2. _Il fallait mettre en place une solution pour archiver son compte et le forum_

_On est donc passé du système de partage de niveau basique à un site complet ; c'est la version qu'on retrouve encore de nos jours. Au cours de l'été 2017, on a aussi décidé d'ouvrir Eternalfest en beta au public. Il n'y avait toujours pas le droit de jouer (on continue de respecter la décision de la MT), mais on offrait aux joueurs la possibilité de lier leur compte et créer une archive de leur frigo._

_Il y a eu entre 100 et 200 personnes qui ont rejoint cette beta, c'était vraiment super de voir ces premiers utilisateurs 🙂._

_En parallèle de ça il y avait déjà l'histoire de la fin de Flash annoncée depuis longtemps. Mozilla avait un projet appelé Shumway pour continuer de lire les fichier Flash mais sans le plugin. Malheureusement ce projet a été abandonné. J'ai donc décidé de reprendre ce travail avec [Open Flash](https://open-flash.github.io/), une suite d'outil pour lire les fichiers sans avoir besoin du plugin. Je pensais alors développer ça suffisamment pour l'offrir aux projet Open-Source, mais le rendre payant pour les entreprises (ça ne s'est jamais fait au final, mais c'était mon idée d'activité après mes études)._

_J'étais donc moins actif sur Eternalfest et plus concentré sur d'autres projets, mais développer mes propres outils pour manipuler Flash a permis la prochaine fonctionnalité majeure : la possibilité d'injecter des graphismes personnalisés (et ainsi compléter les mods)._

_En 2017 et 2018, les sites de la Motion-Twin étaient de moins en moins actifs. Mais j'ai aussi rejoint une nouvelle plateforme de communication, Discord. J'y ai trouvé un serveur dédié au jeu avec une bonne communauté de nouveau joueurs ça a relancé l'intérêt pour Hammerfest/Eternalfest et posé de nombreux débats. Depuis 2013, on respectait la décision de la MT de ne pas diffuser Eternalfest; mais ça restait dommage. On avait un super projet sur lequel on bossait depuis 5 ans, on en était fiers, mais on n'avait pas le droit de le partager._

_On avait aussi des retours d'admins MT ou ex-MT qui aimaient le projet de manière personnelle; mais pas de réponse officielle. On a essayé de relancer les emails, mais toujours pas eu de réponse... Après maintes discussion, on a finalement pris la décision d'ouvrir Eternalfest au public; et rester dans une zone "grise" où on n'a pas de permission explicite, mais où on espère ne pas avoir d'ennuis. Avec du recul, ça a l'air évident, mais ce n’était pas une décision facile._

_La première contrée publiée a donc été "Eternoël", à Noël 2018. C'était un énorme succès. Vu notre énorme back-catalog, on a commencé le processus de publication de tous nos jeux. Avec 1 nouveau jeu (contrée) toutes les 2 semaines environ pendant 2-3 ans. Pendant que le processus de publication avait lieu, je suis revenu sur mes projets perso (Open Flash) et l'archivage du forum._

_Mais le 27 mars 2020 la Motion-Twin a posté une annonce: ils vont fermer leurs jeux suite à la fin de Flash. Et ça a chamboulé mes plans (début d'Eternaltwin). Depuis, j'étais concentré principalement sur Eternaltwin, ce qui a causé une pause au niveau des nouvelles fonctionnalités pour Eternalfest._

_Mais j'ai pu passer quelques mois sur Eternalfest à nouveau fin 2022 et en janvier 2023 on a publié une nouvelle mise à jour majeure qui ajoutait la gestion des frigos pour les jeux d'Eternalfest (jusque là les frigos n'étaient pas visible) + une refonte complète de la partie interne du site qui n'avait pas vraiment bougé depuis 2018._

_On en arrive au 5 mars 2023, où on fêtera les 10 ans de cette aventure 🙂._

**Merci pour ce partage de ton histoire Demu, c'est super intéressant de voir tout ce qui a déjà pu être fait et comment nous en sommes arrivés ici. Du coup Eternalfest est vraiment un morceau de ta vie. Est-ce que ça t'as aidé professionnellement ou est-ce plutôt l'inverse ?**

_Oui, je pense qu'Eternalfest a été un énorme plus au niveau professionnel. Ça m'a aidé à plusieurs niveaux :_

1. _**Compétences techniques**. On apprend par la pratique, et ça m'a permis de beaucoup pratiquer : sécurité, architecture logicielle, gestion de DB, déploiements, compilation, etc._

2. _**Open Source**. Travailler sur Eternalfest a été ma porte d'entrée pour l'Open Source au sens large. Quand j'ai un problème sur un projet perso, je vais corriger le problème à la source. J'ai pu devenir très actif dans les outils Node en devenant maintainer de Gulp, membre de Babel, ou Typed (l'ancêtre de DefinitelyTyped, j'ai écrit les premières définitions TypeScript pour les grosses libs Node). J'ai écrit l'algo de fusion de rapport des couverture de tests pour V8 (utilisé par Deno et c8). Plus récemment j'ai contribué à Postgresql et SQLx. Ce sont tous de gros projets très connus en informatique, je suis fier d'avoir pu travailler sur eux ; et ça aide à trouver du travail 😛._

3. _**Collaboration**. J'ai démarré Eternalfest/Eternaltwin mais je n'ai jamais été seul. Ces projets ont été possible grâce à l'aide de très nombreuses personnes. Pour ça il a fallut trouver comment partager le travail, faire des compromis, gérer les attentes et les désaccords. Il y a une dizaines de membres actifs pour Eternalfest, et plus d'une soixantaine de contributeurs pour Eternaltwin. Travailler en équipe ainsi est un gros plus._

4. _**Publier**. Démarrer un projet est relativement facile, mais le publier et le maintenir derrière est bien plus dur. Réussir ça m'a aussi aidé au niveau professionnel. Je tiens d'ailleurs à féliciter tous les contributeurs qui ont pu sortir leur jeu, même en beta. Vous êtes très forts._

**Mais comment fais-tu pour répartir ton temps entre ton travail, le développement de Eternalfest, de Eteraltwin et tes autres occupations ?**

_C'est compliqué malheureusement, les journées sont trop courtes. Pour commencer, la vraie vie a la priorité : si de la famille ou des amis veulent qu'on passe du temps ensemble, je ne refuse jamais. Ensuite, j'ai la chance de travailler en télétravail complet; donc j'évite les transports. Je travaille jusqu'à 17-18h; et ensuite j'ai du temps libre. Je travaille sur Eternalfest/Eternaltwin le soir, parfois assez tard ; et les week-end. Quand je suis seul, je ne prends pas beaucoup de temps libre (c'est pour ça aussi que je suis toujours partant pour participer avec les gens). Ça dépend aussi des périodes, parfois il y a plus de travail ; parfois il y en a moins. Mais je ne me plains pas trop, j'aime ce que je fais. Un point important est que j'évite les deadlines pour mes projets : ça me stresse. De manière générale, c'est important que participer à Eternaltwin reste un plaisir, il ne faut pas se forcer._

_Ma priorité sur Eternaltwin est de donner un maximum d'autonomie aux différentes équipes. Vu que je n'ai pas beaucoup de temps, je ne veux pas bloquer le progrès des autres. Le meilleur exemple pour ça est la mise en place au cours de l'été 2022 du nouveau système de déploiement : avant il fallait que je déploie manuellement, après ça les équipes pouvaient envoyer les màj elles-même._

**As-tu suffisament de temps pour voir des loutres ?**

_La partie "loutre" du zoo de Lyon était fermée à cause du Covid, du coup j'ai pas pu les voir ; et depuis je n'y suis pas retourné 😭._

**Un dernier mot ?**

_S'il y en a qui cherchent du travail en ce moment et ont contribué à Eternaltwin, je suis prêt à appuyer votre travail / faire des lettres de motivations si vous êtes intéressés. Je suis épaté par le travail réalisé et aimerait pouvoir vous aider si possible._
