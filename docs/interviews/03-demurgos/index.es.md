# Demurgos

**¡Hola, Demurgos! Gracias por tomarte unos momentos para responder estas preguntas. La mayoría de Eternaltwin ya ha oído hablar de ti. Aun así, ¿podrías introducirte?**

_Hola 👋_

_En la vida real me llamo Charles y soy un Ingeniero en Informática, pero en el Internet soy más conocido como Demurgos por mis contribuciones de Código Abierto y mi trabajo en Eternalfest y Eternaltwin. Vivo en Francia (cerca de Lyon) y tendré 28 en Septiembre. Me gustan los juegos de rompecabezas, ciclismo y las nutrias._

**Eres uno de los miembros fundadores de Eternaltwin, ¡pero también de Eternalfest! ¿Puedes contarnos más acerca de este proyecto que ya tiene 10 años de antigüedad?**

_Desde que era un niño, siempre jugué en una computadora, pero sólo comencé a usar el internet a mediados del año 2000, cuando tenía aproximadamente 10 años de edad. Fue entonces la era dorada de los juegos Flash y tenía un montón de diversión en Miniclip o Armor Games. Rápidamente descubrí Prizee (que estaba colaborando con MT), luego Hammerfest como mi primer juego de MT. Inmediatamente amé este juego de plataformas donde controlas a un hombre de nieve por sus aspectos de arcade y sus mecánicas (sorprendentemente complejas). Hammerfest, gracias a su foro, es también la primer comunidad de internet a la que me uní. Por lo que es un juego al que tengo mucho aprecio._

**¿Así que empezaste a jugar Hammerfest casi desde el principio? ¿Eras de las personas que utilizabas muchos códigos para conseguir copos de nieve?**

_Sí, me uní a Hammerfest en su comienzo (mi ID es "127" en Hammerfest; también tengo ID "38" en Twinoid: esas son mis IDs más bajas, así que me gusta presumirlas un poco, incluso aunque no sean tan importantes 😛 )._

_De hecho, el modelo "free to play" no se encontraba muy maduro en ese entonces. Teníamos 5 partidas ofrecidas al momento de creación de la cuenta, y luego nada. Tenías que pagar. Al principio trabajé mucho con el reinicio de la cuenta. 5 partidas, reiniciar, y empezar de nuevo. Terminé comprando un código SMS que me dio acceso a algunas partidas a la semana. Luego dejé Hammerfest por un lado durante un tiempo, y cuando volví ya tenía un montón de partidas acumuladas._

_Detrás de eso, también jugué muchos juegos de Motion-Twin, y era un miembro activo de Muxxu y luego Twinoid. En 2012, el streaming se estaba volviendo popular. Hammerfest siendo el juego más interesante de MT para ver en vivo, hubo un periodo en el que muchas personas stremearon sus partidas. Luego de ser inactivo en Hammerfest por algunos años, esta locura me permitió reconectar con jugadores de Hammerfest. Durante un stream al final de octubre de 2012, me uní a un grupo de Skype que cambiaría mi vida: "El Lugar Donde Se Oculta La Zanahoria" ("The Carrot Hiding Place")._

**El Cofre Zanahoria, ¿no era ese el grupo de genios de Hammerfest que ves en el juego al principio?**

_Sí, el nombre está basado en eso, y en el juego oficial es "el lugar de la zanahoria", mientras que en Skype éramos "el escondite"._

**¿Después de cuánto tiempo te volviste un poseedor de la zanahoria, el Santo Grial de los Hammerfestianos ?**

_Era bastante malo. Conseguí mi zanahoria bastante tarde. Tendría que buscar la fecha de nuevo, pero podría ser adentrado en el 2012, luego de unirme al grupo de Skype (y gracias a sus consejos). Luego me tomó otros 2-3 años para conseguir la glicerina de nieve (misión final del juego)._ 

**Te dejaré continuar la historia.**

_También era un Augurio de Hammerfest (eso era el sistema interlocutor con los administradores de MT) y empecé a programar en 2010, en segundo grado (HTML/PHP), luego hice extensiones de navegadores para Muxxu/Twinoid ("MotionMax") e hice el mapa interactivo OdysseyMap (como FeverMap pero para Odyssey)._

_Hammerfest tuvo una comunidad activa de creadores de niveles. No había editor de nivel, pero la gente usaba Paint para dibujar sus niveles y luego compartirlos en sus blogs. En 2010, más o menos, un proyecto causó una conmoción en Hammerfest: el jugador gary380 creó el "Templo Olvidado", una versión hackeada de Hammerfest con nuevos niveles. MT prohibió y evitó el hack de ser distribuido._

_Al final del año, el jugador GlobZOsiris (un jugador muy famoso, él fue el primero en completar la misión final del juego) se unió al grupo de Skype. Él tenía acceso al Templo Olvidado y nos explicó cómo jugarlo y hacer nuestros propios niveles. No estaba muy entusiasmado porque MT nos había prohibido jugarlo, y por la manera en que funcionaba hacía muy fácil hacer trampa en el juego real (clasificaciones, misiones). Pero los otros miembros del grupo lo amaron y empezaron a crear sus propios niveles, enviárselos entre ellos, jugarlos, etc._

_En marzo de 2013, era la última persona que todavía se seguía rehusando a jugarlo. MT tiene un hábito de no tocar sus juegos una vez que están finalizados, así que tener la posibilidad de contenido nuevo era muy tentador. Así que decidí codificar mi propio sistema que permitiera compartir niveles y jugarlos, pero el compartirlo sería automático (sitio web en lugar de enviar ZIPs manualmente) y sería aislado del sitio web oficial (eliminando toda posibilidad de usarlo para enviar resultados falsos al sitio web oficial). Así que modifiqué el cargador, codifiqué un sistema de alojamiento básico y envié todo el paquete a un alojamiento gratuito; todo un gran revuelto, pero funcionó._

_Y finalmente compartí este sitio con el grupo y envié este mensaje:_

_**[05/03/2013 21:22:56] Demurgos: Ce sera eternalfest**_

_Luego de Marzo 2013, trabajamos un montón en el lado técnico (hubo un montón de desafíos, pero evitaremos perdernos en ellos). Donde el Templo Olvidado falló en obtener la aprobación de MT, nuestra meta era hacer algo mucho más limpio y tener el derecho de compartir nuestro trabajo._

_Durante el verano creamos una cuenta de Twinoid para compartir el progreso: [Eternalfest](https://twinoid.com/user/7019378)._

_El jueves 5 de septiembre, abrimos un hilo en el foro para anunciar el proyecto a la comunidad: [Proyecto Eternalfest](http://www.hammerfest.fr/forum.html/thread/476324/)_

_Dado que MT ha prohibido el Templo Olvidado, y lo que estábamos haciendo técnicamente no estaba permitido por las reglas, teníamos miedo de ser baneados. Por lo tanto, usamos una cuenta anónima y hablábamos sobre nosotros con aliases. Era un poco como una "sociedad secreta" (la pirámide del logo de Eternalfest también era parte). Fue divertido 😛._

_También publicamos un hilo en Twinoid donde presentamos el juego y pedimos hablar con los administradores. Recuerdo que causó un montón de problemas con los moderadores 😄._


_Enviamos un mail detallado a MT describiendo cómo funciona el juego y cómo jugarlo. Luego de unos recordatorios, finalmente obtuvimos una respuesta al final de 2013: "mantengan esto para ustedes, prohibido compartirlo"._

_Fue una pena, pero aceptamos. Aun así seguimos creando niveles, haciendo shows en vivos, compartiendo noticias. Durante los primeros tres años, creamos unas docenas de regiones, rehicimos el sitio 3 veces (estaba aprendiendo un montón de software), preparamos un sistema para modificar el juego, etc. Durante el tiempo, los integrantes del grupo también se hicieron amigos cercanos._

_En 2015 o 2016, sucedió el próximo detonante: hubo un gran fallo en el servidor oficial que lo hizo inaccesible por una semana. Desde Blobzone, Conquisla o Pioupiouz habían cerrado, y Hammerfest ya tenía alrededor de diez años (publicado el 27 de febrero de 2006, si recuerdo correctamente), temíamos que esto fuera el final del juego. Afortunadamente, el sitio fue finalmente arreglado, pero nos empujó a revisar un poco el funcionamiento:_

1. _Eternalfest tenía que ser 100% independiente de Hammerfest (hasta ese entonces estaba usando una extensión de navegador para agregar un menú al sitio oficial)_**
2. _Era necesario alcanzar una solución para archivar su cuenta y el foro._

_Así que fuimos desde un nivel básico de compartir sistemas a un sitio completo; esta es la versión que todavía tenemos hoy en día. En el verano de 2017, también decidimos abrir Eternalfest en beta para el público. Todavía no había derecho para jugar (seguimos respetando la decisión de MT), pero los jugadores tenían la posibilidad de vincular su cuenta y crear un archivo de su refrigerador._

_Había entre 100 y 200 personas que se unieron a esta beta, era realmente bueno ver a todos estos primeros usuarios 🙂._

_En paralelo a eso, ya se encontraba la historia del final de Flash, anunciado hace mucho tiempo. Mozilla tenía un proyecto llamado Shumway para que continúe reproduciendo los archivos Flash pero sin el plugin. Desafortunadamente, este proyecto fue abandonado. Así que decidí tomar este trabajo con [Open Flash](https://open-flash.github.io/), una suite de herramientas para reproducir los archivos sin la necesidad del plugin. Estaba pensando en desarrollarlo lo suficiente para ofrecerlo [de manera gratuita] a proyectos de código abierto, pero hacerlo pago para compañías (nunca se realizó al final, pero era mi idea de actividad luego de mis estudios)._

_Así que era menos activo en Eternalfest y más concentrado en otros proyectos, pero desarrollando mis propias herramientas para manipular Flash me permitió alcanzar la próxima gran característica: la habilidad de inyectar gráficos personalizados (y, por lo tanto, completar los mods)._

_En 2017 y 2018, los sitios de Motion-Twin eran menos y menos activos. Pero también me uní a una plataforma de comunicación nueva, Discord. Allí encontré un servidor dedicado al juego con una buena comunidad de jugadores nuevos que revivieron el interés en Hammerfest/Eternalfest y postularon varios debates. Desde 2013, respetamos la decisión de MT de no compartir Eternalfest; pero era una pena. Teníamos un gran proyecto que estuvimos trabajando durante 5 años, y estábamos orgullosos de él, pero no teníamos permitido compartirlo._

_También tuvimos retroalimentación de administradores de MT o ex-MT que les habían gustado el proyecto en una manera personal; pero ninguna respuesta oficial. Tratamos de mantener el contacto a través de emails, pero sin respuesta... Después de mucha discusión, finalmente llegamos a la decisión de abrir EternalFest al público; y mantenernos en una área "gris" donde no teníamos permiso explícito, pero con la esperanza de mantenernos fuera de problemas. En retrospectiva, parecía obvio, pero no fue una decisión sencilla._

_Así que el primer país que publicamos fue "Eternoel" en la Navidad de 2018. Fue un gran éxito. A causa de nuestro gran trasfondo, comenzamos el proceso de publicar todos nuestros juegos. Con 1 juego nuevo (condado) cada 2 semanas aproximadamente durante 2-3 años. Mientras que el proceso de publicación estaba sucediendo, yo retomé mis proyectos personales (Open Flash) y el archivado del foro._

_El 27 de marzo de 2020, Motion-Twin publicó un anuncio: cerrarían sus juegos a causa del final de Flash. Y eso puso mis planes pies para arriba (inicio de Eternaltwin). Desde entonces, estaba enfocado principalmente en Eternaltwin, que causó una pausa en las nuevas características para Eternalfest._

_Tuve la posibilidad de trabajar unos meses en Eternalfest nuevamente al final de 2022, y en enero de 2023 publicamos una actualización mayor que agregó la posibilidad de administrar el refrigerador para los juegos de Eternalfest (hasta entonces, los refrigeradores no eran visibles) + un rediseño completo de la parte interna del sitio que no había movido desde 2018._

_Nos acercamos a 5 de marzo de 2023, donde celebraremos el décimo aniversario de esta aventura 🙂._

**Gracias por compartir tu historia, Demu. Es realmente interesante ver cuánto se ha hecho y cómo llegamos aquí. Así que Eternalfest es realmente parte de tu vida. ¿Te ayudó de manera profesional o fue a la inversa?**

_Sí, creo que Eternalfest ha sido una gran ventaja profesionalmente. Me ayudó en varias maneras:_

1. _**Habilidades técnicas**. Aprendes mucho mientras haces, y eso me permitió practicar mucho: seguridad, arquitectura de software, administración de base de datos, desplegamientos, compilación, etc._

2. _**Código Abierto**. Trabajar en Eternalfest ha sido mi puerta de entrada al Código Abierto, en un sentido amplio. Cuando tengo un problema en un proyecto personal, arreglaré el problema en la fuente. Pude volverme muy activo en las herramientas de Nodos por convertirme en un mantenedor de Gulp, miembro de Bable, o Typed (el ancestro de DefinitelyTyped, escribí la primer definición de TypeScript para las librerías de gran Nodo). Escribí el algoritmo de fusión del informe de cobertura de prueba para V8 (usado por Deno y c8). Más recientemente contribuí a Postgresql y SQLx. Todos estos son proyectos grandes y bien conocidos en la web, estoy orgulloso de haber trabajado en ellos; y ayuda a encontrar trabajo 😛._

3. _**Colaboración**. Empecé Eternalfest/Eternaltwin pero nunca estuve solo. Estos proyectos han sido posibles gracias a la ayuda de muchas, muchas personas. Para eso tuvimos que encontrar cómo compartir el trabajo, hacer compromisos, manejar expectativas y desacuerdos. Hay una docena de miembros activos para Eternalfest y más de sesenta colaboradores para Eternaltwin. Trabajar en equipo es una gran ventaja._

4. _**Publicación**. Comenzar un proyecto es relativamente fácil, pero publicarlo y mantenerlo es mucho más difícil. Tener éxito en esto también me ha ayudado profesionalmente. Me gustaría felicitar a todos los colaboradores que pudieron lanzar su juego, incluso en beta. Son muy fuertes._

**Pero, ¿cómo te las arreglas para dividir tu tiempo entre tu trabajo, el desarrollo de Eternalfest, Eteraltwin y tus otras ocupaciones?**

_Es complicado por desgracia, los días son muy cortos. En primer lugar, la vida real tiene prioridad: si la familia o los amigos quieren pasar tiempo juntos, nunca me niego. En segundo lugar, tengo la oportunidad de trabajar completamente desde casa, por lo que evito el transporte. Trabajo hasta las 5-6pm; luego tengo algo de tiempo libre. Trabajo en Eternalfest/Eternaltwin por la noche, a veces bastante tarde; y los fines de semana. Cuando estoy solo, no tomo mucho tiempo libre (por eso también siempre estoy dispuesto a participar con la gente). También depende de los períodos, a veces hay más trabajo; a veces hay menos. Pero no me quejo demasiado, me gusta lo que hago. Un punto importante es que evito los plazos de mis proyectos: me estresa. En general, es importante que participar en Eternaltwin siga siendo un placer, no deberías forzarte._

_Mi prioridad en Eternaltwin es dar la máxima autonomía a los diferentes equipos. Como no tengo mucho tiempo, no quiero bloquear el progreso de los demás. El mejor ejemplo de esto es la implementación del nuevo sistema de desplegamiento en el verano de 2022: antes tenía que desplegar manualmente, luego de eso los equipos podían enviar las actualizaciones ellos mismos._

**¿Tienes tiempo suficiente para ver nutrias?**

_La parte de las "nutrias" del zoológico de Lyon estaba cerrada por el Covid, así que no pude verlas; y desde entonces no he vuelto 😭._


**¿Una última palabra?**

_Si hay alguien que esté buscando trabajo en este momento y haya contribuido a Eternaltwin, estoy dispuesto a apoyar su trabajo / hacer cartas de presentación si está interesado. Estoy asombrado por el trabajo que se ha hecho y me encantaría ayudarlos si es posible._
