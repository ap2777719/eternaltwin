# Demurgos

**Hallo Demurgos! Danke, dass du dir ein paar Minuten Zeit nimmst, um diese Fragen zu beantworten. Die meisten in Eternaltwin haben bereits von dir gehört, aber kannst du dich kurz vorstellen?**

_Hallo 👋_

_Im wirklichen Leben heiße ich Charles und bin Computeringenieur, aber im Internet bin ich besser bekannt als Demurgos für meine Open-Source-Beiträge und meine Arbeit an EternalFest und EternalTwin. Ich lebe in Frankreich (in der Nähe von Lyon) und werde im September 28 Jahre alt. Ich mag Puzzlespiele, Radfahren und Otter._

**Du bist eines der Gründungsmitglieder von EternalTwin, aber auch von EternalFest! Kannst du uns mehr über dieses Projekt erzählen, das bereits 10 Jahre alt ist?**

_Seit meiner Kindheit habe ich immer am Computer gespielt, aber ich habe erst Mitte 2000, als ich etwa 10 Jahre alt war, angefangen, das Internet zu benutzen. Damals war das goldene Zeitalter der Flash-Spiele und ich hatte viel Spaß auf Miniclip oder Armor Games. Ich entdeckte schnell Prizee (das mit MT zusammenarbeitete) und dann Hammerfest als mein erstes MT-Spiel. Dieses Plattformspiel, in dem man einen Schneemann spielt, gefiel mir sofort wegen seines Arcade-Aspekts und seiner (überraschend komplexen) Spielmechanik. Hammerfest war dank seines Forums auch die erste Internet-Community, der ich beitrat. Es ist also ein Spiel, dem ich sehr zugetan bin._

**Du hast also fast von Anfang an Hammerfest gespielt? Warst du eine Person, die eine Menge Code verwendet hat, um Flocken zu bekommen?**

_Ja, ich bin Hammerfest ganz am Anfang beigetreten. Meine ID ist "127" auf Hammerfest; außerdem habe ich die ID "38" auf Twinoid. Das sind meine niedrigsten IDs, also möchte ich damit ein bisschen angeben, auch wenn es nicht so wichtig ist 😛 ._

_In der Tat war das "free to play"-Modell zu dieser Zeit noch nicht sehr ausgereift. Bei der Erstellung des Kontos wurden 5 Spiele angeboten, danach nichts mehr. Du musstest bezahlen. Am Anfang habe ich viel mit dem Zurücksetzen des Kontos gearbeitet. 5 Spiele, zurücksetzen und neu anfangen. Am Ende habe ich mir einen SMS-Code gekauft, mit dem ich ein paar Spiele pro Woche spielen konnte. Dann habe ich Hammerfest für eine Weile beiseite gelegt, und als ich zurückkam, hatte ich einen Haufen Spiele angesammelt._

_Danach habe ich viele Motion-Twin-Spiele gespielt und war ein aktives Mitglied von Muxxu und dann Twinoid. Im Jahr 2012 wurde das Streaming immer beliebter. Da Hammerfest das interessanteste MT-Spiel ist, das man live sehen kann, gab es eine Zeit, in der viele Leute ihre Spiele streamten. Nachdem ich einige Jahre lang auf Hammerfest inaktiv gewesen war, ermöglichte mir diese Begeisterung, wieder mit Hammerfest-Spielern in Kontakt zu treten. Während eines Streams Ende Oktober 2012 trat ich einer Skype-Gruppe bei, die mein Leben verändern sollte: "The Carrot Hiding Place"._

**"The Carrot Stash", ist das nicht die Gruppe von Hammerfest-Genies, die man gleich zu Beginn des Spiels sieht?**

_Ja, der Name basiert darauf, im offiziellen Spiel ist es "The Carrot Spot", aber auf Skype waren wir "The Hodeout"._

**Wie lange ist es her, dass du im Besitz der Karotte, dem Heiligen Gral der Hammerfestianer, warst?**

_Ich war ziemlich schlecht. Ich habe meine Karotte ziemlich spät bekommen. Ich müsste das Datum suchen, aber es könnte Ende 2012 gewesen sein, nachdem ich der Skype-Gruppe beigetreten war (und dank ihrer Ratschläge). Dann dauerte es weitere 2-3 Jahre, bis ich das "Snow-O-Glycerin" bekam (letzte Quest des Spiels)._

**Ich überlasse es dir, die Geschichte fortzusetzen.**

_Ich war auch Hammerfest Augur (das war das Gesprächspartner-System mit MT-Admins) und habe 2010 in der zweiten Klasse mit dem Programmieren angefangen (HTML/PHP), dann habe ich Browser-Erweiterungen für Muxxu/Twinoid ("MotionMax") und die interaktive Karte OdysseyMap (wie Fevermap, aber für Odyssey) gemacht._

_Hammerfest hatte eine aktive Gemeinschaft von Levelbauern. Es gab keinen Level-Editor, aber die Leute haben ihre Level mit Paint gezeichnet und sie dann in ihren Blogs geteilt. Im Jahr 2010 oder so sorgte ein Projekt für Aufsehen auf Hammerfest: Der Spieler gary380 erstellte den "Forgotten Temple", eine gehackte Version von Hammerfest mit neuen Levels. MT hat die Verbreitung des Hacks verboten._

_Gegen Ende des Jahres trat der Spieler GlobZOsiris (ein sehr berühmter Spieler, er war der erste, der die letzte Quest des Spiels abgeschlossen hat) der Skype-Gruppe bei. Er hatte Zugang zum Vergessenen Tempel und erklärte uns, wie wir ihn spielen und unsere eigenen Level erstellen können. Ich war nicht begeistert, weil MT es verboten hatte, es zu spielen, und die Art und Weise, wie es funktionierte, machte es einfach, das echte Spiel zu betrügen (Ranglisten, Quests). Aber die anderen Mitglieder der Gruppe liebten es und begannen, ihre eigenen Level zu erstellen, sie einander zu schicken, sie zu spielen usw._

_Im März 2013 war ich die letzte Person, die sich immer noch weigerte, es zu spielen. MT hat die Angewohnheit, ihre Spiele nicht mehr anzurühren, wenn sie einmal fertig sind, also war die Möglichkeit, neue Inhalte zu haben, sehr verlockend. Also beschloss ich, mein eigenes System zu programmieren, das es ermöglichen würde, Levels zu teilen und zu spielen, aber das Teilen würde automatisch erfolgen (über die Website, anstatt ZIPs manuell zu verschicken) und es würde von der offiziellen Website isoliert sein (keine Möglichkeit, es zu benutzen, um gefälschte Ergebnisse an die offizielle Website zu schicken). Also modifizierte ich den Loader, programmierte ein einfaches Hosting-System und schickte das Ganze zu einem kostenlosen Hoster; eine große Fummelei, aber es funktionierte._

_Und schließlich habe ich diese Seite mit der Gruppe geteilt und diese Nachricht gepostet:_

_**[05/03/2013 21:22:56] Demurgos: Ce sera eternalfest**_ (Es wird eternalfest sein)

_Ab März 2013 haben wir viel an der technischen Seite gearbeitet (es gab eine Menge Herausforderungen, aber wir werden uns nicht darin verlieren). Wo der Vergessene Tempel keine MT-Zulassung erhielt, war es unser Ziel, etwas viel Saubereres zu haben und das Recht zu haben, unsere Arbeit zu teilen._

_Während des Sommers haben wir ein Twinoid-Konto eingerichtet, um die Fortschritte zu teilen: [Eternalfest](https://twinoid.com/user/7019378)._

_Am Donnerstag, den 5. September, haben wir einen Thread im Forum eröffnet, um das Projekt der Community vorzustellen: [Project Eternalfest](http://www.hammerfest.fr/forum.html/thread/476324/)_

_Da MT den Vergessenen Tempel verboten hatte und das, was wir taten, technisch nicht durch die Regeln erlaubt war, hatten wir Angst, verboten zu werden. Also benutzten wir einen anonymen Account und sprachen mit Pseudonymen über uns. Es war ein bisschen wie ein "Geheimbund" (das Pyramidenlogo von Eternalfest war auch Teil davon...). Es hat Spaß gemacht 😛._

_Wir haben auch einen Thread auf Twinoid gepostet, in dem wir das Spiel vorstellten und um ein Gespräch mit den Administratoren baten. Ich erinnere mich, dass dies den Moderatoren eine Menge Ärger bereitet hat 😄._

_Wir schickten eine ausführliche Mail an MT, in der wir beschrieben, wie das Spiel funktioniert und wie man es spielt. Nach einigen Mahnungen erhielten wir Ende 2013 endlich eine Antwort: "Behaltet das für euch, es ist verboten, das zu verbreiten"._

_Es war schade, aber wir haben es akzeptiert. Wir haben weiterhin Levels erstellt, Liveshows gemacht und Neuigkeiten ausgetauscht. In den ersten drei Jahren erstellten wir einige Dutzend Regionen, überarbeiteten die Website dreimal (ich lernte viel über Software), richteten ein System zum Modifizieren des Spiels ein, usw. Mit der Zeit wurden die Mitglieder der Gruppe auch zu engen Freunden._

_2015 oder 2016 gab es den folgenden Auslöser: Es gab eine große Störung auf dem offiziellen Server, die ihn für eine Woche unzugänglich machte. Da Blobzone, Conquisla oder Pioupiouz bereits geschlossen waren und Hammerfest etwa zehn Jahre alt war (veröffentlicht am 27. Februar 2006, wenn ich mich richtig erinnere), wurde befürchtet, dass dies das Ende des Spiels war. Glücklicherweise wurde die Seite schließlich repariert, aber es drängte uns, die Funktionsweise ein wenig zu überprüfen:_

1. _Eternalfest musste zu 100 % unabhängig von Hammerfest sein (bis dahin wurde eine Browsererweiterung verwendet, die ein Menü zur offiziellen Website hinzufügte)._
2. _Es war notwendig, eine Lösung zur Archivierung seines Kontos und des Forums einzurichten._

_So gingen wir von einem einfachen Level-Sharing-System zu einer vollständigen Website über; das ist die Version, die wir heute noch haben. Im Sommer 2017 beschlossen wir außerdem, Eternalfest in der Beta-Phase für die Öffentlichkeit zu öffnen. Es gab immer noch kein Recht zu spielen (wir respektieren immer noch die Entscheidung von MT), aber den Spielern wurde die Möglichkeit geboten, ihr Konto zu verknüpfen und ein Archiv ihres Kühlschranks zu erstellen._

_Es waren zwischen 100 und 200 Leute, die sich an der Beta beteiligt haben, es war wirklich toll, diese ersten Nutzer zu sehen 🙂 ._

_Parallel dazu war das Ende von Flash schon seit langem angekündigt. Mozilla hatte ein Projekt namens Shumway, um Flash-Dateien weiterhin abspielen zu können, allerdings ohne das Plugin. Leider wurde dieses Projekt aufgegeben. Also beschloss ich, diese Arbeit mit [Open Flash](https://open-flash.github.io/) fortzusetzen, einer Suite von Werkzeugen zum Abspielen von Dateien ohne das Plugin. Ich dachte daran, es so weit zu entwickeln, dass es Open-Source-Projekten angeboten werden kann, aber auch, dass es sich für Unternehmen lohnt (es wurde letztendlich nie umgesetzt, aber es war meine Idee einer Aktivität nach dem Studium)._

_Ich war also weniger aktiv bei Eternalfest und konzentrierte mich mehr auf andere Projekte, aber die Entwicklung meiner eigenen Tools zur Bearbeitung von Flash ermöglichte das nächste wichtige Feature: die Möglichkeit, benutzerdefinierte Grafiken einzufügen (und damit die Mods zu vervollständigen)._

_In den Jahren 2017 und 2018 waren die Motion-Twin-Seiten immer weniger aktiv. Aber ich trat auch einer neuen Kommunikationsplattform bei, Discord. Dort fand ich einen Server, der dem Spiel gewidmet war, mit einer guten Gemeinschaft von neuen Spielern, die das Interesse an Hammerfest/Eternalfest wiederbelebten und viele Debatten auslösten. Seit 2013 respektierten wir die Entscheidung von MT, Eternalfest nicht zu veröffentlichen; aber es war trotzdem schade. Wir hatten ein großartiges Projekt, an dem wir 5 Jahre lang gearbeitet hatten, wir waren stolz darauf, aber wir durften es nicht teilen._

_Wir hatten auch Rückmeldungen von MT-Administratoren oder Ex-MTs, die das Projekt persönlich mochten; aber keine offizielle Antwort. Wir haben versucht, mit E-Mails nachzuhaken, aber immer noch keine Antwort... Nach vielen Diskussionen trafen wir schließlich die Entscheidung, das Eternalfest für die Öffentlichkeit zu öffnen und uns in einer "Grauzone" zu bewegen, in der wir keine ausdrückliche Erlaubnis haben, aber hoffen, keinen Ärger zu bekommen. Im Nachhinein scheint es offensichtlich, aber es war keine leichte Entscheidung._

_Das erste Land, das veröffentlicht wurde, war "Eternoel" zu Weihnachten 2018. Es war ein großer Erfolg. Aufgrund unseres riesigen Back-Katalogs haben wir damit begonnen, alle unsere Spiele zu veröffentlichen. Mit einem neuen Spiel (Land) alle 2 Wochen oder so für 2-3 Jahre. Während des Veröffentlichungsprozesses widmete ich mich wieder meinen persönlichen Projekten (Open Flash) und der Archivierung des Forums._

_Aber am 27. März 2020 gab Motion-Twin bekannt, dass sie ihre Spiele wegen des Endes von Flash einstellen werden. Und das hat meine Pläne auf den Kopf gestellt (Beginn von Eternaltwin). Seitdem habe ich mich hauptsächlich auf Eternaltwin konzentriert, was eine Pause bei den neuen Features für Eternalfest verursacht hat._

_Aber ich konnte Ende 2022 wieder ein paar Monate auf Eternalfest verbringen und im Januar 2023 haben wir ein neues großes Update veröffentlicht, das die Kühlschrankverwaltung für Eternalfest-Spiele hinzugefügt hat (bis dahin waren die Kühlschränke nicht sichtbar) + ein komplettes Re-design des internen Teils der Website, der sich seit 2018 nicht wirklich bewegt hatte._

_Wir nähern uns dem 5. März 2023, wo wir das 10-jährige Jubiläum dieses Abenteuers feiern werden 🙂._

**Danke, dass du deine Geschichte mit uns teilst, Demu. Es ist wirklich interessant zu sehen, wie viel getan wurde und wie wir hierher gekommen sind. Eternalfest ist also wirklich ein Teil deines Lebens. Hat es dir beruflich geholfen oder ist es andersherum?**

_Ja, ich denke, Eternalfest war beruflich ein großer Gewinn. Es hat mir in vielerlei Hinsicht geholfen:_

1. _**Technische Fähigkeiten**. Man lernt, indem man etwas tut, und so konnte ich viel üben: Sicherheit, Software-Architektur, DB-Verwaltung, Bereitstellung, Kompilierung usw._

2. _**Open Source**. Die Arbeit an Eternalfest war für mich der Einstieg in Open Source. Im weitesten Sinne. Wenn ich bei einem persönlichen Projekt ein Problem habe, werde ich es an der Quelle beheben. Ich war in der Lage, sehr aktiv in Node-Tools zu werden, indem ich ein Maintainer von Gulp, ein Mitglied von Babel oder Typed wurde (der Vorgänger von DefinitelyTyped, ich schrieb die ersten TypeScript-Definitionen für die großen Node-Libs). Ich habe den Test Coverage Report Merge Algo für V8 geschrieben (verwendet von Deno und c8). In letzter Zeit habe ich zu Postgresql und SQLx beigetragen. Das sind alles große und bekannte Projekte im Web, ich bin stolz darauf, an ihnen gearbeitet zu haben; und es hilft, einen Job zu finden 😛._

3. _**Zusammenarbeit**. Ich habe Eternalfest/Eternaltwin begonnen, aber ich war nie allein. Diese Projekte waren nur dank der Hilfe vieler, vieler Menschen möglich. Dazu mussten wir herausfinden, wie wir uns die Arbeit teilen, Kompromisse schließen, Erwartungen und Meinungsverschiedenheiten bewältigen können. Es gibt ein Dutzend aktive Mitglieder bei Eternalfest und mehr als sechzig Mitwirkende bei Eternaltwin. Die Arbeit im Team ist ein großes Plus._

4. _**Publizieren**. Ein Projekt zu beginnen ist relativ einfach, aber es zu veröffentlichen und zu betreuen ist viel schwieriger. Dies zu schaffen, hat mir auch beruflich geholfen. Ich möchte allen Mitwirkenden gratulieren, die es geschafft haben, ihr Spiel zu veröffentlichen, sogar in der Beta-Phase. Ihr seid sehr stark._

**Aber wie schaffst du es, deine Zeit zwischen deiner Arbeit, der Entwicklung von Eternalfest, Eteraltwin und deinen anderen Beschäftigungen aufzuteilen?**

_Es ist leider kompliziert, die Tage sind zu kurz. Erstens hat das wirkliche Leben Vorrang: Wenn Familie oder Freunde Zeit miteinander verbringen wollen, lehne ich das nie ab. Zweitens habe ich die Möglichkeit, komplett von zu Hause aus zu arbeiten, so dass ich Transportwege vermeide. Ich arbeite bis 17-18 Uhr; dann habe ich etwas Freizeit. Abends arbeite ich an Eternalfest/Eternaltwin, manchmal bis spät in die Nacht, und an den Wochenenden. Wenn ich allein bin, nehme ich mir nicht viel Zeit (deshalb bin ich auch immer bereit, mich mit anderen zu treffen). Es hängt auch von den Zeiten ab, manchmal gibt es mehr Arbeit, manchmal weniger. Aber ich beschwere mich nicht zu sehr, ich mag, was ich tue. Ein wichtiger Punkt ist, dass ich Fristen für meine Projekte vermeide: Das stresst mich. Generell ist es wichtig, dass die Teilnahme an Eternaltwin ein Vergnügen bleibt, man sollte sich nicht zwingen._

_Meine Priorität bei Eternaltwin ist es, den verschiedenen Teams ein Höchstmaß an Autonomie zu geben. Da ich nicht viel Zeit habe, möchte ich den Fortschritt der anderen nicht blockieren. Das beste Beispiel dafür ist die Einführung des neuen Verteilungssystems im Sommer 2022: Vorher musste ich es manuell verteilen, danach konnten die Teams die Updates selbst verschicken._

**Hast du genug Zeit, um Otter zu sehen?**

_Das "Otter"-Abteil des Lyoner Zoos war wegen Covid geschlossen, so dass ich sie nicht sehen konnte; und seither war ich nicht mehr dort 😭._

**Letzte Worte?**

_Wenn es jemanden gibt, der gerade auf der Suche nach Arbeit ist und einen Beitrag zu Eternaltwin geleistet hat, bin ich bereit, eure Arbeit zu unterstützen/Anschreiben zu verfassen, wenn ihr daran interessiert seid. Ich bin erstaunt über die geleistete Arbeit und würde euch gerne helfen, wenn möglich._
