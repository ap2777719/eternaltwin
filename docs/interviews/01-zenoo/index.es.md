# Zenoo

**¡Hola, Zenoo! Gracias por tomarte unos minutos para responder a estas preguntas. Hiciste una gran entrada en el verano de 2022 al hacerte cargo de LaBrute. ¿Puedes presentarte en pocas palabras?**

_Hola, soy Zenoo, un ex jugador de Hordes de los viejos tiempos. Soy un desarrollador de tiempo completo, con otros proyectos personales al margen, incluido LaBrute más recientemente._

**Tu primera publicación que muestra un boceto de tu versión de LaBrute fue el 30 de julio y la primera versión 1.0.0 se lanzó el 10 de agosto. ¿Cómo te las arreglaste para sacar un juego funcional tan rápido? ¿Tienes un secreto para compartir?**

_Mirando el historial de Git, realmente comencé a trabajar en este proyecto a principios o mediados de agosto, pero las primeras 2 o 3 semanas se dedicaron a descifrar/recuperar activos de los archivos fuente de MT (en ese momento todavía no estaban disponibles como lo están hoy) ._

_Así que en realidad comencé a codificar el proyecto a fines de agosto. La v1 se lanzó poco después, pero cabe recordar que esta v1 fue la primera versión funcional, pero le faltaban muchas cosas. La visualización de las peleas no existía, por ejemplo, los jugadores solo tenían una vista textual de lo que pasaba en las peleas._

_En cuanto a la velocidad de producción, trato de ser eficiente en mi metodología y organizo estrictamente mi desarrollo. Todas las tareas están agrupadas en un flujo de trabajo (disponible en mi [Github](https://github.com/Zenoo/labrute/issues), si alguna vez quieren venir y ayudar), lo que me permite olvidarme de todo lo que tengo que hacer cuando empiezo una tarea, y concentrarme plenamente en ella._

**¡De hecho, podemos notar que estás trabajando en el proyecto por sesiones bastante intensamente. ![Entrevista - Git Graph de Zenoo](./interview_zenoo.png)**

_En realidad, tiendo a echarme humo cuando empiezo algo, probablemente se deba a la satisfacción cuando logro superar un obstáculo. A medida que pasa el tiempo, los obstáculos se hacen cada vez más grandes, lo que hace que me detenga y vuelva a empezar más tarde, una vez que se acaba la emoción y nuevas ideas son encontradas._

**LaBrute comienza a ser estable y atrae a más y más jugadores. ¿Puedes darnos algunas estadísticas sobre la cantidad de bullies creados? **

_En lo que respecta a las estadísticas, lamentablemente no puedo proporcionar ninguna para todo el desarrollo, ya que ha habido varios reinicios. Desde el último reinicio (30 de enero), se han creado un total de 11925 brutos._

** Oh, sí, de hecho, casi 12k de brutos en menos de un mes, ¡eso es realmente impresionante! Antes dijiste que eras un exjugador de Hordes. ¿Me parece que eres el creador de Zavatar, un sitio web de creación de avatares?**

_¡Vaya, eso fue hace mucho tiempo! De hecho, creé el V2 de Zavatars con Tsha (una persona con un talento inmenso para dibujar), que ya estaba administrando Zavatars V1 varios años antes de que fuera abandonado._

_El sitio sigue funcionando desde que abrió en abril de 2016, aunque por el momento está muy tranquilo. A diferencia del desarrollo en curso en LaBrute, este proyecto se desarrolló de una sola vez, prácticamente. No recuerdo haber tenido que hacer una actualización para corregir un error, era una época diferente, con un desarrollo mucho más sencillo que el actual 😄._

_Por cierto, es posible que algún día aparezcan Zavatars en MyHordes, ¿quién sabe?_

_... ¡Y no solo Zavatars!_

**¡Muy buenas noticias para Zavatars! Y como Hordiano, ¿por qué elegiste desarrollar LaBrute en lugar de ayudar en MyHordes? ¿Las tecnologías utilizadas no eran de tu agrado?**

_Así es, MyHordes está desarrollado en PHP, un lenguaje que prefiero no tocar más, lo usé demasiado hace diez años, lo encuentro demasiado atrasado en comparación con lo que tenemos hoy en día._

**¿En qué porcentaje de progreso crees que estás con LaBrute? ¿Tiene un objetivo final para decir "terminé" o planeas agregar funciones incluso si eso significa rehacer v2?**

_Es difícil estimar el progreso de LaBrute, simplemente porque es un proyecto de desarrollo continuo, que tiene en cuenta los comentarios y sugerencias de los usuarios. Sin contar las ideas de desarrollo que llegan a diario, diría que actualmente estoy en un 30% de progreso._

_Realmente no tengo un objetivo final, excepto tener al menos todas las características que existían antes, y sin errores. En comparación con V2, esta nueva versión es en realidad una combinación de V1/V2, tratando de mantener lo mejor de ambos. También planeo agregar algunas funciones que no estaban ni en V1 ni en V2._

**Imaginemos por un minuto que consideras terminada tu versión de LaBrute. Has podido agregar todas las características interesantes de v1 y v2. ¿Qué función o modo de juego agregarás? ¿O tal vez nada y pasar a otro juego para acelerar el desarrollo?**

_Una vez que V1 y V2 estén implementadas, estoy pensando en agregar un sistema de logros, misiones, mejoras en el sistema de recompensas del torneo y sacrificio de bullies. Una vez hecho todo esto, si DinoRPG aún no está terminado, quiero echarle un ojo. Este tipo de juego con batallas automáticas y un sistema de nivelación diario es muy satisfactorio para mí._

_Habría vuelto a hacer Hordas más adelante, pero como ya está manejado, no sucederá. Otro juego del universo MT que me llamó la atención hace 10 o 15 años fue Zepirates, que tiene el mismo principio de nivelación diaria. Si no encuentro nada más mientras tanto, tal vez pueda entrar._

**¡Tantas buenas noticias! ¿Cuál ha sido tu mayor desafío en el desarrollo de LaBrute hasta ahora? ¿Y cuál es la característica que más te asusta, que parece ser la más difícil de integrar?**

_El mayor problema que tengo con LaBrute es que no es fácil de integrar. El mayor problema que he encontrado hasta ahora ha sido configurar los activos de los brutos y mostrarlos correctamente en el motor de combate. Crear los sprites de los brutos con apariencias personalizadas es una tarea monumental._

_Esta es también mi respuesta a la segunda pregunta. Terminar la implementación de los sprites de los bullies con colores/formas personalizados, y la implementación de la visualización de armas y escudos requerirá que cree cientos, si no miles, de archivos fuente, cada uno de los cuales incluye la lógica de mostrar los colores/formas en cada cuadro de cada animación._

_**Uff, un gran trabajo.**_

**De hecho, tenemos el mismo problema en DinoRPG y vamos poco a poco porque cada dinoz en pantalla estática (fuera de combate) representa ya cientos de archivos. Última pregunta: ¿puede decirnos cuál será el contenido de la próxima gran actualización de LaBrute?**

_Probablemente será la implementación de los diferentes rangos de bruto, después de ganar un torneo._

**¡Excelente! ¡Gracias por todas sus respuestas! ¿Tienes algo más que agregar? 🙂**

_Siéntase libre de pasar por Discord para compartir sus comentarios y notificarme cualquier error que encuentren, ayuda a que el proyecto avance._
