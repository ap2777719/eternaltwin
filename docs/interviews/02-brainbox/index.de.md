# Brainbox

**Danke, dass du dir für "Diesen Monat in EternalTwin" Zeit genommen hast. BrainBox ist ein Name, den viele Leute kennen, aber kannst du eine Präsentation für die anderen machen?**

_Klar. Ich bin Brainbox, 32 Jahre alt, komme aus Deutschland und bin der Projektleiter für MyHordes. Ich spiele DieVerdammten (deutsche Version von Hordes) seit Saison 2 und habe mit Saison 3 begonnen, externe Apps dafür zu entwickeln._

**Hordes (und seine anderen Brüder) war eines der symbolträchtigsten Spiele von MotionTwin. Wann hast du mit der Entwicklung von MyHordes begonnen?**

_Ich habe Ende November 2019 begonnen, lange bevor Eternaltwin gegründet wurde. Die Idee kam auf, als ich mit einem DV-Kollegen über das Ende der Flash-Player-Unterstützung diskutierte. Wir beendeten unsere Diskussion mit dem Gedanken, dass es toll wäre, wenn jemand DieVerdammten ohne Flash neu machen würde; am nächsten Tag dachte ich mir: "Hey, warum auf jemand anderen warten, wenn ich es selbst machen kann?" und machte mich an die Arbeit._

**Und gestern, am 11. Februar 2023, dreieinhalb Jahre später, ist das Spiel endlich bereit für seinen Start mit Saison 15. Das ist eine beeindruckende Leistung. Was hältst du davon?**

_Es war in der Tat eine Menge Arbeit. Wenn ich zurückblicke, bin ich selbst beeindruckt, dass wir geschafft haben, es durchzuziehen. Man sieht bei dieser Art von Open-Source-Projekten immer wieder; die Entwickler verlassen das Projekt, um sich anderen Dingen zu widmen, die Updates werden langsamer und langsamer und am Ende stirbt das Projekt einfach. Aber Hordes hat eine sehr aktive Community, so dass das ständige Feedback - gut und schlecht - uns motiviert hat._

_In gewisser Weise bin ich froh, dass wir es endlich geschafft haben. Auf der anderen Seite haben wir tonnenweise Ideen für zukünftige Verbesserungen, Inhalte und Dinge, die wir verbessern wollen und so weiter. Das Ende der Beta-Phase ist also sozusagen erst der Anfang weiterer Entwicklungen._

**Was ist im Moment die größte Herausforderung, die du bei diesem Projekt bewältigt hast? Und was ist deine stolzeste Errungenschaft?**

_Die größte Herausforderung war es, alle Details richtig hinzubekommen; selbst während der Beta haben wir immer wieder Dinge gefunden, die sich von Hordes unterschieden. Es ist ein sehr komplexes Spiel, aber ich hatte nicht erwartet, dass es DIESE Komplexität haben würde._

_Meine stolzeste Errungenschaft geht ein wenig in die gleiche Richtung: Wir haben es geschafft, viele Dinge hinzuzufügen, die irgendwann einmal aus Hordes entfernt wurden, wie zum Beispiel den Beruf des Schamanen oder die automatischen Ghule. All diese Dinge sind nun wieder in MyHordes enthalten und können als Optionen für private oder Event-Städte ausgewählt werden._

**Hat dir die Veröffentlichung des Quellcodes von MotionTwin geholfen oder waren die Vorteile eher anekdotisch?**

_Es hat uns sehr geholfen. Wir konnten viele Werte, wie z. B. die Drop-Chancen ermitteln, für die wir vorher nur Schätzungen aus der Community hatten. Außerdem konnten wir viele Originaltexte erhalten, die wir sonst aus den Originalspielen hätten auslesen müssen._

**Vermisst du noch einige Gameplay-Daten? Ich habe gehört, dass die Zombieverteilung noch nicht ausbalanciert ist? Denkst du MT gibt dir mehr Quellcode?**

_Wir haben zwar Zombie-Ausbreitungsdaten im Quellcode, den MT uns gegeben hat, aber es ist seltsam. Wir haben es tatsächlich geschafft, den Code ohne Änderungen zum Laufen zu bringen, und er liefert Ergebnisse, die nicht mit der uns bekannten Zombie-Ausbreitung vergleichbar sind. Wir vermuten, dass es sich entweder um einen neuen Ausbreitungsalgorithmus handelt, der nie aktiviert wurde, oder um eine Art Experiment. Also ja, der eigentliche Zombie-Ausbreitungsalgorithmus ist uns immer noch nicht bekannt._

_Es fehlen auch einige kleinere Dinge, zum Beispiel in Bezug auf die Drop-Chancen von Gegenständen in erforschbaren Ruinen._

_Soweit ich mich erinnere, hat skool jedoch irgendwo erwähnt, dass dies der einzige Code ist, den sie weitergeben können. Ich habe also wenig Hoffnung, dass uns der Rest des Codes zur Verfügung gestellt wird._

_Ich schätze, wir müssen einfach das Beste aus dem machen, was wir haben._

**Ich denke, wir sind uns alle einig, dass du es bereits geschafft hast. Deine Arbeit an MH ist beeindruckend. Wie hast du es geschafft, jeden Montag ein Update zu liefern? Wenn wir uns den Git-Graphen ansehen, können wir sehen, dass du fast jede Woche an dem Projekt arbeitest.**
![Interview - Git Graph of BrainBox](./interview_brain.png)

_Normalerweise verbringe ich abends Zeit mit meinen Freunden oder meiner Familie, so dass ich am Wochenende tagsüber arbeiten kann. Es gibt natürlich einige Ausnahmen, wie z. B. Urlaube. Aber selbst dann schaffe ich es meistens, mindestens einen Tag pro Wochenende zu arbeiten, was ausreicht, um ein Update mit wichtigen Korrekturen herauszubringen._

**Dein Vollzeitjob ist auch Webentwickler? Bist du es nicht leid, die ganze Woche und die Wochenenden über Code zu schreiben?**

_Nicht wirklich, ich schätze, ich erfülle das Klischee des arbeitsbesessenen Deutschen. Ich liebe es, zu programmieren, und ich mache es gerne, mit oder ohne Bezahlung._

**Aha, das ist gut zu hören. Und hat dir MH geholfen, deine Fähigkeiten für deinen Job zu verbessern?**

_Um ganz ehrlich zu sein, hat MyHordes mir geholfen, diese Stelle zu bekommen. Als ich mich beworben habe, habe ich es als Referenz angegeben. Ich habe vorher als Universitätsforscher gearbeitet, hauptsächlich in C++ und OpenGL. Obwohl ich nebenbei viele Webprojekte gemacht habe, war keines davon wirklich geeignet, als Referenz verwendet zu werden. Ich denke, wenn ich nicht MyHordes gehabt hätte, um meine Webentwicklungsfähigkeiten zu zeigen, wäre es schwieriger gewesen, meine jetzige Position zu erreichen._

_Aber was die Fähigkeiten angeht, ist es eigentlich umgekehrt. Mein Job gibt mir reichlich Gelegenheit, neue Dinge zu lernen und auszuprobieren, die ich dann in MyHordes einbauen kann._

**Wow, das ist eine gute Geschichte! Und was die Zukunft von MH angeht, habt ihr einen Zeitplan für die Saisons oder werden sie veröffentlicht, wenn sie fertig sind? Jetzt, wo Saison 15 online ist, warten wir schon auf Saison 16 und ihre neuen Features!**

_Wir haben vor, einen groben Zeitplan einzuhalten, auch wenn noch nicht endgültig feststeht, in welchen Abständen wir das tun werden. Natürlich können wir eine neue Saison erst veröffentlichen, wenn ihre Entwicklung abgeschlossen ist, und da wir alle Freiwillige sind, ist es etwas schwierig, genaue Schätzungen abzugeben._

_Aber sei versichert, dass es weitere Saisons geben wird, und wir werden sie so regelmäßig wie möglich herausbringen._

**Es ist beruhigend, Dinge wie diese zu hören. Ich denke, ich habe alles gefragt, was ich in meinem Ärmel habe, möchtest du noch etwas hinzufügen? Vielleicht ein Hinweis auf ein zukünftiges Feature?**

_Ich möchte dem Rest des Teams ein Lob aussprechen: Dylan, Nayr, Ludofloria und Adri, sowie allen anderen, die zu MyHordes beigetragen haben! Ohne sie hätte ich es nicht geschafft!_

_Und als Hinweis... nun, sagen wir mal, wir haben einige Spuren von ungenutzten Inhalten oder zukünftigen Ideen für Hordes gefunden, die nie umgesetzt wurden, wie..._

**Hmmm es scheint, dass dieses Interview vorbei ist, weil ein Zombie Brainbox gefressen hat. Vielleicht wollten sie nicht, dass Brain ihre Pläne verrät...**
