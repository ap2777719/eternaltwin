# 2022-05: Este mes en Eternaltwin #8

[Eternaltwin](https://eternal-twin.net) es un proyecto con el objetivo de
preservar los juegos y comunidades de jugadores de Motion Twin.

# Eternaltwin

Tras una breve pausa en abril, [el foro de Eternaltwin](https://eternal-twin.net/forum)
está habilitado nuevamente.

Eternaltwin ahora es una organización Open Source oficial en Gitlab. Gitlab es
la plataforma para colaboración de código que usamos para todos los proyectos
de Eternaltwin. Con nuestro nuevo estatus, ganamos acceso completo a todas las
funcionalidades de Gitlab de forma gratuita.

En junio, trabajaremos en mejorar los permisos de Gitlab. Ahora mismo, la
mayoría de contribuidores tienen acceso completo a _todos_ los juegos. Lo
cambiaremos para que, en su lugar, los permisos sean concedidos para juegos
_específicos_. Esto permitirá que los lideres de proyecto puedan administrar
mejor el trabajo en cada proyecto.

Finalmente, estamos en proceso de configurar el servidor mencionado hace
algunos meses. Esto les dará a los líderes de proyecto el control completo para
enviar nuevas versiones de sus juegos en línea, sin coordinar con los
administradores de Eternaltwin.

# MyHordes

Reproducir a [MyHordes](https://myhordes.eu/).

_estática de radio_

_silencio prolongado_

_movimiento_

Huff Huff… hola a todos … Ben aquí, una vez más. Lo siento, estoy un poco falto
de aliento, pero Connor me tiene moviendo cajas llenas de papeles de aquí para
allí. Dice que son para alguna clase de voto, o algo…

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Hablando de papel, tenemos un sistema de clasificación de tecnología punta
completamente nuevo para mensajes personales. Por fin, todo este correo basura
aleatorio está siendo apropiadamente clasificado. Incluso hicimos una bandeja
de entrada personalizada para el correo de fans de nuestra estación de radio,
y ya está desbordando de correo de todos ustedes _estática de radio_ habitantes.
Solo me pregunto por qué Connor la tiene etiquetada como “denuncias y amenazas
de muerte”.

De cualquier modo, suficiente de eso. ¡Vayamos a las noticias! Hemos escuchado
que algunas personas quieren jugar con espíritus de nuevo, y juntarse alrededor
de algunos edificios extraños en su pueblo. ¿Cuál es el asunto con las almas
aquí? ¡Prepárense! ¡Los charlatanes… digo los chamanes volverán de nuevo en un
pueblo especial!

_estática de radio_ …yer, un reporte llegó sobre gente empezando a ondear
banderas de países por aquí. He visto a Chile, Austria, Japón, e incluso
algunas extrañas de un lugar inventado que le gente está llamando “Francia”,
aparentemente. Es extraño… _estática de radio_

Por último, pero no menos importante, un anuncio de servicio público. Algunas
almas valientes se están preparando para aventurarse al Ultramundo de Hordes
para registrar información sobre todo tipo de fenómenos extraños.
¿Cuántos objetos se esconden en la arena? ¿Cuáles son las chances de morirse
por el agua maloliente hallada frecuentemente en ruinas? ¿Cuándo fue la
última vez que nuestro Chamán se cambió la ropa interior? _estática de radio_
Si desean ayudar, por favor échenle un ojo a [esta publicación en la pizarra
de nuestra estación de radio](https://myhordes.eu/jx/forum/9/42008).

# eMush

¡Regresamos con algunas noticias sobre [eMush](https://emush.eternaltwin.org/)!
El progreso ha sido un poco más lento últimamente, pero no se preocupen,
seguimos ahí.

Hay gran progreso en la interfaz gráfica. ¡Ahora tenemos 18 de 26 habitaciones
del Daedalus! Hicimos un montón de trabajo en la búsqueda de ruta y cómo
mostrar los distintos sprites. También es más rápido ahora.

La versión 0.4.3, con una interfaz gráfica completa, está bien encaminada. Solo
tenemos que agregar las habitaciones faltantes (torretas, habitación del motor,
cubierta) y agregar animaciones para sentarse/acostarse para los personajes.

Espero volver a ustedes el mes siguiente con la versión de eMush 0.4.3, con un
lanzamiento alfa para dejarles ver qué hay de nuevo.

# Kube

¡Los jugadores _Booti_, _Moulins_, y _TheToto_ han resumido el trabajo en Kube!

## Trabajo previo

En 2020, _Moulins_ ha creado un respaldo del mundo de Kube. [Un hilo del foro fue
creado en el foro de Kube.](http://kube.muxxu.com/tid/forum#!view/492|thread/65361326)
A lo largo de 2021, el respaldo fue actualizado, y ahora incluye los foros
personales de cada zona.

## Reimplementación del generador del mundo

_Booti_ recientemente empezó a reimplementar el algoritmo de generación del mundo.
Afortunadamente, el código estaba en el archivo SWF del juego.

Kube también tenía un antiguo error sin resolver con islas flotantes
("îles volantes") faltantes y biomas mezclados. Resulta que era causado porque
las nuevas versiones de Flash rompían la compatibilidad con código más viejo.

La reimplementación corrige estos errores.

## Nuevo cliente del juego

_TheToto_ está creando un nuevo cliente del juego para jugar sin Flash. Todavía
es temprano, pero ya es posible colocar o quitar kubos, y cargar información.
La información puede ser cargada del servidor oficial, el respaldo previamente
mencionado, o planos Kub3dit.

[El código está disponible aquí.](https://gitlab.com/eternaltwin/kube/kube-client-html5)

## Demostración

Isla flotante ("île volante") generada por el código de _Booti_ y vista a través
del cliente del juego de _TheToto_.

![Kube](./kube.png)

Únanse al canal “Kube” en [el servidor de Discord de Eternaltwin](https://discord.gg/ERc3svy),
si quieren hablar de Kube. 😺

# Eternal DinoRPG

Hola queridos maestros de [DinoRPG](https://dinorpg.eternaltwin.org/).

Liberamos la versión 0.2.2. Esta versión trae algunos arreglos gráficos para
mostrar el Dinoz entero en todo lugar donde pueda aparecer.

Estamos trabajando en subir de nivel. Te dejará ver a tu Dinoz (bueno, solo si
es un Moueffe por ahora) evolucionar y viajar a través de Dinoland.

# EternalKingdom

¡La implementación de la 4ta Alfa continúa! La página de Selección de mundo
está terminada. Una ciudad ahora es asignada a nosotros cuando nos unimos a un
mundo. Y esta ciudad se convierte en una ciudad bárbara cuando nos morimos.

![EternalKingdom - Selección de mundo](./kingdom-select.png)

Ahora nos concentramos en implementar los elementos principales de la página
del Mapa del Mundo incluyendo la visualización de nodos (capital, bárbaros y
sitios de recursos), el menú lateral mostrando las acciones disponibles para el
jugador y los Registros Mundiales.

![EternalKingdom - Mapa del Mundo](./kingdom-world.png)

# Palabras finales

Pueden mandar [sus mensajes para junio](https://gitlab.com/eternaltwin/etwin/-/issues/48).

Este artículo fue editado por: Bibni, Biosha, Brainbox, Breut, Demurgos,
MisterSimple, Patate404, Schroedinger y TheToto.
