# 2023-03 : Ce mois-ci sur Eternaltwin n°18

[Eternaltwin](https://eternal-twin.net) est un projet visant à préserver les jeux et communautés de joueurs de Motion Twin.

# Eternaltwin

Eternaltwin a besoin de développeurs ! Les différents jeux avancent à leur rythme mais le site principal servant à tout unifier a du mal à avancer. Si vous êtes intéressez, contacter sur Discord Patate404#8620, Demurgos#8218 ou Biosha#2584.

La mise à jour 0.12 approche et se concentre sur la publication de ses packages pour les développeurs.

# DinoCard

La première version du site est désormais en ligne. Il est déjà possible de créer son deck et le tester contre d'autres joueurs.

Sur ces premiers mois de développement, nous avons implémenté la boucle de combat et implémenté un total de 96 cartes (71 dinoz et 25 incantations). Il est déjà possible de tester les combats avec la quasi totalité des capacités spéciales des cartes (25 / 28).

La prochaine étape du projet est désormais d'implémenter les rouages internes du système d'effet qui servivra à implémenter la quasi totalité des autres cartes du jeu. Nous avons également débattu sur la façon d'octroyer des récompenses au joueur. Vous pouvez la consulter sur le site dinocard.net ou sur le thread discord associé.

Voici quelques statistiques sur les cartes du jeu
![DinoCard - Cards stats](./dinocard.png)

# Popotamo

Après une (très) longue interruption, le codage a repris dans les derniers jours de mars sous l'impulsion de @Aredhel. Sa première action a été de s'attaquer au bug le plus bloquant et le plus ardu : certains mots légitimes étaient refusés en cas de maçonnage, empêchant de valider le coup. Le correctif est en attente de chargement sur le site d'ePopotamo.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

Bonjour tout le monde !

Petite annonce pour vous indiquer qu’un concours d’art est désormais ouvert. Il se termine le 30 Avril, [toutes les informations sont disponibles sur ce sujet](https://myhordes.eu/jx/forum/5/68039). A vos créations !

# eMush

Bienvenue à tous dans cette section de consacrés à
[🍄 eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

## Quoi de neuf ?

Le mois dernier nous vous annonçions la sortie de la mise à jour Spores for All! Update en open alpha.

Vous avez été plus de 150 à rejoindre un vaisseau !

Nous avons ainsi passé le mois de Mars à stabiliser le jeu grâce à vos nombreuses remontées : toute l'équipe d'eMush vous remercie infiniement ! ❤

![eMush Spore Update for All](./emush1.png)

D'autre part, nous avons également eu l'occasion de nous entretenir ce mois-ci avec blackmagic, l'un des développeurs de Mush, suivi d'une série de questions/réponses avec la communauté.

Ce fut l'occasion de discuter sur les choix qui ont été faits sur Mush, ce qui fut très enrichissant pour nous.

Nous ressortons de cet entretien avec plein de bons conseils et un sursaut de motivation pour vous offrir une reprise de Mush à la hauteur.

## Prochainement

Durant ce mois bien chargé et suivant vos retours, nous avons également commencé à travailler sur la prochaine version d'eMush qui introduiera les hunters.

Nous pouvons d'ores et déjà éliminer les vaisseaux de départ aux tourelles sur la version de développement :

![eMush Coming soon](./emush2.png)

Nous comptons vous proposer cette mise à jour très prochainement et rajouter des fonctionnalités (patrouilleurs, débris...) régulièrement.

## Super ! Comment on joue ?

🎮 Jouer à [eMush](https://emush.eternaltwin.org)

💬 [Discord](https://discord.gg/SMJavjs3gk) (remonter un bug, discuter avec les développeurs...)

Merci de nous avoir lu, et à bientôt sur le Daedalus !

# Eternal DinoRPG

Bonjour tout le monde et merci de votre attention pour cette partie dédiée à [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

Salutations Maîtres Dinoz !

La version 0.6.0 est prête à être mise en ligne. Il n'y a malheuresement pas encore d'imports possible mais nous faisons de notre mieux pour y parvenir au plus vite.

Pas de grosses nouveautés visibles, mais une grosse refactorisation du code est en cours maintenant qu'on a su prendre du recul sur le projet.

L'équipe recherche toujours un nouveau nom pour le jeu. Ne vous retenez pas !

À bientôt. L'équipe EternalDino.

# Closing words

Vous pouvez envoyer [vos messages pour l'édition d'Avril](https://gitlab.com/eternaltwin/etwin/-/issues/60).

Cet article a été édité par : Biosha, Bibni, Demurgos, Dylan57, Evian, Fer, Nadawoo, Patate404 et Schroedinger.
