# News

## 2023-04

### This Month In Eternaltwin #19

[English](./2023-04/index.md) | [Deutsch](./2023-04/index.de.md) | [Español](./2023-04/index.es.md) | [Français](./2023-04/index.fr.md)

## 2023-03

### This Month In Eternaltwin #18

[English](./2023-03/index.md) | [Deutsch](./2023-03/index.de.md) | [Español](./2023-03/index.es.md) | [Français](./2023-03/index.fr.md)

## 2023-02

### This Month In Eternaltwin #17

[English](./2023-02/index.md) | [Deutsch](./2023-02/index.de.md) | [Español](./2023-02/index.es.md) | [Français](./2023-02/index.fr.md)

### Interview of Zenoo

[English](../interviews/01-zenoo/index.md) | [Deutsch](../interviews/01-zenoo/index.de.md) | [Español](../interviews/01-zenoo/index.es.md) | [Français](../interviews/01-zenoo/index.fr.md)

### Interview of BrainBox

[English](../interviews/02-brainbox/index.md) | [Deutsch](../interviews/02-brainbox/index.de.md) | [Español](../interviews/02-brainbox/index.es.md) | [Français](../interviews/02-brainbox/index.fr.md)

### Interview of Demurgos

[English](../interviews/03-demurgos/index.md) | [Deutsch](../interviews/03-demurgos/index.de.md) | [Español](../interviews/03-demurgos/index.es.md) | [Français](../interviews/03-demurgos/index.fr.md)
## 2023-01

### This Month In Eternaltwin #16

[English](./2023-01/index.md) | [Deutsch](./2023-01/index.de.md) | [Español](./2023-01/index.es.md) | [Français](./2023-01/index.fr.md)

## 2022-12

### This Month In Eternaltwin #15

[English](./2022-12/index.md) | [Deutsch](./2022-12/index.de.md) | [Español](./2022-12/index.es.md) | [Français](./2022-12/index.fr.md)

## 2022-11

### This Month In Eternaltwin #14

[English](./2022-11/index.md) | [Deutsch](./2022-11/index.de.md) | [Español](./2022-11/index.es.md) | [Français](./2022-11/index.fr.md)

## 2022-10

### This Month In Eternaltwin #13

[English](./2022-10/index.md) | [Deutsch](./2022-10/index.de.md) | [Español](./2022-10/index.es.md) | [Français](./2022-10/index.fr.md)

## 2022-09

### This Month In Eternaltwin #12

[English](./2022-09/index.md) | [Deutsch](./2022-09/index.de.md) | [Español](./2022-09/index.es.md) | [Français](./2022-09/index.fr.md)

## 2022-08

### This Month In Eternaltwin #11

[English](./2022-08/index.md) | [Deutsch](./2022-08/index.de.md) | [Español](./2022-08/index.es.md) | [Français](./2022-08/index.fr.md)

## 2022-07

### This Month In Eternaltwin #10

[English](./2022-07/index.md) | [Deutsch](./2022-07/index.de.md) | [Español](./2022-07/index.es.md) | [Français](./2022-07/index.fr.md)

## 2022-06

### This Month In Eternaltwin #9

[English](./2022-06/index.md) | [Deutsch](./2022-06/index.de.md) | [Español](./2022-06/index.es.md) | [Français](./2022-06/index.fr.md)

## 2022-05

### This Month In Eternaltwin #8

[English](./2022-05/index.md) | [Deutsch](./2022-05/index.de.md) | [Español](./2022-05/index.es.md) | [Français](./2022-05/index.fr.md)

## 2022-04

### This Month In Eternaltwin #7

[English](./2022-04/index.md) | [Deutsch](./2022-04/index.de.md) | [Español](./2022-04/index.es.md) | [Français](./2022-04/index.fr.md)

## 2022-03

### This Month In Eternaltwin #6

[English](./2022-03/index.md) | [Deutsch](./2022-03/index.de.md) | [Español](./2022-03/index.es.md) | [Français](./2022-03/index.fr.md)

## 2022-02

### This Month In Eternaltwin #5

[English](./2022-02/index.md) | [Deutsch](./2022-02/index.de.md) | [Español](./2022-02/index.es.md) | [Français](./2022-02/index.fr.md)

## 2022-01

### This Month In Eternaltwin #4

[English](./2022-01/index.md) | [Deutsch](./2022-01/index.de.md) | [Español](./2022-01/index.es.md) | [Français](./2022-01/index.fr.md)

## 2021-12

### This Month In Eternaltwin #3

[English](./2021-12/index.md) | [Deutsch](./2021-12/index.de.md) | [Español](./2021-12/index.es.md) | [Français](./2021-12/index.fr.md)

## 2021-11

### This Month In Eternaltwin #2

[English](./2021-11/index.md) | [Deutsch](./2021-11/index.de.md) | [Español](./2021-11/index.es.md) | [Français](./2021-11/index.fr.md)

### This Month In Eternaltwin #1

[English](./2021-10/index.md) | [Français](./2021-10/index.fr.md)
