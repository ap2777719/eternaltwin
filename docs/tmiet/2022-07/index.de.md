# 2022-07: Dieser Monat in Eternaltwin #10

[Eternaltwin](https://eternal-twin.net) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Wir haben den neuen Server und sein Verteilungssystem aktiviert. Die Projektleiter können nun
Aktualisierungen selbst online stellen. Neoparc ist das erste Projekt, das den neuen
Server für seine "Beta"-Version nutzt. Wir werden auch anderen Projekten bei der Umstellung
auf das neue System helfen.

# Eternalfest

Am 26. Juni 2022 erhierlt das größte [Eternalfest](https://eternalfest.net/)-Spiel das vorerst letzte Update. **Hackfest** ist nach 10 Jahren Arbeit vollständig.

Dieses Spiel hat über 1600 Level, Quests und Erfolge. Kehre zu Hackfest zurück, um
die Herausforderungen in Valhalla und dem finalen Labyrinth zu meistern und stelle dich dem mächtigsten 'Eternal', um die Welt ein weiteres Mal zu retten!

# Eternal DinoRPG

Das [Eternal DinoRPG](https://dinorpg.eternaltwin.org/)-Team ist relativ ruhig
auf Discord in Bezug auf die Entwicklung in letzter Zeit. Aber macht euch keine Sorgen, wir arbeiten immer noch aktiv an dem Projekt. Der aktuelle Schwerpunkt ist die Umstellung auf einen
Datenbank-Manager (von sequelize zu TypeORM für die Neugierigen). Dieser Wechsel ist notwendig, um dem Team die Möglichkeit zu geben, die Datenbankarchitektur und
Schemata zu aktualisieren, ohne sie auszulöschen. Dies ist ein großer Arbeitsaufwand, der uns auch die Möglichkeit gibt, unsere Code-Basis zu bereinigen und Code-Schulden zu reduzieren. Wir nähern uns dem Ende dieser Arbeit und sind mit den Fortschritten und den neuen Vorteilen zufrieden
von denen wir profitieren.

Darüber hinaus wird Swagger für Neugierige integriert, die unsere API direkt ausprobieren und testen wollen. Zu guter Letzt werden alle Bibliotheken des Projekts aktualisiert.

Jolu und Biosha werden in den Sommerferien sein (nicht zusammen ;D) und sind zum
Beginn des Schuljahres zurück. Sie werden Version 0.4.0 fertigstellen!

_Ein paar Worte von Biosha, dem Projektleiter:_

> Aus meiner persönlichen Sicht ist es nun etwas mehr als ein Jahr her, dass ich die
> Leitung des Projekts übernommen habe. Ich bin sehr zufrieden mit all der Arbeit, die geleistet wurde.
> Vor einem Jahr konnte man sich nur mit Eternaltwin anmelden und einen Dinoz kaufen. Heute,
> kann man einen Dinoz kaufen, ihn sehen, sich bewegen, Erfahrung und Stufen gewinnen, seine Fähigkeiten verbessern,
> Status erhalten, Gegenstände in den Shops kaufen, Nachrichten veröffentlichen, die Rangliste einsehen usw.
> Wir haben im Laufe des Projekts viel gelernt und lernen immer noch dazu. Vor einem Jahr,
> hatten wir uns zum Ziel gesetzt, unsere erste Version online zu veröffentlichen. Mit ein paar Monaten,
> und dank Demurgos, wurde Version 0.1.0 am 11. Dezember 2021 veröffentlicht! Seitdem
> wurden 114 Commits und 25.000 Zeilen Code geändert.
> Der Weg bis zu einer Version, die dem Originalspiel nahe kommt, ist noch lang, aber
> wir sind immer noch hoffnungsvoll und kommen jeden Tag weiter.

Vielen Dank an alle für die Hilfe und Unterstützung. Schöne Sommerferien!

# Neoparc

Hallo [Dinoz-Meister] (http://neoparc.eternal-twin.net/), lasst uns in dieser
TMIET-Ausgabe mit der aktuellen Arbeit anfangen. Wie viele von euch bereits wissen, bereiten wir ein neues "Clankrieg"-System vor, um es an die Anzahl der aktiven Neoparc-Spieler anzupassen.
Dieses System wird im Vergleich zu dem auf Dinoparc leicht verändert, aber es wird genauso spaßig und wettbewerbsorientiert bleiben wie das, was ihr vielleicht aus der Vergangenheit kennt. Es
wird keine Änderungen an den dunklen Dinoz und Totems geben, aber auch einige neue Änderungen! Außerdem
arbeiten wir an dem Dinoville-Missionszentrum, dem perfekten Ort, um ein paar zusätzliche
Münzen und Gegenstände für eure täglichen Aufgaben zu bekommen! Das Tempo ist im Sommer etwas langsamer,
aber wir sind immer noch sehr gut gelaunt dabei.

Was die technische Seite betrifft, so wird Neoparc bald auf einen leistungsfähigeren und
und funktionsreicheren Server wechseln. Dies wird euch nicht direkt betreffen, aber es wird es einfacher machen die
Patches und Fehlerkorrekturen zu verteilen. Wir haben bereits die Beta-Version
von Neoparc migriert, die Hauptversion wird im August umziehen.

Außerdem hatten wir ein einstündiges Treffen mit **warp**, dem ursprünglichen
Dinoparc-Schöpfer aus der Motion-Twin-Ära. Wir begannen mit der Vorstellung von Eternaltwin,
den verschiedenen Projekten und Neoparc. Dann haben wir eine Neoparc-Live-Demo präsentiert
und konzentrierten uns auf die neuen Funktionen wie die wöchentlichen Schlachtzüge, den "Goupignon", den
"Cherry"-Dinoz, die Farming-Technik, den überarbeiteten Bazar und die neuen Gegenstände.
Warp schien sehr gerührt, dass unsere Gemeinschaft sein Erbe bewahrt. Er hat volles Vertrauen in unser Projekt. Wir haben ihn gebeten, seine ehemaligen
Mitarbeiter bei Motion-Twin zu kontaktieren, um die alte Dinoparc-Website zu aktualisieren und auf
Neoparc zu verweisen. Das ist zwar kein Versprechen, aber es ist schön, dass er direkt nachfragt.

Insgesamt gab es diesen Monat nur wenige neue Funktionen, aber im Hintergrund gab es
viele wichtige Verbesserungen und Kontakte.

Ich wünsche Euch einen schönen Tag im Dinoland.

Bis zum nächsten Mal, Jonathan.

# eMush

Willkommen in der **[eMush](https://emush.eternaltwin.org/)**-Sektion von _Dieser
Monat in Eternaltwin_ für Juli 2022, das Projekt zur Erhaltung von Mush.

## Was ist neu?

Die letzten Neuigkeiten haben wir im Mai 2022 gepostet, die Version 0.4.2 des Spiels sah
wie folgt aus:

![0.4.2](./emush-0.4.2.png)

Heute sieht die Version 0.4.4 (noch in Entwicklung) eher so aus:

![0.4.4](./emush-0.4.4.png)

Wie ihr sehen könnt, gab es in den letzten zwei Monaten **viele Verbesserungen**, die wichtigste davon
die **2D-isometrische Schnittstelle** der Version 0.4.3. Sie wurde ausgiebig
von unseren Alpha-Testern im Juni getestet.

Wir haben den Juli damit verbracht, Fehler zu beheben, die während der Alpha gefunden wurden. Die meisten Gameplay-Probleme
sind bereits behoben. Grafische Fehler werden bald folgen.

Die nächste **Großaktualisierung** des Spiels sieht eine Menge Fortschritte: **Krankheiten**.
Du wirst bald einige **"lustige" Effekte** (wieder)entdecken...

![Space rabies](./emush-disease.png)

**Physische Krankheiten** sind fast alle erledigt. Unser Ziel ist es, **Seelische
Krankheiten** und **Wunden** zu implementieren, damit das Update für **Oktober** fertig ist.

## Nächste Schritte

Wir werden dann an **Fähigkeiten** arbeiten.

Das bedeutet auch, dass wir fast alle Aktionen aus dem Originalspiel implementieren werden.
Momentan sind etwa **40% der Aktionen** für alle Charaktere verfügbar... einschließlich
die erstaunliche Langweiler-Rede.

![Boring Speech](./emush-speech.png)

(ja, es ist wahr: wir haben den Fehler behoben, so dass ihr jetzt wirklich 3MP erhaltet 😳)

## Roadmap

Uns fehlen noch einige wichtige Funktionen von eMush:

- 🚧 Krankheiten und Wunden
- ❌ Fertigkeiten
- ❌ Forschung
- ❌ Projekte
- ❌ Das Kommunikationsterminal
- ❌ Jäger und Piloten
- ❌ Planeten und Reisen
- ❌ Expeditionen

Die folgenden Mechanismen sind jedoch bereits verfügbar (in der geschlossenen Alpha-Version):

- ✅ Beitritt der Daedalus mit 15 anderen Spielern, in isometrischer Ansicht
- ✅ Mush sein, Sporen bekommen, Menschen infizieren
- ✅ Die Mushs (oder Menschen) treffen und töten
- ✅ Aushalten von Erschütterungen, Bränden, Motorausfällen... und du kannst dich selbst heilen, die Brände löschen, die Probleme beheben... (oder von NERON ermordet werden, sobald es keine Menschen mehr gibt)

Das ist so ähnlich wie im Originalspiel, oder? 🙂

## Großartig! Wie spielt man?

Im Moment ist das Spiel nur während der geschlossenen Alpha-Sitzungen verfügbar. Wir halten sie
bei jedem größeren Update ab.

Wir geben euch Bescheid, wenn die nächste Sitzung beginnt. Bitte tretet dem [Eternaltwin
Discord](https://discord.gg/ERc3svy) bei, um die neuesten Informationen über das Projekt zu erhalten.

Vielen Dank an alle für eure Unterstützung und Hilfe, wir sehen uns später auf der Daedalus!

# Abschließende Worte

Du kannst deine [Nachrichten für August](https://gitlab.com/eternaltwin/etwin/-/issues/52) senden.

Dieser Artikel wurde bearbeitet von: Biosha, Demurgos, Evian, Fer, Jonathan, MisterSimple,
Patate404, SylvanSH und Schroedinger.
