# 2021-12 : Ce mois-ci sur Eternaltwin n°3

[Eternaltwin](https://eternal-twin.net) est un projet visant à préserver les jeux
et communautés de joueurs de la Motion Twin.

# Application Eternaltwin

Vous pouvez dorénavant [télécharger la version 0.5.6](https://eternal-twin.net/docs/desktop)
de l'application Eternaltwin, pour continuer de de jouer aux jeux Flash.

- Mise à jour des liens : MyHordes, Eternal DinoRPG.
- Les utilisateurs Windows peuvent désormais télécharger un `.zip` pour éviter
  l'installation.
- Correction des problèmes d'accès à la version anglaise d'Hammerfest.

Pensez bien à télécharger la dernière version !

# Eternaltwin

La conversion du serveur vers le langage Rust touche à sa fin.

Le forum a désormais [une section allemande](https://eternal-twin.net/forum/sections/86b23bed-27e8-4a19-b4d6-22a9fcd9142d).

[Eternal DinoRPG](https://dinorpg.eternaltwin.org/) est désormais en ligne, et
Epopotamo ne saurait tarder. Face à l'augmentation du nombre de jeux, nous nous
préparons également à migrer vers un serveur plus puissant.

# Emush

Le projet [Emush](https://emush.eternaltwin.org/) a un peu avancé. En particulier,
les bugs identifiés lors de la dernière Alpha ont été (presque) tous corrigés.
Ce qui nous amène à la dernière version (0.4.2) déployée au début du mois, incluant
ces corrections et complétant les effets des maladies. Des testeurs sont
actuellement en train de chercher à couler ces nouveaux Daedalus.

La prochaine version se concentrera sur l'amélioration de l'interface graphique
avec de nouvelles pièces ajoutées au Daedalus et l'ajout de nouvelles
fonctionnalités.

# MyHordes

_grésillements de radio_

Bonjour tout le monde et merci d'utiliser notre système radiophonique sans fil !
Je suis Connor et aujourd'hui je vous adresse un petit message concernant
[MyHordes](https://myhordes.eu/).

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Comme vous le savez, les Corbeaux se rassemblent aux alentours des villes en fin
d'année. Faute de chaleur, ils dévoreront tout de même vos foies en cette période
hivernale. Mais cependant ils ne sont pas cupides : du 20 décembre au
7 janvier, vous pourrez voler vos amis grâce à la tenue du père Noël. Qui sait,
peut-être que vous pourrez supporter son odeur nauséabonde ? Prenez également
le temps de bien fouiller les centres de poste, peut-être que vous trouverez des
colis de Noël perdus ? Ah et au passage, faites attention car le sable
devient dur, vous pourriez vous blesser ! Et gare à vos foies...
_grésillements_ le sucre, il ne faut pas en abuser.

C'était tout pour le moment, passez de bonnes fêtes avec vos citoyens et vos
am... _brouillé_ zombies. *grésillements*

# Eternal DinoRPG

**La version alpha 0.1.0 de Eternal DinoRPG vient de sortir !**

Vous pouvez la retrouver ici : [https://dinorpg.eternaltwin.org/](https://dinorpg.eternaltwin.org/)

Le site fonctionne sans Flash, cependant certaines animations peuvent ne pas s'afficher
correctement. Il est recommandé d'utiliser [l'application Eternaltwin](https://eternal-twin.net/docs/desktop)
pour le moment : Eternal DinoRPG est en haut de la liste.

Pour le moment vous ne pourrez que vous connecter, acheter des dinoz et les
faire se déplacer aléatoirement sur quelques lieux. Moins visibles, mais
fonctionnels, il est également possible d'afficher tous les statuts du jeu et
le contenu de votre inventaire ainsi que d'afficher et activer/désactiver toutes
les compétences du jeu.

Nous avons un comparateur de dinoz afin que vous, la communauté DinoRPG, puissiez
nous aider à trier les plus de 7000 éléments graphiques pouvant constituer un Dinoz.
N'hésitez surtout pas à acheter des dinoz, testez plein de choses !

La base de données sera remise à zéro plusieurs fois durant la période de développement.

Nous travaillons déjà sur la prochaine version qui permettra d'importer son
compte DinoRPG ainsi que quelques ajouts visuels tels que la sélection de la
langue sur le site. La version suivante quant à elle permettra d'afficher les
dinoz sur leur page sans Flash !

À très bientôt pour de nouvelles avancées !

# Neoparc

Un événement de Noël est en place depuis le 1er décembre sur [Neoparc](https://neoparc.eternal-twin.net/).
Cet événement permet à chaque maître Dinoz d’amasser plusieurs Tickets Cadeaux
chaque jour en fouillant sur les différents lieux de Dinoland. La fameuse race
des "Santaz" est aussi de retour.

Plusieurs joueurs chanceux ont déjà récolté quelques œufs de cette précieuse
race de Dinoz à la Roue de Noël de Dinoville. Petit spoiler pour nos chers
lecteurs : tous les joueurs qui se connecteront sur Neoparc le 25 décembre
auront un Oeuf de Santaz en guise de cadeau pour cette merveilleuse première
année sur le jeu.

Cette mise à jour spécialement préparée pour la période des fêtes est remplie de
nouveautés excitantes ! Je vous laisse le plaisir de tout découvrir à votre
rythme. N'hésitez pas à partager vos trouvailles et expériences sur [le channel
Discord](https://discord.gg/ERc3svy) avec le reste de la communauté.

C'est avec plaisir que je vous souhaite à tous et à toutes, une merveilleuse
période des fêtes !

Votre Neodev, Jonathan.

# Directquiz

[Directquiz](https://www.directquiz.org/) a bénéficié de quelques nouvelles
fonctionnalités au cours des 2 derniers mois.

La première nouvelle fonctionnalité qui a été développée, permet maintenant de
savoir quand quelqu'un est en train d'écrire dans le chat, on retrouve cela
dans de nombreuses applications de chat modernes, c'est désormais disponible
sur **Directquiz** !

Ensuite, vous pouvez désormais [soumettre vos nouvelles questions](https://www.directquiz.org/validationQuestion.php)
sans dépenser vos **Direct Dollars**. Vous pouvez choisir parmi une liste
de plus de 300 catégories ! N'hésitez pas à proposer vos questions !

Et enfin, pour cet hiver, le site arbore désormais un paysage enneigé animé afin
de célébrer cette fin d'année hivernale comme il se doit.

Nous avons également travaillé en interne pour réorganiser les fichiers de
configuration pour préparer une future migration vers les serveurs d'Eternaltwin.

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de janvier](https://gitlab.com/eternaltwin/etwin/-/issues/36).

Joyeux Noël et bonne fin d'année !
