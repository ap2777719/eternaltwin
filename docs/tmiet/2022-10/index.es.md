# 2022-10: Este Mes En Eternaltwin #13

[Eternaltwin](https://eternal-twin.net) es un proyecto con el objetivo de preservar los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

## Soporte de Crowdin

[Eternaltwin se encuentra en Crowdin](https://eternaltwin.crowdin.com/). Crowdin es una página que trabaja en traducciones. Crowdin nos patrocina, y tenemos una cuenta de empresa gratuita.

Si eres un jugador que desea ayudar, ahora puedes postular traducciones para ayudar a tus proyectos favoritos. Si eres un líder de juego, por favor contacta a _Demurgos_ para configurar tu proyecto.

## Migración a Rust

En octubre, el nuevo servidor de Eternaltwin implementado en Rust fue activado y el viejo Node.js backend fue completamente removido.

El código de Eternaltwin fue importado de Eternalfest, que data aproximadamente a 2015. Ha tenido varios problemas haciendo que el servidor se colapsara o se congelara. Para arreglar esto, comenzamos un gran esfuerzo interno para mudarnos al lenguaje de programación Rust, el cual es más rápido y confiable. Esto ha resultado exitoso: no hay más colapsos y el rendimiento es muy bueno.

Durante la migración, hemos tenido una gran cantidad de capas de controles y compatibilidad entre el código viejo y nuevo. Esto nos permitió evitar cualquier ruptura, pero también hizo que fuera bastante complicado agregar nuevas funciones. Ahora que este trabajo está definitivamente completo, pronto podremos concentrarnos en algunas funciones anunciadas hace mucho tiempo, como lo son mejores herramientas orientadas a las cuentas (registración por correo electrónico, verificación de correo electrónico, restauración de contraseña)

Aquí tienen una imagen de la evolución de los lenguajes utilizados para Eternaltwin:

![Eternaltwin](./loc.svg)

# Eternal DinoRPG

¡Bienvenidos, [maestros dinoz](https://dinorpg.eternaltwin.org/)!

La versión alfa 0.4.0 ahora está disponible. Pueden viajar, luchar, subir de nivel y ver a sus _moueffes_.

Todavía no hay muchos cambios visibles, pero se ha realizado mucho trabajo para que trabajar en el proyecto sea más fácil. Podremos publicar actualizaciones con mayor frecuencia.

Aun así, hay una nueva característica en esta actualización: ¡Personajes No-Jugadores (PNJ)! Sólo hay dos por el momento, pero nuevos se unirán a lo largo del tiempo. Por cierto, uno de estos dos debería darles algo bonito ;)

¡Nos vemos luego!

# eMush

Bienvenidos a esta sección dedicada a [eMush](https://emush.eternaltwin.org/), el proyecto dedicado a salvar Mush.

## ¿Qué hay de nuevo?

La última vez anunciamos la **Disease Update**, una actualización mayor de eMush, la cual completó el sistema de enfermedades del juego con más de 60 enfermedades y heridas (incluyendo algunas que no funcionaban de manera correcta en Mush). Algunos jugadores valientes se unieron a la nave espacial de prueba y están lidiando con estas nuevas plagas.

![eMush](./emush.png)

Gracias a su ayuda, estamos trabajando en arreglar las peculiaridades genéticas causadas por esta actualización (lengua que vuelve a crecer, Mushes alérgicos a Mush... 😱).

## Lo que se viene

Mientras tanto, estamos trabajando en la próxima actualización. Nos permitirá iniciar con la **alfa abierta**.

Nos estamos concentrando en 3 puntos principales:

- **Migración de base de datos segura**, así no tenemos que reiniciar el juego y las naves espaciales cada vez.
- **Archivo de naves espaciales**, para tener un seguimiento de sus logros.
- **Traducciones al inglés**

Respecto a este último punto, el juego se encuentra traducido al inglés en un 65% gracias a la ayuda de diferentes contribuyentes. Muchísimas gracias.

## ¡Me perdí la versión alfa! ¿Cómo juego?

Dependiendo de cuándo leas estas noticias, la versión alfa... todavía podría seguir estando disponible 👀.

Encontrarás los detalles para unirte a las pruebas en el [servidor de Discord de Eternaltwin](https://discord.gg/SMJavjs3gk).

Mush es un proyecto de Código Abierto. Nuestras notas de lanzamiento detalladas de la **Disease Update** se encuentran [disponibles en Gitlab](https://gitlab.com/eternaltwin/mush/mush/-/releases/0.4.4).

¡Muchas gracias por su atención, espero verlos pronto a bordo del Daedalus!

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

_ruido de radio_

¡Hola a todos! ¡Ben aquí con nuestro postre de noticias de actualización regular!

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Escuché reportes que muchos ciudadanos _interferencia_ calabazas en el yermo. Puede que se torne una ocurrencia regular, dado que sucedió el año pasado también... Aunque esta vez hay muchos menos avistamientos. ¿Qué podría significar todo esto? _interferencia_

En otras noticias, parece ser que los pueblos dejaron de jugar bajo las reglas, o mejor dicho, inventaron sus propias reglas. _interferencia_ reportó incidentes de envenenamientos misteriosos, ciudadanos corriendo a través del desierto cargando varias bolsas al mismo tiempo, e incluso... _crujido de papel_ ¿cavando en el mismo lugar repetidas veces? Estos son verdaderamente tiempos confusos _interferencia_

Oh, eso me recuerda... Seguramente han notado _interferencia_ mi voz clara como el cristal. ¡Eso es porque he invertido en una tecnología mucho mejor para nuestra estación de emisión! Con esto, nuestro incesante esfuerzo para traer las mejores noticias continúa mej... _interferencia_

_bip fuerte_ _estática fuerte_ ¿¿¿a qué te refieres con que la antena está prendida fuego??? OH DIOS M _interferencia_

#Eternal Kingdom

Ha pasado bastante tiempo desde la última entrada sobre [Eternal Kingdom](https://kingdom.eternaltwin.org/).

Seguimos enviando actualizaciones para la **alfa 0.4**. Hemos arreglado muchos problemas, ¡miren los [detalles completos en el foro](https://eternal-twin.net/forum/threads/733e22e2-cbf8-474e-bd28-3183d67a2bc6).

En este momento estamos trabajando en dos cosas: la generación del mapa y algunas tareas de mantenimiento.

Hemos trabajado en la herramienta de generación del mapa. Particularmente en cómo mostramos las casas de cada ciudad.

![Mapa del mundo de Kingdom con diferentes tipos de casas](./kingdom.png)

También estamos planeando un mantenimiento antes de proceder a las tareas de la alfa 0.5.

- Migrar el proyecto a Symfony 5 (mejor rendimiento)
- Configurar un proyecto en Crowdin para las traducciones
- Añadir pruebas automáticas para cazar bugs más fácilmente

Nos gustaría desplegar la alfa 0.5 en noviembre.

# PiouPiouZ

En el mundo de los Piouz, todo está bien.

El trabajo en una versión del juego sin Flash está avanzando. Los Piouz se pueden mover, pero no tienen habilidades todavía.

[Aquí tienen un pequeño vistazo de esta nueva versión](https://public.thetoto.fr/piou/pixi/).

Aclaración: ningún Piou fue dañado durante la producción. No intenten replicar esto en su casa. Ante cualquier situación adversa, consulte con su maestro Piou de confianza más cercano.

# Palabras finales

Pueden enviarnos [sus mensajes de noviembre(https://gitlab.com/eternaltwin/etwin/-/issues/55).

Este artículo ha sido editado por: Bibni, Biosha, Brainbox, Demurgos, Evian, Fer,
Patate404 y Schroedinger
