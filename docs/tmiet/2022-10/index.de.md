# 2022-10: Dieser Monat in Eternaltwin #13

[Eternaltwin](https://eternal-twin.net) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

## Crowdin Support

[Eternaltwin ist jetzt auf Crowdin](https://eternaltwin.crowdin.com/). Crowdin ist eine
Website, um an Übersetzungen zu arbeiten. Crowdin hat uns gesponsert, und wir haben ein kostenloses
Unternehmenskonto.

Wenn du ein Spieler bist, der helfen möchte, kannst du jetzt Übersetzungen einreichen, um deinen
bevorzugten Projekten zu helfen. Wenn du Spielleiter bist, kontaktiere bitte _Demurgos_, um
dein Projekt zu konfigurieren.

## Rust Migration

Im Oktober wurde der neue, in Rust implementierte Eternaltwin-Server aktiviert und
das alte Node.js-Backend wurde vollständig entfernt.

Der Eternaltwin-Code wurde von Eternalfest importiert und datiert etwa auf das Jahr 2015 zurück.
Er hatte verschiedene Probleme, die dazu führten, dass der Server regelmäßig abstürzte oder hing. Um dies zu beheben, haben wir eine große interne Anstrengung unternommen, um auf die Programmiersprache Rust umzusteigen,
die schneller und zuverlässiger ist. Dies hat sich als erfolgreich erwiesen: Es gibt keine Abstürze mehr
und die Leistung ist sehr gut.

Während der Migration hatten wir eine große Anzahl von Prüfungen und Kompatibilitätsschichten
zwischen dem alten und dem neuen Code. Dadurch konnten wir zwar Fehler vermeiden, machte es aber auch ziemlich schwer, neue Funktionen hinzuzufügen. Nun, da diese Arbeit endgültig abgeschlossen ist,
können wir uns auf einige lange angekündigte Funktionen konzentrieren, wie z.B. bessere Tools
rund um die Konten (Registrierung per E-Mail, E-Mail-Verifizierung, Passwortrücksetzung).

Hier ist ein Bild der Entwicklung der für Eternaltwin verwendeten Sprachen:

![Eternaltwin](./loc.svg)

# Eternal DinoRPG

Willkommen [Dinoz-Meister](https://dinorpg.eternaltwin.org/)!

Die Alpha-Version 0.4.0 ist jetzt verfügbar. Ihr könnt weiterhin reisen, kämpfen, leveln
und deinen _Moueffe_ sehen.

Es gibt nicht viele sichtbare Änderungen, aber es wurde eine Menge Arbeit geleistet, um die
die Arbeit an dem Projekt zu erleichtern. Wir werden in der Lage sein, häufiger Updates zu veröffentlichen.

Es gibt noch ein neues Feature in diesem Update: Nicht spielbare Charaktere (NSC)!
Im Moment gibt es nur zwei, aber mit der Zeit werden neue hinzukommen. Nebenbei bemerkt,
einer dieser beiden sollte euch etwas Nettes bieten.

Bis später!

# eMush

Willkommen in diesem Bereich, der [eMush](https://emush.eternaltwin.org/) gewidmet ist,
dem Projekt zur Rettung von Mush.

## Was ist neu?

Letztes Mal haben wir das **Disease Update** angekündigt, ein großes Update für eMush. Dieses
Update vervollständigte das Krankheitssystem des Spiels mit über 60 Krankheiten und Wunden
(einschließlich einiger, die in Mush nicht richtig funktionierten). Einige tapfere Spieler haben sich
dem Testraumschiff angeschlossen und beschäftigen sich mit diesen neuen Plagen.

![eMush](./emush.png)

Dank ihrer Hilfe arbeiten wir daran, die genetischen Fehler zu beheben, die durch dieses
Update entstanden sind (nachwachsende Zunge, Mushes allergisch gegen Mush... 😱).

## Demnächst

In der Zwischenzeit arbeiten wir an dem nächsten Update. Es wird uns erlauben, die **offene Alpha** zu starten.
Wir konzentrieren uns auf 3 Hauptpunkte:

- **sichere Datenbankmigration**, damit wir das Spiel und die Raumschiffe nicht jedes Mal zurücksetzen müssen
- **Raumschiffsarchive**, damit du deine Erfolge im Auge behalten kannst
- **englische Übersetzungen**

Was den letzten Punkt betrifft, so ist das Spiel jetzt zu 65% ins Englische übersetzt, dank der Hilfe der verschiedenen Mitwirkenden. Ich danke euch sehr.

## Ich habe die Alpha verpasst! Wie kann ich spielen?

Je nachdem, wann du diese Nachricht liest, könnte die Alpha noch verfügbar sein 👀.

Die Details zur Teilnahme an den Tests findet ihr [im Eternaltwin-Discord](https://discord.gg/SMJavjs3gk).

eMush ist ein Open Source Projekt, unsere detaillierten Release Notes für das **Disease Update** sind [verfügbar auf Gitlab](https://gitlab.com/eternaltwin/mush/mush/-/releases/0.4.4).

Vielen Dank für Ihre Aufmerksamkeit, und bis bald an Bord der Daedalus!

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

_Radioknistern_

Hallo zusammen, hier ist Ben mit unserem regelmäßigen Wüsten-News-Update!

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Ich habe Berichte gehört, dass viele Bürger in der Einöde Kürbisse _zerhackt_ haben. Das
könnte zu einem regelmäßigen Ereignis werden, da es auch im letzten Jahr passiert ist... Obwohl
dieses Mal gibt es viel weniger Sichtungen. Was könnte das alles bedeuten? _verschlüsselt_

Außerdem haben einige Städte offenbar aufgehört, sich an die Regeln zu halten, oder vielmehr
ihre eigenen zu erfinden. _verschlüsselt_ meldete Vorfälle von mysteriösen Vergiftungen,
Bürger, die in der Wüste herumlaufen und mehrere Taschen auf einmal tragen und sogar...
_Papierrascheln_ ... mehrmals an der gleichen Stelle graben? Dies sind wirklich
verwirrende Zeiten _verschlüsselt_

Oh, das erinnert mich an etwas. Sicherlich habt ihr den kristallklaren Klang meiner Stimme bemerkt. Das liegt daran, dass wir in bessere Technik für unsere Sendestation investiert haben!
Damit wird unser ständiges Bemühen, die neuesten Nachrichten zu bringen, noch _verschlüsselt_.
_lauter Piepton_ _lautes Rauschen_ Was soll das heißen, die Antenne brennt??? OH MEIN G _verschlüsselt_

# Eternal Kingdom

Es ist schon lange her, dass ich das letzte Mal etwas über [Eternal Kingdom](https://kingdom.eternaltwin.org/) geschrieben habe.

Wir bringen weiterhin Updates für die **Alpha 0.4**. Wir haben viele Probleme behoben, siehe dazu [die vollständigen Details im Forum](https://eternal-twin.net/forum/threads/733e22e2-cbf8-474e-bd28-3183d67a2bc6).

Wir arbeiten im Moment an zwei Dingen: der Kartenerstellung und einigen Wartungsarbeiten.

Wir haben an dem Werkzeug zur Erstellung von Weltkarten gearbeitet. Insbesondere daran, wie wir die Häuser der einzelnen Städte anzeigen.

![Kingdom world map with different kinds of houses](./kingdom.png)

Wir planen auch einige Wartungsarbeiten, bevor wir zu den Alpha 0.5 Aufgaben übergehen.
- Migrieren des Projektes zu Symfony 5 (bessere Leistung)
- Konfigurieren eines Crowdin-Projekts für Übersetzungen
- Automatisierte Tests hinzufügen, um Bugs leichter zu finden

Wir möchten die Alpha 0.5 im November bereitstellen.

# PiouPiouZ

In der Welt der Piouz ist alles gut.

Die Arbeit an einer Spielversion ohne Flash schreitet voran. Die Piouz können sich bewegen, haben aber noch keine Fähigkeiten.

[Hier ist eine Vorschau auf diese neue Version](https://public.thetoto.fr/piou/pixi/).

Während der Entwicklung wurde kein Piou verletzt.

# Abschließende Worte

Du kannst deine [Nachrichten für November](https://gitlab.com/eternaltwin/etwin/-/issues/55) senden.

Dieser Artikel wurde bearbeitet von: ibni, Biosha, Brainbox, Demurgos, Evian, Fer,
Patate404 und Schroedinger.
