# Server

## Environment

| Name     | Version                  |
|----------|--------------------------|
| JRE      | 17                       |
| Nginx    | 1.22                     |
| Node.js  | Any, 18.14.2 recommended |
| PHP      | 8.2                      |
| Postgres | 15.1                     |
| Ruby     | 3.0                      |

## Network

- Internal name: `norray.eternaltwin.org`
- IPv4: `135.125.2.219`
- IPv6: `2001:41d0:203:96db::`

# Configuration

The server is initialized from a custom Arch VM image.
The application configuration is then loaded from the repo [eternaltwin/config](https://gitlab.com/eternaltwin/config).
