use crate::precompile::precompile_all;
use clap::Parser;
use gitlab_client::http::HttpGitlabClient;
use gitlab_client::{
  CreateReleaseError, CreateReleaseLinkRequestView, CreateReleaseRequestView, GetReleaseRequestView, GitlabAuth,
  GitlabClient, InputPackageStatus, InputReleaseAssetsView, InputReleaseLink, ProjectRef, ProjectSlug,
  PublishPackageFileRequestView, Release, ReleaseLinkType,
};
use std::collections::{BTreeMap, BTreeSet};
use std::error::Error;
use std::fs;
use std::path::PathBuf;
use std::process::Command;
use std::thread::sleep;
use std::time::Duration;

const CRATES: [&str; 35] = [
  "mt_querystring",
  "postgres_tools",
  "serde_tools",
  "scraper_tools",
  "squirrel",
  "log",
  "core",
  "config",
  "constants",
  "mt_dns",
  "db_schema",
  "populate",
  "email_formatter",
  "mailer",
  "password",
  "dinoparc_client",
  "dinoparc_store",
  "hammerfest_client",
  "hammerfest_store",
  "popotamo_client",
  "twinoid_client",
  "twinoid_store",
  "dinorpg_client",
  "user_store",
  "oauth_provider_store",
  "auth_store",
  "token_store",
  "link_store",
  "forum_store",
  "oauth_client",
  "services",
  "rest",
  "app",
  "cli",
  "client",
];

pub const NATIVE_PACKAGES: [NativePackage; 3] = [
  NativePackage {
    name: "eternaltwin-x86_64-apple-darwin",
    target: Target {
      name: "x86_64-apple-darwin",
    },
  },
  NativePackage {
    name: "eternaltwin-x86_64-pc-windows-gnu",
    target: Target {
      name: "x86_64-pc-windows-gnu",
    },
  },
  NativePackage {
    name: "eternaltwin-x86_64-unknown-linux-gnu",
    target: Target {
      name: "x86_64-unknown-linux-gnu",
    },
  },
];

const EXE_PACKAGES: [&str; 5] = [
  "exe-armv7-unknown-linux-gnueabihf",
  "exe-x86_64-pc-windows-msvc",
  "exe-x86_64-unknown-linux-gnu",
  "exe",
  "cli",
];

const NPM_PACKAGES: [&str; 12] = [
  "core",
  "etwin-client-http",
  "etwin-client-in-memory",
  "etwin-pg",
  "local-config",
  "mt-dns",
  "native",
  "oauth-client-http",
  "pg-db",
  "rest-server",
  "website",
  "cli",
];

/// Arguments to the `publish` task.
#[derive(Debug, Parser)]
pub struct Args {
  #[clap(long)]
  skip_rust: bool,
  #[clap(long)]
  gitlab_job_token: Option<String>,
  #[clap(long)]
  gitlab_private_token: Option<String>,
  /// Packages to publish:
  /// - `eternaltwin-x86_64-apple-darwin`
  /// - `eternaltwin-x86_64-pc-windows-gnu`
  /// - `eternaltwin-x86_64-unknown-linux-gnu`
  /// - `native`
  packages: Vec<String>,
}

pub async fn publish(args: &Args) -> Result<(), Box<dyn Error>> {
  let working_dir = std::env::current_dir()?;
  let crates_dir = working_dir.join("crates");
  let packages_dir = working_dir.join("packages");

  let mut packages_available = BTreeMap::new();
  packages_available.extend(NATIVE_PACKAGES.into_iter().map(|p| (p.name, vec![p])));
  packages_available.insert("native", NATIVE_PACKAGES.to_vec());

  let mut packages_to_publish: BTreeSet<NativePackage> = BTreeSet::new();
  for pkg in &args.packages {
    match packages_available.get(pkg.as_str()) {
      None => panic!("unknown package: {pkg}"),
      Some(list) => packages_to_publish.extend(list.iter().copied()),
    }
  }

  if !packages_to_publish.is_empty() {
    let gl_auth = if let Some(token) = args.gitlab_private_token.as_deref() {
      GitlabAuth::PrivateToken(token)
    } else if let Some(token) = args.gitlab_job_token.as_deref() {
      GitlabAuth::JobToken(token)
    } else {
      panic!("missing GitLab authentication");
    };
    let gl_client = HttpGitlabClient::new();

    for pkg in packages_to_publish {
      pkg
        .publish(&gl_client, gl_auth)
        .await
        .expect("failed to publish package");
    }
    return Ok(());
  }

  eprintln!("update Rust app");
  let mut cmd = Command::new("yarn");
  cmd.current_dir(packages_dir.join("website"));
  cmd.arg("app:crate:browser");
  let s = cmd.status()?;
  assert!(s.success());

  eprintln!("precompile executable");
  precompile_all(&[])?;

  if !args.skip_rust {
    for c in CRATES {
      let crate_dir = crates_dir.join(c);
      let crate_dir = crate_dir.as_path();
      eprintln!("Publishing {}: {}", c, crate_dir.display());
      let mut cmd = Command::new("cargo");
      cmd.current_dir(crate_dir);
      cmd.arg("clean");
      let s = cmd.status()?;
      assert!(s.success());
      let mut cmd = Command::new("cargo");
      cmd.current_dir(crate_dir);
      cmd.arg("publish");
      let s = cmd.status()?;
      assert!(s.success());
    }
  }

  for p in EXE_PACKAGES {
    eprintln!("Publishing: {}", p);
    let dir = packages_dir.join(p);
    if p == "exe" {
      publish_npm_exe(dir)?
    } else {
      let mut cmd = Command::new("yarn");
      cmd.current_dir(dir);
      cmd.arg("npm").arg("publish");
      let s = cmd.status()?;
      assert!(s.success());
      sleep(Duration::from_secs(30));
    }
  }

  for p in NPM_PACKAGES {
    eprintln!("Publishing: {}", p);
    let dir = packages_dir.join(p);
    if dir.as_os_str().len() > 1 {
      panic!("stop");
    }
    match p {
      "etwin-pg" => publish_npm_etwin_pg(dir)?,
      _ => {
        let mut cmd = Command::new("yarn");
        cmd.current_dir(dir);
        cmd.arg("npm").arg("publish");
        let s = cmd.status()?;
        assert!(s.success());
        sleep(Duration::from_secs(30));
      }
    }
  }

  Ok(())
}

fn publish_npm_exe(pkg_dir: PathBuf) -> Result<(), Box<dyn Error>> {
  let dev_toml = fs::read_to_string(pkg_dir.join("Cargo.toml"))?;
  let dev_package_json = fs::read_to_string(pkg_dir.join("package.json"))?;

  let publish_toml = dev_toml
    .replace("\n# [workspace]\n", "\n[workspace]\n")
    .replace(r#", path = "../../crates/cli""#, "");
  assert_ne!(publish_toml, dev_toml);
  let publish_package_json = dev_package_json.replace("\"//install\"", "\"install\"");
  assert_ne!(publish_package_json, dev_package_json);

  fs::write(pkg_dir.join("Cargo.toml"), publish_toml)?;
  fs::write(pkg_dir.join("package.json"), publish_package_json)?;

  let mut cmd = Command::new("yarn");
  cmd.current_dir(&pkg_dir);
  cmd.arg("npm").arg("publish");
  let s = cmd.status()?;
  assert!(s.success());
  sleep(Duration::from_secs(30));

  fs::write(pkg_dir.join("Cargo.toml"), dev_toml)?;
  fs::write(pkg_dir.join("package.json"), dev_package_json)?;
  Ok(())
}

fn publish_npm_etwin_pg(pkg_dir: PathBuf) -> Result<(), Box<dyn Error>> {
  let scripts = pkg_dir.join("scripts");
  let scripts_tmp = pkg_dir.join("scripts-tmp");
  fs::rename(&scripts, &scripts_tmp)?;
  fs::create_dir(&scripts)?;
  let mut options = fs_extra::dir::CopyOptions::new();
  options.content_only = true;
  fs_extra::dir::copy(&scripts_tmp, &scripts, &options)?;

  let mut cmd = Command::new("yarn");
  cmd.current_dir(&pkg_dir);
  cmd.arg("npm").arg("publish");
  let s = cmd.status()?;
  assert!(s.success());
  sleep(Duration::from_secs(30));

  fs::remove_dir_all(&scripts)?;
  fs::rename(&scripts_tmp, &scripts)?;
  Ok(())
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NativePackage {
  pub name: &'static str,
  pub target: Target,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Target {
  pub name: &'static str,
}

impl NativePackage {
  pub async fn publish<Gl: GitlabClient>(&self, gl_client: &Gl, gl_auth: GitlabAuth<&str>) -> Result<(), ()> {
    let mut result = precompile_all(&[self.target.name.to_string()]).expect("failed to precompile");
    let (_, (path, meta)) = result.pop_first().expect("precompiled result is available");
    if self.is_published(gl_client, gl_auth, meta.version.as_str()).await? {
      eprintln!("already published");
      return Ok(());
    }
    let exe_path = path.join(meta.executable);
    let exe_name = exe_path
      .file_name()
      .expect("exe name exists")
      .to_str()
      .expect("exe name is valid");
    let exe = fs::read(exe_path.as_path()).expect("failed to read exe");
    let exe_result = gl_client
      .publish_package_file(PublishPackageFileRequestView {
        auth: Some(gl_auth),
        project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
        package_name: self.name,
        package_version: meta.version.as_str(),
        filename: exe_name,
        status: InputPackageStatus::Default,
        data: exe.as_slice(),
      })
      .await
      .expect("failed to upload exe");
    // not `metadata`, `meta_data`...
    let meta_data = fs::read(path.join("meta.json")).expect("failed to read meta");
    let meta_result = gl_client
      .publish_package_file(PublishPackageFileRequestView {
        auth: Some(gl_auth),
        project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
        package_name: self.name,
        package_version: meta.version.as_str(),
        filename: "meta.json",
        status: InputPackageStatus::Default,
        data: meta_data.as_slice(),
      })
      .await
      .expect("failed to upload meta");

    assert_eq!(exe_result.package_id, meta_result.package_id, "package id must match");
    eprintln!("published package files for: {}", self.name);
    let package_id = meta_result.package_id;
    let tag_name = format!("v{}", meta.version);
    let url = format!("https://gitlab.com/eternaltwin/eternaltwin/-/packages/{package_id}");
    let direct_asset_path = format!("/{}", self.name);
    let release_result = gl_client
      .create_release(CreateReleaseRequestView {
        auth: Some(gl_auth),
        project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
        tag_name: tag_name.as_str(),
        name: None,
        tag_message: None,
        description: None,
        r#ref: None,
        assets: InputReleaseAssetsView {
          links: &[InputReleaseLink {
            name: self.name.to_string(),
            url: url.clone(),
            direct_asset_path: Some(direct_asset_path.clone()),
            link_type: ReleaseLinkType::Package,
          }],
        },
        released_at: None,
      })
      .await;
    match release_result {
      Ok(_) => Ok(()),
      Err(CreateReleaseError::AlreadyExists) => {
        gl_client
          .create_release_link(CreateReleaseLinkRequestView {
            auth: Some(gl_auth),
            project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
            tag_name: tag_name.as_str(),
            name: self.name,
            url: url.as_str(),
            direct_asset_path: Some(direct_asset_path.as_str()),
            link_type: ReleaseLinkType::Package,
          })
          .await
          .expect("failed to create release link");
        Ok(())
      }
      r => {
        r.expect("release creation failed");
        Err(())
      }
    }
  }

  /// Check if this package is already published
  async fn is_published<Gl: GitlabClient>(
    &self,
    gl_client: &Gl,
    gl_auth: GitlabAuth<&str>,
    version: &str,
  ) -> Result<bool, ()> {
    let tag_name = format!("v{version}");
    let release: Release = gl_client
      .get_release(GetReleaseRequestView {
        auth: Some(gl_auth),
        project: ProjectRef::Slug(ProjectSlug::new("eternaltwin/eternaltwin")),
        tag_name: tag_name.as_str(),
        include_html_description: false,
      })
      .await
      .expect("failed to check release");
    let is_published = release.assets.links.iter().any(|l| l.name == self.name);
    Ok(is_published)
  }
}

// https://gitlab.com/api/v4/projects/eternaltwin%2Feternaltwin/packages/generic/eternaltwin-x86_64-apple-darwin/0.12.5/meta.json
