mod dns;
mod docs;
pub mod metagen;
pub mod precompile;
pub mod publish;

pub static REPO_DIR: Lazy<PathBuf> = Lazy::new(|| PathBuf::from(env!("CARGO_MANIFEST_DIR")).join(".."));

pub use dns::{dns, DnsArgs};
pub use docs::docs;
pub use metagen::{kotlin, php};
use once_cell::sync::Lazy;
use std::path::PathBuf;
