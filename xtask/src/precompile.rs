use crate::publish::{NativePackage, NATIVE_PACKAGES};
use crate::REPO_DIR;
use cargo_metadata::{Message, MetadataCommand};
use clap::Parser;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, BTreeSet, HashSet};
use std::error::Error;
use std::fs;
use std::io::ErrorKind;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

/// Arguments to the `precompile` task.
#[derive(Debug, Parser)]
pub struct Args {
  /// Targets to compile (all if none is provided)
  targets: Vec<String>,
}

/// Generated precompiled packages
pub fn run(args: &Args) -> Result<(), Box<dyn Error>> {
  precompile_all(&args.targets).map(drop)
}

pub fn precompile_all(targets: &[String]) -> Result<BTreeMap<NativePackage, (PathBuf, DistMeta)>, Box<dyn Error>> {
  let valid_targets = NATIVE_PACKAGES.iter().map(|t| t.target.name).collect::<BTreeSet<_>>();
  for target in targets {
    if !valid_targets.contains(target.as_str()) {
      return Err(format!("invalid target {target:?}").into());
    }
  }
  let wanted_targets: HashSet<&str> = targets.iter().map(|t| t.as_str()).collect::<HashSet<_>>();

  let bin_dir = REPO_DIR.join("bin");
  let bin_dir = bin_dir.as_path();
  let dist_dir = REPO_DIR.join("dist");
  // fs::remove_dir_all(&dist_dir).expect("failed to clean dist dir");
  fs::create_dir_all(&dist_dir).expect("failed to create dist dir");
  let cargo_meta = MetadataCommand::new().no_deps().current_dir(bin_dir).exec()?;
  let version = cargo_meta
    .packages
    .iter()
    .find(|p| p.name.as_str() == "eternaltwin")
    .map(|p| p.version.to_string())
    .expect("version not found");
  let version = version.as_str();
  let host_target_dir = PathBuf::from(cargo_meta.target_directory);
  let host_target_dir = host_target_dir.as_path();

  let mut result: BTreeMap<NativePackage, (PathBuf, DistMeta)> = BTreeMap::new();

  for package in NATIVE_PACKAGES {
    if !wanted_targets.contains(package.target.name) {
      continue;
    }
    eprintln!("dist: {}", package.name);
    let out_dir_buf = dist_dir.join(package.target.name);
    let out_dir = out_dir_buf.as_path();
    fs::create_dir_all(out_dir)?;
    let old_meta: Option<DistMeta> = get_meta(out_dir)?;
    let new_meta = match old_meta {
      Some(m) if m.version == version => m,
      _ => {
        let guest_target_dir = if std::env::var("CROSS_REMOTE") == Ok("1".to_string()) {
          // Workaround for <https://github.com/cross-rs/cross/issues/1218>
          PathBuf::from("/cross/project/target")
        } else {
          let cross_meta = MetadataCommand::new()
            .cargo_path("cross")
            .no_deps()
            .current_dir(bin_dir)
            .other_options(vec!["--target".to_string(), package.target.name.to_string()])
            .exec()?;
          cross_meta.target_directory.into_std_path_buf()
        };
        let guest_target_dir = guest_target_dir.as_path();
        let exe = precompile(host_target_dir, guest_target_dir, bin_dir, out_dir, package.target.name)?;
        DistMeta {
          version: version.to_string(),
          executable: exe,
        }
      }
    };
    let meta_path = out_dir.join("meta.json");
    let mut meta = serde_json::to_string_pretty(&new_meta)?;
    meta.push('\n');
    fs::write(meta_path, meta)?;
    result.insert(package, (out_dir_buf, new_meta));
  }

  Ok(result)
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct DistMeta {
  pub version: String,
  pub executable: String,
}

fn get_meta(target_dir: &Path) -> Result<Option<DistMeta>, Box<dyn Error>> {
  let meta_path = target_dir.join("meta.json");
  match fs::read_to_string(meta_path) {
    Ok(old_meta) => {
      let meta: DistMeta = serde_json::from_str(old_meta.as_str())?;
      Ok(Some(meta))
    }
    Err(e) => {
      if e.kind() == ErrorKind::NotFound {
        Ok(None)
      } else {
        Err(Box::new(e))
      }
    }
  }
}

fn precompile(
  host_target_dir: &Path,
  guest_target_dir: &Path,
  bin_dir: &Path,
  out_dir: &Path,
  target: &str,
) -> Result<String, Box<dyn Error>> {
  eprintln!("starting cross-compilation for precompiled target {target}");
  let container_executable = cross_build(bin_dir, target)?;
  let relative_executable = container_executable.strip_prefix(guest_target_dir)?;
  let host_executable = host_target_dir.join(relative_executable);
  eprintln!("compilation complete, executable: {}", host_executable.display());
  let executable = host_executable
    .file_name()
    .expect("missing exe name")
    .to_str()
    .expect("invalid exe name")
    .to_string();
  let dist_executable = out_dir.join(&executable);
  fs::copy(host_executable, dist_executable)?;
  let mut command = Command::new("cargo")
    .args(["clean"])
    .current_dir(bin_dir)
    .spawn()
    .unwrap();
  let out = command.wait()?;
  if !out.success() {
    panic!("cargo clean failed");
  }
  Ok(executable)
}

fn cross_build(bin_dir: &Path, target: &str) -> Result<PathBuf, Box<dyn Error>> {
  let mut command = Command::new("cross")
    .args([
      "build",
      "--bin",
      "eternaltwin",
      "--release",
      "--message-format=json",
      "--target",
      target,
      "--config",
      "profile.release.opt-level=\"z\"",
      "--config",
      "profile.release.lto=true",
    ])
    .env("CROSS_CONFIG", "./Cross.toml")
    .stdout(Stdio::piped())
    .current_dir(bin_dir)
    .spawn()
    .unwrap();

  let reader = std::io::BufReader::new(command.stdout.take().unwrap());
  let mut executable: Option<PathBuf> = None;
  for message in cargo_metadata::Message::parse_stream(reader) {
    if let Message::CompilerArtifact(artifact) = message? {
      if artifact.target.kind.contains(&"bin".to_string()) && artifact.target.name == "eternaltwin" {
        let exe = artifact
          .executable
          .map(PathBuf::from)
          .expect("`executable` field is present");
        let old = executable.replace(exe);
        assert!(old.is_none(), "duplicate executable");
      }
    }
  }

  let output = command.wait()?;
  if !output.success() {
    return Err("non-success exit status".to_string().into());
  }
  executable.ok_or_else(|| "executable path not found".to_string().into())
}

// fn generate_node_package(target: &str, version: &str, executable: &Path) -> Result<(), Box<dyn Error>> {
//   let working_dir = std::env::current_dir()?;
//   let package_dir = working_dir.join(format!("packages/exe-{target}"));
//   fs::create_dir_all(&package_dir)?;
//   let executable_name = executable
//     .file_name()
//     .ok_or_else(|| "failed to resolve executable name".to_string())?;
//   fs::copy(executable, package_dir.join(executable_name))?;
//   let executable_name = executable_name.to_str().expect("invalid executable name");
//   fs::write(package_dir.join("index.mjs"), get_index_mjs(executable_name))?;
//   fs::write(package_dir.join("index.d.mts"), get_index_d_mts())?;
//   fs::write(package_dir.join("README.md"), get_readme(target))?;
//   fs::write(
//     package_dir.join("package.json"),
//     get_package_json(target, version, executable_name),
//   )?;
//   Ok(())
// }
//
// fn get_index_mjs(executable_name: &str) -> String {
//   format!(
//     r#"// Auto-generated by `cargo xtask precompile`, do not edit manually
// export const executableName = {};
// export const executableUri = new URL(executableName, import.meta.url);
// "#,
//     serde_json::to_string(executable_name).expect("serializing the executable name succeeds")
//   )
// }
//
// fn get_index_d_mts() -> String {
//   r#"// Auto-generated by `cargo xtask precompile`, do not edit manually
// export const executableName: string;
// export const executableUri: URL;
// "#
//   .to_string()
// }
//
// fn get_package_json(target: &str, version: &str, executable_name: &str) -> String {
//   let (cpu, os) = match target {
//     "x86_64-unknown-linux-gnu" => ("x64", "linux"),
//     "x86_64-apple-darwin" => ("x64", "darwin"),
//     "x86_64-pc-windows-msvc" => ("x64", "win32"),
//     "armv7-unknown-linux-gnueabihf" => ("arm", "linux"),
//     t => panic!("unexpected target {t}"),
//   };
//
//   let pkg = json!({
//     "name": format!("@eternaltwin/exe-{target}"),
//     "version": version,
//     "description": format!("Precompiled Eternaltwin executable for {target}"),
//     "licenses": [
//       {
//         "type": "AGPL-3.0-or-later",
//         "url": "https://spdx.org/licenses/AGPL-3.0-or-later.html"
//       }
//     ],
//     "publishConfig": {
//       "access": "public",
//       "registry": "https://registry.npmjs.org/"
//     },
//     "type": "module",
//     "os": [os],
//     "cpu": [cpu],
//     "exports": {
//       ".": "./index.mjs",
//       "./package.json": "./package.json",
//     },
//     "bin": {
//       format!("etwin-{target}"): format!("./{executable_name}")
//     },
//     "files": [
//       format!("./{executable_name}"),
//       "./index.mjs",
//       "./index.d.mts",
//       "./package.json",
//       "./README.md",
//     ],
//     "preferUnplugged": true
//   });
//   let mut pkg = serde_json::to_string_pretty(&pkg).expect("writing the package.json succeeds");
//   pkg.push('\n');
//   pkg
// }
//
// fn get_readme(target: &str) -> String {
//   format!(
//     r#"# Precompiled eternaltwin for `{target}`
//
// This package is internal implementation of `@eternaltwin/exe`. You should
// not depend on it directly.
//
// This package was autogenerated through `cargo xtask precompile`, do not edit it
// manually.
// "#
//   )
// }
