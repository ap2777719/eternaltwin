# 2022-11-13

- **[Feature]** Publish TMIET#12 and TMIET#13
- **[Fix]** Update app version to `0.6.4`.
- **[Fix]** Convert all backend endpoints to Rust.
- **[Fix]** Use REST API for server-side rendering. It used custom implementations previously.

# 2022-09-07

- **[Feature]** Publish TMIET#11

# 2022-08-29

- **[Fix]** Do not apply the username lock when there is no username.

# 2022-08-29

- **[Fix]** Update app version to `0.6.3`.

# 2022-08-26

- **[Fix]** Update app version to `0.6.2`.
- **[Fix]** Update dependencies.
